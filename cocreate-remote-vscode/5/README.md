# Issue 5

https://gitlab.com/snapping-shrimp/cocreate-remote-vscode/-/issues/5

https://code.visualstudio.com/api/working-with-extensions/continuous-integration

---

GitLab support for windows?

https://stackoverflow.com/q/52031860/4772008

https://docs.gitlab.com/ee/development/windows.html

https://about.gitlab.com/blog/2020/01/21/windows-shared-runner-beta/#getting-started

https://about.gitlab.com/pricing/#gitlab-com

400 CI/CD free minutes as of this writing. Not sure if this is enough or not.
Let me put it into number of days. Oops, actually it's just hours, it's 6.666
hrs. Hmm. Not sure how much time each of my build would run for. I need to run
lint, unit test, and then integration test in multiple environments

---

Let me look for blog posts about CI/CD services for Open Source projects for
cross platform Development

https://www.katalon.com/resources-center/blog/ci-cd-tools/
seems like some old data but new post

https://www.reddit.com/r/devops/comments/96rmzz/cloud_based_cicd_platform_to_build_linux_osx_and/ very old post

Oh. Appveyor has support for all too - https://www.appveyor.com/

https://www.browserstack.com/blog/best-ci-cd-tools-comparison/ 2019 post

Travis CI has support for windows for Open Source projects too I think.
https://blog.travis-ci.com/2018-10-11-windows-early-release#:~:text=Windows%20is%20available%20right%20now,feedback%20on%20our%20community%20forum.

but I don't see it in the example here
https://code.visualstudio.com/api/working-with-extensions/continuous-integration#travis-ci hmm.

https://stackoverflow.com/q/19818336/4772008

https://circleci.com/blog/windows-general-availability-announcement/ circle CI also has windows support

https://travis-ci.com/plans 5 concurrent jobs is cool I guess. And unlimited builds!!! :O

https://circleci.com/pricing/

https://circleci.com/open-source/ a lot of free credits for Open Source!! :) Hmm

For Azure, it's 1800 minutes per month. 30 hrs. Not bad! https://azure.microsoft.com/en-in/pricing/details/devops/azure-devops-services/ wow!

For now, Travis seems nice, with unlimited offer. Hmm.

https://azure.microsoft.com/en-us/services/devops/pipelines/ apparently does
have unlimited minutes for open source projects and pandas uses it - I can see
pandas testimonial on their page. I guess the thing mentioned here

https://code.visualstudio.com/api/working-with-extensions/continuous-integration is true then. Of course.

Now I'm wondering should I just go ahead with it. I'm just going to check how
pandas uses the azure pipeline and also check other vs code extensions and see
what they use especially when running integration tests for cross platform.

So, from the current thoughts, clearly the goals are

- Free hosted service on the cloud.
  As we cannot maintain our own CI/CD server or runners etc. It's a lot of
  effort and maintenance. Also, given our project is open source, with no sort
  of donations or funds, we are on our own with nothing. So it would be useful
  to with just simple free options.

- Enough build minutes
  Given we do want to run tests, we still need enough free build minutes for the
  project or else we would be stuck with no CI/CD after we finish our quota /
  limits. I hope it doesn't come to that and I hope it comes to that too :P
  I hope because - that would mean that there are lot of commits in the project
  and probably many people contributing, many PRs which again needs CI. So yeah,
  but I hope not because, well, of course, we want CI/CD to be always present
  no matter what, so that we can build and delivery quality software! :)

- Support for Linux, Windows and MacOS for integration testing.
  I assumed this is needed. But I'm not so sure. I don't think we will be
  building any operating system specific features or use any operating system
  specific features except a thing or two. I gotta check if it's really needed.
  For now I'm only assuming this is needed. Also, VS Code docs also show
  examples and seems like they recommend it by showing examples? Idk, or maybe
  they are just examples for demo. I could check what other projects do and if
  they do cross platform integration testing.

  Given VS Code is an electron app, electron being cross platform, I think it's
  good enough by itself in terms of cross platform stuff. Let's see.

- Good developer experience
  Of course this is key. I don't want my builds to be stuck for long or take a
  lot of time. I agree we are looking for free service, but it shouldn't be too
  bad. 😅 I know, too much to ask for. I hope that our work for the open source
  community can balance for the free stuff we get.

- Support for integration with GitLab
  Since we are on GitLab, we need some sort of integration with GitLab. Usually
  this translates to support for webhooks where CI/CD is run for each push.
  Some source code hosting providers also have first class support for
  integration with CI/CD and have a good UI for it and CI/CD providers also
  use APIs and provide good developer experience. Gotta see how the experience
  is for the one we choose. Push to master and for PRs in the future. It's okay
  if the developer experience is normal, but CI/CD must run for sure, for both
  master and PRs too ideally.

---

I can see vscode-jest extension have this `.travis.yaml`

https://github.com/jest-community/vscode-jest

```yml
language: node_js

stages:
  - verify
  - test
  - pre-publish

env:
  - YARN_GPG=no

## disable eol conversion upon clone
git:
  autocrlf: input

jobs:
  - stage: "verify"
    name: "verify"
    node_js: "10"
    os: osx
    script: yarn danger ci

node_js:
  - "10"
  - "lts/*"

os:
  - osx
  - linux
  - windows

script:
  - yarn ci
  - cat ./coverage/lcov.info | ./node_modules/coveralls/bin/coveralls.js
  - yarn vscode:prepublish
```

Clearly they use Travis CI service for open source projects -

https://travis-ci.org/github/jest-community/vscode-jest/builds/718657170

---

GitHub Actions - I believe I can only use it in GitHub and not anywhere else.
Or even if I could, it would be a hacky solution, like, replicate or mirror my
GitLab repo to GitHub and then run GitHub actions. It wouldn't be exactly
viable for PR model though.

---

I checked pandas project using Azure. The pipeline config looks a bit complex,
because of their complexity. The most basic source file is this

```yml
# Adapted from https://github.com/numba/numba/blob/master/azure-pipelines.yml
trigger:
  - master
  - 1.1.x

pr:
  - master
  - 1.1.x

variables:
  PYTEST_WORKERS: auto

jobs:
  # Mac and Linux use the same template
  - template: ci/azure/posix.yml
    parameters:
      name: macOS
      vmImage: macOS-10.14

  - template: ci/azure/posix.yml
    parameters:
      name: Linux
      vmImage: ubuntu-16.04

  - template: ci/azure/windows.yml
    parameters:
      name: Windows
      vmImage: vs2017-win2016
```

With many other files which are some sort of templates.

I could just use a simple config like the one present here

https://code.visualstudio.com/api/working-with-extensions/continuous-integration#azure-pipelines

Also, I noticed that other CI mention only the OS name - `linux`, `osx`,
`windows`. No idea what exactly is running behind the scenes, probably the
docs can help. They probably have support for more? Idk

GitHub Actions seems to have some sort of `latest` version

```yml
[macos-latest, ubuntu-latest, windows-latest]
```

Azure has a clear mention of versions. I like it! `macOS-10.14`, `ubuntu-16.04`,
`vs2017-win2016`

---

I think I'm going to go with Azure pipelines for now. If I face any issues or
think I can use something better or change my mind for some reason, then at that
time I can start looking at other options, given there are tons! I guess next
time I'll checkout Travis CI. I have used it a bit before. It's a simple and
good service. I have used Circle CI too a bit. A lot of GitLab CI/CD but in a
self hosted environment and very little GoCD for building a plugin, and that's
it. And GitHub Actions in another project. It looks simple and clean. I love the
UI in most of these services.

Azure pipeline UI looks normal :P Idk :P

---

Okay, so I already have a Azure DevOps account and a Azure DevOps project for
my extension. Thankfully I didn't create another project just for pipeline.
So apparently Azure has tons of things for projects - kanban boards etc.

I made my Azure DevOps project for the extension public. Anyone can see the
build pipelines now

https://dev.azure.com/SnappingShrimp/cocreate-remote-vscode/_build

https://docs.microsoft.com/en-us/azure/devops/organizations/public/make-project-public

Now I gotta check how to integrate my GitLab repo with this pipeline as I think
that by default, it has it's own code repo, artifacts etc

https://azure.microsoft.com/en-us/services/devops/pipelines/

Right. I have to use a third party integration for integrating GitLab with
Azure. Hmm

https://duckduckgo.com/?t=ffab&q=azure+pipelines+with+gitlab&ia=web

https://stackoverflow.com/questions/64183087/how-do-i-use-gitlab-code-project-in-azure-devops

https://marketplace.visualstudio.com/items?itemName=onlyutkarsh.gitlab-integration

https://github.com/onlyutkarsh/gitlab-integration

https://marketplace.visualstudio.com/search?term=gitlab&target=AzureDevOps&category=Azure%20Pipelines&sortBy=Relevance

Damn it, for GitHub, there's existing integration

https://marketplace.visualstudio.com/items?itemName=ms-vsts.services-github

Hmm.

https://marketplace.visualstudio.com/search?term=github&target=AzureDevOps&category=Azure%20Pipelines&sortBy=Relevance

Maybe I should go back to choosing a different pipeline? Hmm.

I think this is not great because - in this case, I can only run the build in
Azure pipelines, but I cannot get notifications automatically from Azure
pipeline for my commits or PRs.

The good developer experience will be lost.

https://stackoverflow.com/questions/57611081/integrating-azure-pipeline-with-gitlab-continuous-integration

https://zapier.com/apps/azure-devops/integrations/gitlab

If it was GitHub, I would get a good tick mark or cross mark near the commit
and good integration to show the status of pipeline in the GitHub UI. Hmm.

---

For now I should stick with Travis CI maybe. Still unlimited runs. I think it's
a good service. :)

GitLab integration
https://docs.travis-ci.com/user/gl-oauth-scopes/

Multi OS
https://docs.travis-ci.com/user/multi-os/

Config validation
https://docs.travis-ci.com/user/build-config-validation/

VM or runner configuration in terms of OS and other features

https://docs.travis-ci.com/user/reference/overview/#sts=Virtualisation%20Environment%20vs%20Operating%20System%20#

https://docs.travis-ci.com/user/reference/overview/#what-infrastructure-is-my-environment-running-on

I guess the fact that I can't get PR status information on GitLab is because of
the fact that it does not probably have such a feature for external service?

It's crazy how I chose GitLab for hosting my source code. This was based on the
fact that GitLab has an Open Source version of it's service, even though
gitlab.com itself is Enterprise Edition and hence closed source. Hmm. I gotta
check what other people do in such cases. Like, how they integrate CI/CD
with GitLab and still have a good developer experience.

If nothing works out, probably my decision to choose GitLab has to be revisited.
As I cannot have a bad developer experience at the cost of this decision. Have
to measure the pros and cons.

If I think about it, almost any of the CI/CD services I considered are not open
source. Only GitLab CI/CD has some open source stuff - GitLab runners and all.
TravisCI, CircleCI, Azure, GitHub Actions, etc are all closed source. Hmm.

---

I was thinking about moving to GitHub for sometime. I'm not really sure. Hmm.

I started to analyze the different options and the reasoning. And come to think
of it, I have not analyzed the Travis CI option well - I didn't try it out. I
can only talk more about it if I try it out. Maybe it does have the support I'm
looking for.

Apart from that, GitLab CI/CD is good too - it's okay if they have only 400
minutes. Apparently, open source projects can apply for a higher plan with more
minutes for free. But I need to add open source licenses for it and also show
that I need more than 400 minutes, for which I need to start using GitLab and
see the usage :) The only small problem I noticed is, GitLab MacOS support is
not present. Hmm.

Maybe I should first try out Travis CI and see if I can get a developer
experience similar to what one would get on GitHub, with respect to commits and
PRs, then think about other steps!

Apart from Travis CI, there's also Circle CI, Appveyor and others. The only
thing to check is, if the integration with GitLab is nice and has a good
developer experience or not. Hmm.

GitLab of course has an API for commit build status

https://docs.gitlab.com/ee/api/commits.html
https://docs.gitlab.com/ee/api/commits.html#post-the-build-status-to-a-commit

I hope travis CI has integration at this level. Okay, I should get started and
try this out!! :)

---

Okay, wow! :) Travis CI does have some good integration!! :)

https://gitlab.com/karuppiah7890/travis-ci-demo
https://gitlab.com/karuppiah7890/travis-ci-demo/-/pipelines
https://gitlab.com/karuppiah7890/travis-ci-demo/-/commits/main
https://gitlab.com/karuppiah7890/travis-ci-demo/-/merge_requests/1

I think this is good enough!! :) I'm just going to go with Travis CI just for
the sake of cross platform as GitLab CI/CD does not currently have support for
Mac. Also, the number of minutes in GitLab is pretty less for the long term,
but is unlimited in Travis CI!

I guess I should have done this analysis before!! I was so wrong assuming that
the integration with GitLab may not be great or that GitLab won't have such
features. Of course it had, for GitLab CI/CD and the same can be used for
external services too! :)

I'm NOT going to go with GitHub just yet. I think GitLab is good enough. I want
to use services that have some sort of Open Source core product too. GitHub is
nice and all, and gives so much to Open Source, but still, it's software is not
Open Source. Idk. Only recently I got this idea where GitLab is a bit better
in terms of Openness in the fact that it's core is open. For me it's ironic
that that's not the case with GitHub. But they do a lot for Open Source. Yeah.
That's great! :)

Anyways.

Now, the travis CI builds for the demo project are here

https://travis-ci.com/gitlab/karuppiah7890/travis-ci-demo/builds

I'll have to use travis-ci.com and do the same for cocreate-remote.

---

Now, I have to come up with a strategy for what tasks to run in the pipeline
and how to run them.

Clearly we have the following automated tasks -

- Lint
- Compile
- Unit Test
- Integration Test

Lint, Compile and Unit Test just need to run once, on one of the platforms,
given it's not exactly platform dependent - if it is, we would run that as part
of integration test. So, since it's all nodejs, we can just run them with Linux
and nodejs installed.

For Integration Test, we need to run them in three platforms - windows, linux
and macos. Also, Integration Test needs Compilation step first. For now, we can
do it again. Maybe later we can see how the output from Compile step can be
used for running the integration test. Actually, the compilation does not take
so much time, so I guess it's okay to compile again. It's just a few seconds.
Let's see. Maybe even in the first try, I can try to get something out where
Compile is done exactly only once ;) That would be cool!

---

https://docs.travis-ci.com/user/build-config-validation/

https://github.com/travis-ci/travis-yml

https://config.travis-ci.com/explore

https://config.travis-ci.com/

https://config.travis-ci.com/ref/os

https://config.travis-ci.com/ref/job/cache
https://docs.travis-ci.com/user/caching/#npm-cache

https://config.travis-ci.com/ref/stages
https://docs.travis-ci.com/user/build-stages/

I think I can run lint, compile and unit test in one stage. Not sure what to
name it. I actually need to run audit too! For checking if there are any
security issues!!

So, one stage for

- Lint
- Compile
- Unit test
- Audit

Or...maybe I could run them slowly. One by one. We don't have to start off so
many parallel jobs and then realize that even basic things are missing.

So, maybe it can be like - run Lint and Audit parallely first. So it's like,
basic issues like lint issues and big issues like security vulnerabilities are
caught very early.

Next we can run compile - which compiles both integration test code and also
extension code, but NOT unit test. If the extension source code has type issues
then it will be caught here.

Next, we can run unit test, considering that the extension code compiles well
and all basic checks are already done.

If all tests pass, then we do integration test in another stage, where each
OS job should ideally be running in parallel, to speed up things :) :P

So, it looks like

Stages:

- Lint and Audit in parallel
- Compile
- Unit tests
- Integration tests in different OS in parallel

What about installing dependencies? Will travis take care of it? Gotta check

https://docs.travis-ci.com/user/job-lifecycle/

I'm going to put the xvfb code in `before_script`. Also, I can mention multiple
build commands as well it seems! :)

I could put the xvfb code as the first script and then run the others. Hmm.

https://docs.travis-ci.com/user/languages/javascript-with-nodejs/

Oh, many macosx versions are available!! Nice

https://docs.travis-ci.com/user/reference/osx/#macos-version

Windows
https://docs.travis-ci.com/user/reference/windows/#windows-version

---

I'm also wondering if it's good to put so many parallel jobs and stages. It's
going to take time to spin up these instances right? Hmm. Let's see. If it
takes too much time, I'm just going to put them all in one stage and one job
and make sure they run in sequence and stop when one of them fails. Actually,
I'm going to do just that I think! :)

---

https://config.travis-ci.com/ref/language/node_js

---

Gotta check about some stuff later. Like Git CRLF

```yml
# .travis.yml
git:
  autocrlf: input
```

```properties
# all text files will be checked in with LF as eol
* text=auto
```

---

Awesome! So I setup the CI and it runs, but one of them failed 😅

https://travis-ci.com/gitlab/snapping-shrimp/cocreate-remote-vscode

https://travis-ci.com/gitlab/snapping-shrimp/cocreate-remote-vscode/builds/192562825

Windows build failed. Gotta check why. :)

---

I'm afraid that the possible reason could be that the kind of test I want to
run for Windows is not supported by Travis CI? I should probably check what to
do and also debug more to ensure that's the case.

I should have ideally done some experiment instead of directly commiting code
in the main repo. Hmm. It's okay.

Other options are CircleCI, AppVeyor and probably many others. One idea is -
for windows alone, I could use another CI. It's not a problem I guess? Not sure
though.

I need to try out multiple as an experiment. Hmm. Even Azure pipelines, just
for the sake of trying :P :) With some dummy project.

Tasks

- Debug further the Travis CI build error
- Try out other CI options in case Travis CI cannot support Windows

---

https://docs.travis-ci.com/user/reference/windows/#windows-version

So, what we use is a windows server.

It has some pre installed stuff

https://docs.travis-ci.com/user/reference/windows/#pre-installed-chocolatey-packages

Some issues on the Travis forum -
https://travis-ci.community/t/make-running-gui-multimedia-apps-available-in-windows/1557
https://travis-ci.community/t/make-running-gui-multimedia-apps-available-in-windows/1557/7

---

Circle CI support for windows

https://duckduckgo.com/?t=ffab&q=circleci+windows&ia=web

https://circleci.com/docs/2.0/hello-world-windows/#windows-executor-images

https://circleci.com/docs/2.0/hello-world-windows/#software-pre-installed-in-the-windows-image

---

Azure pipelines windows support

https://duckduckgo.com/?t=ffab&q=azure+pipelines+windows&ia=web

https://docs.microsoft.com/en-us/azure/devops/pipelines/?view=azure-devops

https://docs.microsoft.com/en-us/azure/devops/pipelines/ecosystems/containers/build-image?view=azure-devops#windows-container-images

---

I was wondering about xvfb

http://www.xfree86.org/4.0.1/Xvfb.1.html

---

https://help.appveyor.com/discussions/questions/2228-windows-equivalent-of-linux-export-display-for-gui-app-testing

---

https://www.appveyor.com/docs/how-to/rdp-to-build-worker/

---

https://www.appveyor.com/docs/windows-images-software/

---

https://help.appveyor.com/discussions/questions/678-gui-unit-testing-in-appveyor

---

Debugging the failed build - I tried to restart the build just as a start.

Checking out the exit code it showed

`3221225781`

https://duckduckgo.com/?t=ffab&q=exit+code+3221225781&ia=web

https://forum.plutonium.pw/topic/759/failed-to-start-laucher-exit-status-3221225781/3

Restarted build also failed
https://travis-ci.com/gitlab/snapping-shrimp/cocreate-remote-vscode/jobs/406799511

https://stackoverflow.com/questions/49413443/trouble-debugging-error-with-exit-code-3221225781-missing-libraries-in-windows

https://duckduckgo.com/?q=exit+code+3221225781+travis+ci&t=ffab&ia=web

https://travis-ci.community/t/vscode-extension-builds-failing-exit-code-3221225781/7187

https://travis-ci.community/t/cgo-and-exit-status-3221225781/2015/7

---

Azure pipelines windows images

https://docs.microsoft.com/en-us/azure/devops/pipelines/agents/hosted?view=azure-devops&tabs=yaml

For the example given in VS Code website

`vs2017-win2016` - https://github.com/actions/virtual-environments/blob/main/images/win/Windows2016-Readme.md

---

https://travis-ci.community/t/opengl32-dll-cannot-open-shared-object-file-no-such-file-or-directory/5188

https://github.com/microsoft/vscode/issues/77499

---

https://github.com/getgauge/gauge-vscode/blob/master/.github/workflows/vscode.yml

I think it works in that. Hmm.

https://github.com/getgauge/gauge-vscode/runs/1300978933

https://github.com/getgauge/gauge-vscode/blob/master/package.json

https://github.com/getgauge/gauge-vscode/blob/master/test/runTest.ts

https://github.com/getgauge/gauge-vscode/blob/master/test/index.ts

Gotta check what GitHub actions provides as part of windows environment :)

https://github.com/actions/virtual-environments

`windows-latest` currently points to Windows Server 2019

https://github.com/actions/virtual-environments/blob/main/images/win/Windows2019-Readme.md

https://github.com/actions/virtual-environments/blob/releases/win19/20201021/images/win/Windows2019-Readme.md

Oh. This is the same as what azure pipelines said. They all use similar images
I guess. Hmm. It's all microsoft at the end of the day. Meh.

Circle CI supports windows server 2019 it seems. Hmm. Sweet. Gotta try that
first I guess! ;)

https://circleci.com/build-environments/windows/

https://circleci.com/docs/2.0/hello-world-windows/#software-pre-installed-in-the-windows-image - Windows 2019 with Visual Studio
2019 community edition. Hmm. Gotta try it! :)

---

https://dockerquestions.com/2019/06/16/chrome-installation-fails-in-windows-docker-container-non-zero-code-3221225781/

---

I'm trying out running integration tests in my local within Docker container.

I'm using `node:12-alpine` for a nodejs environment and that too a minimal one.

I have installed the dependencies. I need to use xvfb now I think as the test
failed

```bash
$ docker run -it --rm -v $(pwd):/app node:12-alpine sh
# cd /app
/app # npm test

> vscode-gitlab-ci-demo@0.0.1 pretest /app
> npm run compile && npm run lint


> vscode-gitlab-ci-demo@0.0.1 compile /app
> tsc -p ./


> vscode-gitlab-ci-demo@0.0.1 lint /app
> eslint src --ext ts


> vscode-gitlab-ci-demo@0.0.1 test /app
> node ./out/test/runTest.js

Downloading VS Code 1.50.1 from https://update.code.visualstudio.com/1.50.1/linux-x64/stable
Downloaded VS Code 1.50.1 into .vscode-test/vscode-1.50.1
Test error: Error: spawn /app/.vscode-test/vscode-1.50.1/VSCode-linux-x64/code ENOENT
Exit code:   -2
Done

Failed to run tests
npm ERR! Test failed.  See above for more details.
/app # ls /app/.vscode-test/vscode-1.50.1/VSCode-linux-x64/code
/app/.vscode-test/vscode-1.50.1/VSCode-linux-x64/code
/app # ls /app/.vscode-test/vscode-1.50.1/VSCode-linux-x64/
bin                      icudtl.dat               libvulkan.so             swiftshader
chrome-sandbox           libEGL.so                locales                  v8_context_snapshot.bin
chrome_100_percent.pak   libGLESv2.so             resources                vk_swiftshader_icd.json
chrome_200_percent.pak   libffmpeg.so             resources.pak
code                     libvk_swiftshader.so     snapshot_blob.bin
```

I can see these commands in the vs code docs

```bash
$ /usr/bin/Xvfb :99 -screen 0 1024x768x24 > /dev/null 2>&1 &
```

```bash
$ xvfb-run -a npm test
```

Since I'm on alpine OS, I was checking alpine packages

https://duckduckgo.com/?t=ffab&q=alpine+xvfb&ia=software

https://pkgs.org/download/xvfb-run

https://pkgs.org/search/?q=xvfb

https://pkgs.org/download/xvfb

```bash
/app # apk add xvfb
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/community/x86_64/APKINDEX.tar.gz
(1/50) Installing encodings (1.0.5-r0)
(2/50) Installing font-alias (1.0.3-r1)
(3/50) Installing libfontenc (1.1.4-r0)
(4/50) Installing libbz2 (1.0.8-r1)
(5/50) Installing libpng (1.6.37-r1)
(6/50) Installing freetype (2.10.1-r1)
(7/50) Installing mkfontscale (1.2.1-r1)
(8/50) Installing expat (2.2.9-r1)
(9/50) Installing libuuid (2.34-r1)
(10/50) Installing fontconfig (2.13.1-r2)
(11/50) Installing pkgconf (1.6.3-r0)
(12/50) Installing util-macros (1.19.2-r1)
(13/50) Installing font-misc-misc (1.1.2-r1)
(14/50) Installing font-cursor-misc (1.0.3-r1)
(15/50) Installing xkeyboard-config (2.28-r0)
(16/50) Installing libxau (1.0.9-r0)
(17/50) Installing libbsd (0.10.0-r0)
(18/50) Installing libxdmcp (1.1.3-r0)
(19/50) Installing libxcb (1.13.1-r0)
(20/50) Installing libx11 (1.6.12-r0)
(21/50) Installing libxkbfile (1.1.0-r0)
(22/50) Installing xkbcomp (1.4.2-r0)
(23/50) Installing libxext (1.3.4-r0)
(24/50) Installing libice (1.0.10-r0)
(25/50) Installing libsm (1.2.3-r0)
(26/50) Installing libxt (1.2.0-r0)
(27/50) Installing libxmu (1.1.3-r0)
(28/50) Installing xauth (1.1-r0)
(29/50) Installing libblkid (2.34-r1)
(30/50) Installing libmount (2.34-r1)
(31/50) Installing libsmartcols (2.34-r1)
(32/50) Installing findmnt (2.34-r1)
(33/50) Installing mcookie (2.34-r1)
(34/50) Installing xmodmap (1.0.10-r0)
(35/50) Installing mcpp-libs (2.7.2-r2)
(36/50) Installing mcpp (2.7.2-r2)
(37/50) Installing xrdb (1.2.0-r0)
(38/50) Installing xinit (1.4.1-r0)
(39/50) Installing mesa (19.2.7-r0)
(40/50) Installing libxdamage (1.1.5-r0)
(41/50) Installing libxfixes (5.0.3-r2)
(42/50) Installing libxxf86vm (1.1.4-r2)
(43/50) Installing libpciaccess (0.16-r0)
(44/50) Installing libdrm (2.4.100-r0)
(45/50) Installing mesa-glapi (19.2.7-r0)
(46/50) Installing libxshmfence (1.3-r0)
(47/50) Installing mesa-gl (19.2.7-r0)
(48/50) Installing libxfont2 (2.0.4-r0)
(49/50) Installing pixman (0.38.4-r0)
(50/50) Installing xvfb (1.20.6-r2)
Executing busybox-1.31.1-r9.trigger
Executing mkfontscale-1.2.1-r1.trigger
Executing fontconfig-2.13.1-r2.trigger
OK: 30 MiB in 66 packages
```

```bash
/app # /usr/bin/Xvfb :99 -screen 0 1024x768x24 > /dev/null 2>&1 &
/app # echo $?
0
```

```bash
/app # npm test

> vscode-gitlab-ci-demo@0.0.1 pretest /app
> npm run compile && npm run lint


> vscode-gitlab-ci-demo@0.0.1 compile /app
> tsc -p ./


> vscode-gitlab-ci-demo@0.0.1 lint /app
> eslint src --ext ts


> vscode-gitlab-ci-demo@0.0.1 test /app
> node ./out/test/runTest.js

Found .vscode-test/vscode-1.50.1. Skipping download.
Test error: Error: spawn /app/.vscode-test/vscode-1.50.1/VSCode-linux-x64/code ENOENT
Exit code:   -2
Done

Failed to run tests
npm ERR! Test failed.  See above for more details.
/app #

```

Hmm. Weird. Is it possible that something else is going wrong here? Hmm. Some
spawning issue.

https://duckduckgo.com/?q=vscode+test+in+linux+docker+container&t=ffab&ia=web

Hmm. Just to ensure everything is okay, I'm going to use a bigger docker image
which has a proper OS and a lot of files - run time libraries etc `node:12`

```bash
$ docker run -it --rm -v $(pwd):/app node:12 sh
# bash
root@1dd973029290:/# cd /app/
root@1dd973029290:/app# npm test

> vscode-gitlab-ci-demo@0.0.1 pretest /app
> npm run compile && npm run lint


> vscode-gitlab-ci-demo@0.0.1 compile /app
> tsc -p ./


> vscode-gitlab-ci-demo@0.0.1 lint /app
> eslint src --ext ts


> vscode-gitlab-ci-demo@0.0.1 test /app
> node ./out/test/runTest.js

Found .vscode-test/vscode-1.50.1. Skipping download.
/app/.vscode-test/vscode-1.50.1/VSCode-linux-x64/code: error while loading shared libraries: libX11-xcb.so.1: cannot open shared object file: No such file or directory

Exit code:   127
Done

Failed to run tests
npm ERR! Test failed.  See above for more details.
```

Better error. Different error with different error code / exit code. Hmm.

https://duckduckgo.com/?t=ffab&q=error+while+loading+shared+libraries%3A+libX11-xcb.so.1%3A+cannot+open+shared+object+file%3A+No+such+file+or+directory&ia=web

https://techoverflow.net/2018/06/05/how-to-fix-puppetteer-error-while-loading-shared-libraries-libx11-xcb-so-1-cannot-open-shared-object-file-no-such-file-or-directory/

https://askubuntu.com/questions/1123722/error-while-loading-shared-libraries-libx11-xcb-so-1-cannot-open-shared-objec

```bash
$ apt update
$ apt install libx11-xcb1
root@1dd973029290:/app# npm test

> vscode-gitlab-ci-demo@0.0.1 pretest /app
> npm run compile && npm run lint


> vscode-gitlab-ci-demo@0.0.1 compile /app
> tsc -p ./


> vscode-gitlab-ci-demo@0.0.1 lint /app
> eslint src --ext ts


> vscode-gitlab-ci-demo@0.0.1 test /app
> node ./out/test/runTest.js

Found .vscode-test/vscode-1.50.1. Skipping download.
/app/.vscode-test/vscode-1.50.1/VSCode-linux-x64/code: error while loading shared libraries: libxcb-dri3.so.0: cannot open shared object file: No such file or directory

Exit code:   127
Done

Failed to run tests
npm ERR! Test failed.  See above for more details.
```

```bash
$ apt install -y gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget
```

```bash
root@1dd973029290:/app# node ./out/test/runTest.js
Found .vscode-test/vscode-1.50.1. Skipping download.
Fontconfig warning: "/etc/fonts/fonts.conf", line 100:
unknown element "blank"


/dev/fd/3: No such file or directory

Server response:

Exit code:   null
Done

Failed to run tests
```

Maybe I need to do the xvfb thingy? I didn't do it in this docker container.
Right...

```bash
root@1dd973029290:/app# /usr/bin/Xvfb :99 -screen 0 1024x768x24 > server.log 2>&1 &
[1] 1750
root@1dd973029290:/app# less server.log
bash: less: command not found
[1]+  Exit 127                /usr/bin/Xvfb :99 -screen 0 1024x768x24 > server.log 2>&1
root@1dd973029290:/app# cat server.log
bash: /usr/bin/Xvfb: No such file or directory
```

```bash
$ apt install xvfb
```

```bash
$ /usr/bin/Xvfb :99 -screen 0 1024x768x24 > server.log 2>&1 &
root@1dd973029290:/app# ps aux
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.0  0.0   4280   712 pts/0    Ss   03:54   0:00 sh
root         7  0.0  0.2  18216  3220 pts/0    S    03:54   0:00 bash
root      1889  0.3  2.3 203956 35344 pts/0    Sl   04:07   0:00 /usr/bin/Xvfb :99 -screen 0 1024x768x24
root      1897  0.0  0.1  36636  2780 pts/0    R+   04:08   0:00 ps aux
root@1dd973029290:/app# node ./out/test/runTest.js
Found .vscode-test/vscode-1.50.1. Skipping download.
Fontconfig warning: "/etc/fonts/fonts.conf", line 100: unknown element "blank"

Exit code:   1
Done

Failed to run tests
```

```bash
root@1dd973029290:/app# node ./out/test/runTest.js
Found .vscode-test/vscode-1.50.1. Skipping download.
Fontconfig warning: "/etc/fonts/fonts.conf", line 100:
unknown element "blank"


/dev/fd/3: No such file or directory

Server response:

Exit code:   null
Done

Failed to run tests
```

OHH. I had to set the `DISPLAY` environment variable too. Hmm. I noticed this
in the pipeline examples too. Hmm. Or we could try running `xvfb-run`

```bash
$ export DISPLAY=':99.0'
root@1dd973029290:/app# node ./out/test/runTest.js
Found .vscode-test/vscode-1.50.1. Skipping download.
Fontconfig warning: "/etc/fonts/fonts.conf", line 100: unknown element "blank"

ATTENTION: default value of option force_s3tc_enable overridden by environment.

Warning: 'sandbox' is not in the list of known options, but still passed to Electron/Chromium.

[main 2020-10-25T04:11:27.874Z] update#setState idle

(node:2240) Electron: Loading non-context-aware native module in renderer: '/app/.vscode-test/vscode-1.50.1/VSCode-linux-x64/resources/app/node_modules.asar.unpacked/vscode-sqlite3/build/Release/sqlite.node'. This is deprecated, see https://github.com/electron/electron/issues/18397.

(node:2240) Electron: Loading non-context-aware native module in renderer: '/app/.vscode-test/vscode-1.50.1/VSCode-linux-x64/resources/app/node_modules.asar.unpacked/spdlog/build/Release/spdlog.node'. This is deprecated, see https://github.com/electron/electron/issues/18397.



  Extension Test Suite

    ✓ Sample test

  1 passing (40ms)

Exit code:   0
Done

$ unset DISPLAY
root@1dd973029290:/app# node ./out/test/runTest.js
Found .vscode-test/vscode-1.50.1. Skipping download.
Fontconfig warning: "/etc/fonts/fonts.conf", line 100:
unknown element "blank"


/dev/fd/3: No such file or directory

Server response:


Exit code:   null
Done

Failed to run tests
```

`xfvb-run` didn't work out

```bash
$ ps aux
$ # kill the existing xvfb server
$ kill <process-id>
root@1dd973029290:/app# xvfb-run -a node ./out/test/runTest.js
Found .vscode-test/vscode-1.50.1. Skipping download.
Fontconfig warning: "/etc/fonts/fonts.conf", line 100: unknown element "blank"

Warning: 'sandbox' is not in the list of known options, but still passed to Electron/Chromium.

[main 2020-10-25T04:13:35.925Z] update#setState idle

/dev/fd/3: No such file or directory

Server response:

/dev/fd/3: No such file or directory

Server response:

/dev/fd/3: No such file or directory

Server response:

/dev/fd/3: No such file or directory

Server response:

/dev/fd/3: No such file or directory

Server response:

[2832:1025/041337.058052:FATAL:gpu_data_manager_impl_private.cc(439)] GPU process isn't usable. Goodbye.
/dev/fd/3: No such file or directory

Server response:

/dev/fd/3: No such file or directory

Server response:

(node:2886) Electron: Loading non-context-aware native module in renderer: '/app/.vscode-test/vscode-1.50.1/VSCode-linux-x64/resources/app/node_modules.asar.unpacked/vscode-sqlite3/build/Release/sqlite.node'. This is deprecated, see https://github.com/electron/electron/issues/18397.

/dev/fd/3: No such file or directory

Server response:

Exit code:   null
Done

Failed to run tests
```

Maybe the server was needed? Idk. I also tried without the `-a` for
experimenting

```bash
$ xvfb-run node ./out/test/runTest.js
```

Anyways. Now I know how to run inside a docker container ;) :D

Wow. Now it all runs :D :D

https://gitlab.com/karuppiah7890/vscode-gitlab-ci-demo/-/jobs/810438721

So now I can run it inside a linux machine (docker container). Now what? I
should try windows machine next with GitLab CI :)

---

https://docs.gitlab.com/ee/development/windows.html
https://docs.gitlab.com/ee/development/windows.html#shared-windows-runners

https://about.gitlab.com/blog/2020/01/21/windows-shared-runner-beta/#getting-started

Windows config
https://docs.gitlab.com/ee/user/gitlab_com/#windows-shared-runners-beta

```yml
.shared_windows_runners:
  tags:
    - shared-windows
    - windows
    - windows-1809

windows-test:
  extends:
    - .shared_windows_runners
  script:
    - npm install
    - npm test
```

I got the same error in GitLab CI that I got in Travis CI.

https://gitlab.com/karuppiah7890/vscode-gitlab-ci-demo/-/jobs/810502383

Same exit code - `3221225781`

---

Moving on to CircleCI, wow, CircleCI does NOT have support for GitLab. Hmm.

https://circleci.com/docs/2.0/migrating-from-gitlab/#source-control-setup

They are asking me to migrate from GitLab.

Well, not going to migrate from GitLab as of now. So, moving on.

---

Next to try is AppVeyor

```
 An application called AppVeyor CI is requesting access to your GitLab account. This application was created by AppVeyor CI. Please note that this application is not provided by GitLab and you should verify its authenticity before allowing access.

This application will be able to:

    Access the authenticated user's API
    Grants complete read/write access to the API, including all groups and projects, the container registry, and the package registry.
```

https://www.appveyor.com/docs/
https://www.appveyor.com/docs/build-configuration/

```yml
version: 1.0.{build}

install:
  - npm install

test_script:
  - npm test
```

Okay, it failed

https://ci.appveyor.com/project/karuppiah7890/vscode-appveyor-ci-demo/builds/35940209

```
The build phase is set to "MSBuild" mode (default), but no Visual Studio project or solution files were found in the root directory. If you are not building Visual Studio project switch build mode to "Script" and provide your custom build command.
```

https://www.appveyor.com/docs/build-configuration/#build-mode

I have nothing to build actually. I could set it to off, or use `Script` but
I'm not sure if it will throw error if there's no `build_script`. Let me
switch it off for now :)

Okay, that was better.

But still a failure.

https://ci.appveyor.com/project/karuppiah7890/vscode-appveyor-ci-demo/builds/35940221

```log
[00:00:00] Build started
[00:00:00] git clone -q --branch=main https://gitlab.com/karuppiah7890/vscode-appveyor-ci-demo.git C:\projects\vscode-appveyor-ci-demo
[00:00:01] git checkout -qf 441c177c8a5a4561b57d89309b6cfe8323e8d4d1
[00:00:01] Running Install scripts
[00:00:01] npm install
[00:00:11] npm WARN vscode-appveyor-ci-demo@0.0.1 No repository field.
[00:00:11] npm WARN vscode-appveyor-ci-demo@0.0.1 No license field.
[00:00:11] npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@2.1.3 (node_modules\fsevents):
[00:00:11] npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.1.3: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"ia32"})
[00:00:11]
[00:00:11] added 213 packages from 155 contributors and audited 216 packages in 6.375s
[00:00:11]
[00:00:11] 27 packages are looking for funding
[00:00:11]   run `npm fund` for details
[00:00:11]
[00:00:11] found 0 vulnerabilities
[00:00:11]
[00:00:11] npm test
[00:00:12]
[00:00:12] > vscode-appveyor-ci-demo@0.0.1 pretest C:\projects\vscode-appveyor-ci-demo
[00:00:12] > npm run compile && npm run lint
[00:00:12]
[00:00:13]
[00:00:13] > vscode-appveyor-ci-demo@0.0.1 compile C:\projects\vscode-appveyor-ci-demo
[00:00:13] > tsc -p ./
[00:00:13]
[00:00:17]
[00:00:17] > vscode-appveyor-ci-demo@0.0.1 lint C:\projects\vscode-appveyor-ci-demo
[00:00:17] > eslint src --ext ts
[00:00:17]
[00:00:17] C:\projects\vscode-appveyor-ci-demo\node_modules\eslint\bin\eslint.js:93
[00:00:17]         } catch {
[00:00:17]                 ^
[00:00:17]
[00:00:17] SyntaxError: Unexpected token {
[00:00:17]     at createScript (vm.js:80:10)
[00:00:17]     at Object.runInThisContext (vm.js:139:10)
[00:00:17]     at Module._compile (module.js:617:28)
[00:00:17]     at Object.Module._extensions..js (module.js:664:10)
[00:00:17]     at Module.load (module.js:566:32)
[00:00:17]     at tryModuleLoad (module.js:506:12)
[00:00:17]     at Function.Module._load (module.js:498:3)
[00:00:17]     at Function.Module.runMain (module.js:694:10)
[00:00:17]     at startup (bootstrap_node.js:204:16)
[00:00:17]     at bootstrap_node.js:625:3
[00:00:17] npm ERR! code ELIFECYCLE
[00:00:17] npm ERR! errno 1
[00:00:17] npm ERR! vscode-appveyor-ci-demo@0.0.1 lint: `eslint src --ext ts`
[00:00:17] npm ERR! Exit status 1
[00:00:17] npm ERR!
[00:00:17] npm ERR! Failed at the vscode-appveyor-ci-demo@0.0.1 lint script.
[00:00:17] npm ERR! This is probably not a problem with npm. There is likely additional logging output above.
[00:00:17]
[00:00:17] npm ERR! A complete log of this run can be found in:
[00:00:17] npm ERR!     C:\Users\appveyor\AppData\Roaming\npm-cache\_logs\2020-10-25T08_14_11_141Z-debug.log
[00:00:17] npm ERR! Test failed.  See above for more details.
[00:00:17] Command exited with code 1
```

Oh. I missed to mention what version of NodeJs I need. Lol. :P I'm glad it had
npm ;)

https://duckduckgo.com/?t=ffab&q=eslint+windows%3A+SyntaxError%3A+Unexpected+token+%7B&ia=web

https://stackoverflow.com/questions/62636329/how-to-fix-unexpected-token-in-eslint

I guess I need to use the NodeJs LTS version first

https://www.appveyor.com/docs/lang/nodejs-iojs/#selecting-nodejs-or-iojs-version

https://www.appveyor.com/docs/windows-images-software/#node-js

Okay, so it has v8.x by default it seems

```yml
environment:
  nodejs_version: "LTS"
```

Looks like that's now how it works. Hmm

Same error

https://ci.appveyor.com/project/karuppiah7890/vscode-appveyor-ci-demo/builds/35940259

I'm going to check what version of node and npm are running first :)
Travis usually shows this on it's own, and also about what worker the build is
running on, the environment details and all. Cool stuff :)

Ohhh. That was just simple environment variable. Damn me. Didn't read this
well

https://www.appveyor.com/docs/lang/nodejs-iojs/

```yml
# Test against the latest version of this Node.js version
environment:
  nodejs_version: "6"

# Install scripts. (runs after repo cloning)
install:
  # Get the latest stable version of Node.js or io.js
  - ps: Install-Product node $env:nodejs_version
  # install modules
  - npm install

# Post-install test scripts.
test_script:
  # Output useful info for debugging.
  - node --version
  - npm --version
  # run tests
  - npm test

# Don't actually build.
build: off
```

Clearly I need to do that.

I did see this part
https://www.appveyor.com/docs/lang/nodejs-iojs/#selecting-nodejs-or-iojs-version

but didn't understand where to put it. Hmm. So it's a power shell command, and
hence the `ps`

So yeah, I'm still on v8.x

https://ci.appveyor.com/project/karuppiah7890/vscode-appveyor-ci-demo/builds/35940274

Let's get this fixed! :)

I should have read the effing manual as they say. RTFM is the short form. Lol.
Rude phrases. Hmm.

Wow!!!! The whole thing passed!!!! Yay!!! :D :D

https://ci.appveyor.com/project/karuppiah7890/vscode-appveyor-ci-demo/builds/35940317

---

I was checking out Semaphore CI and noticed they don't have support for windows
and only support Linux and Mac as of now

https://docs.semaphoreci.com/ci-cd-environment/machine-types/

---

I don't want to run integration tests for now. I'm going to comment them, and
remove appveyor integration, till I have some real proper integration tests to
run - because, as of now, there's no real integration tests! 😅

Okay, I have fixed that.

---

I have also added documentation for the CI pipeline.

---

Adding husky now.

https://github.com/typicode/husky
https://typicode.github.io/husky

```bash
$ npm install husky@next --save-dev
```

```bash
$ npx husky install
```

```json
{
  "scripts": {
    "postinstall": "husky install"
  }
}
```

```bash
$ npx husky add pre-push "npm test"
```

I also added `npm audit` as part of the `pretest` script as audit is run in CI
too and it was not part of `test`. So yeah, now everything was contained inside
`test` command ;) :D

I also added documentation for it. Also ensured that it does NOT run in CI using
`is-ci`

https://typicode.github.io/husky/#/?id=disable-hooks-in-ci

https://github.com/watson/is-ci
