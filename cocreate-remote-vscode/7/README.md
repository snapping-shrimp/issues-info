# Issue 7

So, I planned to go ahead with Jest for the framework.

https://jestjs.io/en/

https://jestjs.io/docs/en/getting-started.html

I ran this

```bash
$ npm install --save-dev jest
```

After this, I ran

```bash
$ jest --notify
```

This was still using the global `jest` though. And this gave a big error! Just
to ensure I always use the right version of `jest`, matching the one in the
`package.json`, I ran again using `npx jest --notify` for the habit, I did know
that it will still fail :|

```bash
$ npx jest --notify
 FAIL  src/test/suite/extension.test.ts
  ● Test suite failed to run

    Jest encountered an unexpected token

    This usually means that you are trying to import a file which Jest cannot parse, e.g. it's not plain JavaScript.

    By default, if Jest sees a Babel config, it will use that to transform your files, ignoring "node_modules".

    Here's what you can do:
     • To have some of your "node_modules" files transformed, you can specify a custom "transformIgnorePatterns" in your config.
     • If you need a custom transformation specify a "transform" option in your config.
     • If you simply want to mock your non-JS modules (e.g. binary assets) you can stub them out with the "moduleNameMapper" config option.

    You'll find more details and examples of these config options in the docs:
    https://jestjs.io/docs/en/configuration.html

    Details:

    /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/src/test/suite/extension.test.ts:1
    ({"Object.<anonymous>":function(module,exports,require,__dirname,__filename,global,jest){import * as assert from 'assert'; // You can import and use all API from the 'vscode' module
                                                                                             ^^^^^^

    SyntaxError: Cannot use import statement outside a module

      at Runtime.createScriptFromCode (node_modules/jest-runtime/build/index.js:1295:14)

 FAIL  out/test/suite/extension.test.js
  ● Test suite failed to run

    Cannot find module 'vscode' from 'out/test/suite/extension.test.js'

      3 | // You can import and use all API from the 'vscode' module
      4 | // as well as import your extension to test it
    > 5 | import * as vscode from 'vscode';
        | ^
      6 | // import * as myExtension from '../../extension';
      7 |
      8 | suite('Extension Test Suite', () => {

      at Resolver.resolveModule (node_modules/jest-resolve/build/index.js:306:11)
      at Object.<anonymous> (src/test/suite/extension.test.ts:5:1)

Test Suites: 2 failed, 2 total
Tests:       0 total
Snapshots:   0 total
Time:        1.5 s
Ran all test suites.
```

So, since I'm using TypeScript, and there are some compiled files too in `out`,
`jest` ran both of them. So, now I'm trying to figure out what to do.

The TypeScript file test error - I can understand, as `jest` is not able to
understand it. But I don't get why it was not able to find the module `vscode`
when running the JavaScript file, even though all modules have been installed
and I also double checked by running `npm install`

And there's something about using TypeScript

https://jestjs.io/docs/en/getting-started#using-typescript

I should probably ignore `out` directory from `jest` so that it does not run
tests twice.

I'm not already using Babel. And Jest is also mentioning about using ts-jest in
case we want type-check

The alternative could also be like - ignore `src` directory and only consider
`out` directory with the compiled files. Hmm

---

About the module `vscode` not found - that's a valid error. I just realized that
`package.json` does NOT have `vscode` as a dependency. Hence the error by `jest`

The problem is that, that is running integration tests, with the `vscode`
module and that is run using `vscode-test` module, which takes up some test
files path and executes it. Internally, `mocha` is getting used. I think I'll
however have to ignore the integration tests part of it, and have to run it
differently or integrate it with `jest` somehow. Gotta see what people are
already doing

Probably there's some `jest` plugin for it? Maybe 🤷‍♂️

Perfect! I got it! It's called Jest Test Runner.

https://github.com/search?utf8=%E2%9C%93&q=jest%20vs%20code%20extension -
didn't help

https://github.com/search?q=jest+vs+code - helped :P :D

https://github.com/bmealhouse/vscode-jest-test-runner

Checking the Jest docs for runners, I get this

https://jestjs.io/docs/en/configuration#runner-string

Perfect search I guess - https://github.com/search?q=jest+test+runner+vs+code

I'm not sure which is the best though. Some are in TypeScript and JavaScript.
Hmm

Okay wait. Apparently some of them run normal Jest tests from VS Code. It's not
about running `vscode-test` tests through Jest.

Weirdly, the same user has created two repos for the same jest test runner I
think

https://github.com/bmealhouse/vscode-jest-test-runner
https://github.com/bmealhouse/jest-test-runner

Not sure. Or I might just ignore integration tests to start with and focus on
simple unit tests written in TypeScript and then come back to this! :) As this
seems more complicated! 😅 And I don't know if I'll write so much integration
tests to put a lot of effort first on this thing. But yeah, both need good
effort. Hmm.

---

So, I have created a basic config for jest

https://jestjs.io/docs/en/getting-started#additional-configuration

https://jestjs.io/docs/en/getting-started#generate-a-basic-configuration-file

```bash
$ npx jest --init

The following questions will help Jest to create a suitable configuration for your project

✔ Would you like to use Jest when running "test" script in "package.json"? … yes
✔ Would you like to use Typescript for the configuration file? … yes
✔ Choose the test environment that will be used for testing › node
✔ Do you want Jest to add coverage reports? … yes
✔ Which provider should be used to instrument code for coverage? › v8
✔ Automatically clear mock calls and instances between every test? … yes

✏️  Modified /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/package.json

📝  Configuration file created at /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/jest.config.ts
```

It's a very big file.

```typescript
# jest.config.ts
/*
 * For a detailed explanation regarding each configuration property and type check, visit:
 * https://jestjs.io/docs/en/configuration.html
 */

export default {
  // All imported modules in your tests should be mocked automatically
  // automock: false,

  // Stop running tests after `n` failures
  // bail: 0,

  // The directory where Jest should store its cached dependency information
  // cacheDirectory: "/private/var/folders/fg/55xcrj215gs2n9gnpz4077y40000gq/T/jest_dz",

  // Automatically clear mock calls and instances between every test
  clearMocks: true,

  // Indicates whether the coverage information should be collected while executing the test
  // collectCoverage: false,

  // An array of glob patterns indicating a set of files for which coverage information should be collected
  // collectCoverageFrom: undefined,

  // The directory where Jest should output its coverage files
  coverageDirectory: "coverage",

  // An array of regexp pattern strings used to skip coverage collection
  // coveragePathIgnorePatterns: [
  //   "/node_modules/"
  // ],

  // Indicates which provider should be used to instrument code for coverage
  coverageProvider: "v8",

  // A list of reporter names that Jest uses when writing coverage reports
  // coverageReporters: [
  //   "json",
  //   "text",
  //   "lcov",
  //   "clover"
  // ],

  // An object that configures minimum threshold enforcement for coverage results
  // coverageThreshold: undefined,

  // A path to a custom dependency extractor
  // dependencyExtractor: undefined,

  // Make calling deprecated APIs throw helpful error messages
  // errorOnDeprecated: false,

  // Force coverage collection from ignored files using an array of glob patterns
  // forceCoverageMatch: [],

  // A path to a module which exports an async function that is triggered once before all test suites
  // globalSetup: undefined,

  // A path to a module which exports an async function that is triggered once after all test suites
  // globalTeardown: undefined,

  // A set of global variables that need to be available in all test environments
  // globals: {},

  // The maximum amount of workers used to run your tests. Can be specified as % or a number. E.g. maxWorkers: 10% will use 10% of your CPU amount + 1 as the maximum worker number. maxWorkers: 2 will use a maximum of 2 workers.
  // maxWorkers: "50%",

  // An array of directory names to be searched recursively up from the requiring module's location
  // moduleDirectories: [
  //   "node_modules"
  // ],

  // An array of file extensions your modules use
  // moduleFileExtensions: [
  //   "js",
  //   "json",
  //   "jsx",
  //   "ts",
  //   "tsx",
  //   "node"
  // ],

  // A map from regular expressions to module names or to arrays of module names that allow to stub out resources with a single module
  // moduleNameMapper: {},

  // An array of regexp pattern strings, matched against all module paths before considered 'visible' to the module loader
  // modulePathIgnorePatterns: [],

  // Activates notifications for test results
  // notify: false,

  // An enum that specifies notification mode. Requires { notify: true }
  // notifyMode: "failure-change",

  // A preset that is used as a base for Jest's configuration
  // preset: undefined,

  // Run tests from one or more projects
  // projects: undefined,

  // Use this configuration option to add custom reporters to Jest
  // reporters: undefined,

  // Automatically reset mock state between every test
  // resetMocks: false,

  // Reset the module registry before running each individual test
  // resetModules: false,

  // A path to a custom resolver
  // resolver: undefined,

  // Automatically restore mock state between every test
  // restoreMocks: false,

  // The root directory that Jest should scan for tests and modules within
  // rootDir: undefined,

  // A list of paths to directories that Jest should use to search for files in
  // roots: [
  //   "<rootDir>"
  // ],

  // Allows you to use a custom runner instead of Jest's default test runner
  // runner: "jest-runner",

  // The paths to modules that run some code to configure or set up the testing environment before each test
  // setupFiles: [],

  // A list of paths to modules that run some code to configure or set up the testing framework before each test
  // setupFilesAfterEnv: [],

  // The number of seconds after which a test is considered as slow and reported as such in the results.
  // slowTestThreshold: 5,

  // A list of paths to snapshot serializer modules Jest should use for snapshot testing
  // snapshotSerializers: [],

  // The test environment that will be used for testing
  testEnvironment: "node",

  // Options that will be passed to the testEnvironment
  // testEnvironmentOptions: {},

  // Adds a location field to test results
  // testLocationInResults: false,

  // The glob patterns Jest uses to detect test files
  // testMatch: [
  //   "**/__tests__/**/*.[jt]s?(x)",
  //   "**/?(*.)+(spec|test).[tj]s?(x)"
  // ],

  // An array of regexp pattern strings that are matched against all test paths, matched tests are skipped
  // testPathIgnorePatterns: [
  //   "/node_modules/"
  // ],

  // The regexp pattern or array of patterns that Jest uses to detect test files
  // testRegex: [],

  // This option allows the use of a custom results processor
  // testResultsProcessor: undefined,

  // This option allows use of a custom test runner
  // testRunner: "jasmine2",

  // This option sets the URL for the jsdom environment. It is reflected in properties such as location.href
  // testURL: "http://localhost",

  // Setting this value to "fake" allows the use of fake timers for functions such as "setTimeout"
  // timers: "real",

  // A map from regular expressions to paths to transformers
  // transform: undefined,

  // An array of regexp pattern strings that are matched against all source file paths, matched files will skip transformation
  // transformIgnorePatterns: [
  //   "/node_modules/",
  //   "\\.pnp\\.[^\\/]+$"
  // ],

  // An array of regexp pattern strings that are matched against all modules before the module loader will automatically return a mock for them
  // unmockedModulePathPatterns: undefined,

  // Indicates whether each individual test should be reported during the run
  // verbose: undefined,

  // An array of regexp patterns that are matched against all source file paths before re-running tests in watch mode
  // watchPathIgnorePatterns: [],

  // Whether to use watchman for file crawling
  // watchman: true,
};
```

I'm going to check some of these configs and then remove the remaining I think.
I can configure the stuff later

https://jestjs.io/docs/en/configuration.html

I read something about `ts-node`. I ignored it. Now I see this error

```bash
$ npx jest --notify
Error: Jest: Failed to parse the TypeScript config file /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/jest.config.ts
  Error: Jest: 'ts-node' is required for the TypeScript configuration files. Make sure it is installed
Error: Cannot find module 'ts-node'
Require stack:
- /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/node_modules/jest-config/build/readConfigFileAndSetRootDir.js
- /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/node_modules/jest-config/build/index.js
- /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/node_modules/jest/node_modules/jest-cli/build/cli/index.js
- /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/node_modules/jest/node_modules/jest-cli/bin/jest.js
- /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/node_modules/jest/bin/jest.js
    at readConfigFileAndSetRootDir (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/node_modules/jest-config/build/readConfigFileAndSetRootDir.js:150:13)
```

Gotta install that. Also, about ignoring tests, I think I might use this for
now, temporarily atleast -

https://jestjs.io/docs/en/configuration#testpathignorepatterns-arraystring

---

One good thing is - I have created a separate jest configuration file. I have
read before that jest config can also be added in `package.json`. I read it now
too. I'm not going to do that, as `package.json` is actually part of the
extension. I didn't want to bundle more and more things into it. So, ignoring
jest config from `package.json` and keeping it separately in a separate file.

---

Okay, so now, I'm able to run jest :D and I get this

```bash
$ npx jest --notify
No tests found, exiting with code 1
Run with `--passWithNoTests` to exit with code 0
In /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
  580 files checked.
  testMatch: **/__tests__/**/*.[jt]s?(x), **/?(*.)+(spec|test).[tj]s?(x) - 2 matches
  testPathIgnorePatterns: /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/node_modules/, /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/src/test/suite, /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/out/test/suite - 576 matches
  testRegex:  - 0 matches
Pattern:  - 0 matches
```

But it's looking at a lot of files. Hmm. I don't want it to look at
`node_modules` and all and waste time. I think I should use include patterns eh?
Not sure.

---

So, I decided to actually look at existing extensions and how they do testing
using jest. And a perfect extension to look for was an extension created for
running jest tests! ;)

https://github.com/jest-community/vscode-jest

Okay, they run tests. But they do not have any integration tests from what I
see.

They are also keeping the test files separately. Hmm. Like, `src` for all the
source and `tests` for all the test, and they use `testRegex` to get all the
test files. And they use `ts-jest` for typescript stuff. All of their code is in
typescript

And they use js file for jest config and not ts file. By the way, in the
package.json, they mention publisher name and not publisher ID I think.
I can see the publisher name as `Orta` in the marketplace search results and in
the extension page, but publisher ID is `orta` I think as the extension ID is
`orta.vscode-jest`

There's also glob that jest config has, hmm. I could try that later too.

https://github.com/search?q=vs+code+extension

Next I'm checking Prettier VS Code

https://github.com/prettier/prettier-vscode

Prettier VS Code codebase has some nice extensions as recommendations -

Spell checking extension - `streetsidesoftware.code-spell-checker`
ESlint - `dbaeumer.vscode-eslint`
And itself, haha. Prettier - `esbenp.prettier-vscode`

I was checking their code. They don't even use Jest :P But they do have
integration tests! :) What they do is, they use the same code that VS Code gives
for integration tests. They use `mocha` and that's it. They also use some
workspace data or configuration while running integration tests.

I think what I can do is, first get rid of integration test suite being under
`src`. I can move it out of there and create a folder called `test` or
`integration-test` and then run the integration tests from there and let it use
`mocha` or anything. I'll not let `jest` even come into the picture this way,
till we get to integration test setup and what we need.

On moving things out of `src`, I get errors now for compilation

```bash
$ npm run test

> cocreate-remote@0.0.1 pretest /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> npm run compile && npm run lint


> cocreate-remote@0.0.1 compile /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> tsc -p ./

error TS6059: File '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/jest.config.ts' is not under 'rootDir' '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/src'. 'rootDir' is expected to contain all source files.

error TS6059: File '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/test/runTest.ts' is not under 'rootDir' '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/src'. 'rootDir' is expected to contain all source files.

error TS6059: File '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/test/suite/extension.test.ts' is not under 'rootDir' '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/src'. 'rootDir' is expected to contain all source files.

error TS6059: File '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/test/suite/index.ts' is not under 'rootDir' '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/src'. 'rootDir' is expected to contain all source files.


Found 4 errors.

npm ERR! code ELIFECYCLE
npm ERR! errno 2
npm ERR! cocreate-remote@0.0.1 compile: `tsc -p ./`
npm ERR! Exit status 2
npm ERR!
npm ERR! Failed at the cocreate-remote@0.0.1 compile script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     /Users/karuppiahn/.npm/_logs/2020-10-22T03_38_42_438Z-debug.log
npm ERR! code ELIFECYCLE
npm ERR! errno 2
npm ERR! cocreate-remote@0.0.1 pretest: `npm run compile && npm run lint`
npm ERR! Exit status 2
npm ERR!
npm ERR! Failed at the cocreate-remote@0.0.1 pretest script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     /Users/karuppiahn/.npm/_logs/2020-10-22T03_38_42_474Z-debug.log
```

Hmm.

Also, one thing to note is, the jest extension by Orta actually uses separate
directories for src and test, and for compilation, they use webpack. So, some
sort of magic happens there :P

I think I'm going to do the simplest thing. So, the default pattern for Jest to
find a test is, to look for the following pattern

regex pattern - `(/__tests__/.*|(\\.|/)(test|spec))\\.[jt]sx?$`

glob patterns - `[ "**/__tests__/**/*.[jt]s?(x)", "**/?(*.)+(spec|test).[jt]s?(x)" ]`

So, I was thinking that I should just rename the integration test files with
something like `extension.integration-test.js` instead of letting it be
`extension.test.js`. Cool right? :P Big names, but meh :P

I made the changes accordingly. Hmm. Now we just have one small problem.

```bash
$ npm test

> cocreate-remote@0.0.1 pretest /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> npm run compile && npm run lint


> cocreate-remote@0.0.1 compile /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> tsc -p ./

error TS6059: File '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/jest.config.ts' is not under 'rootDir' '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/src'. 'rootDir' is expected to contain all source files.


Found 1 error.

npm ERR! code ELIFECYCLE
npm ERR! errno 2
npm ERR! cocreate-remote@0.0.1 compile: `tsc -p ./`
npm ERR! Exit status 2
npm ERR!
npm ERR! Failed at the cocreate-remote@0.0.1 compile script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     /Users/karuppiahn/.npm/_logs/2020-10-22T03_54_40_460Z-debug.log
npm ERR! Test failed.  See above for more details.
```

We might have to ignore the jest config during compilation, or use js for it I
guess! :)

Cool! Things work now. I changed jest config to js file

```bash
$ npm test

> cocreate-remote@0.0.1 pretest /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> npm run compile && npm run lint


> cocreate-remote@0.0.1 compile /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> tsc -p ./


> cocreate-remote@0.0.1 lint /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> eslint src --ext ts


> cocreate-remote@0.0.1 test /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> npm run test:unit && npm run test:integration


> cocreate-remote@0.0.1 test:unit /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> jest


No tests found, exiting with code 1
Run with `--passWithNoTests` to exit with code 0
In /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
  585 files checked.
  testMatch: **/__tests__/**/*.[jt]s?(x), **/?(*.)+(spec|test).[tj]s?(x) - 0 matches
  testPathIgnorePatterns: /node_modules/ - 585 matches
  testRegex:  - 0 matches
Pattern:  - 0 matches
npm ERR! code ELIFECYCLE
npm ERR! errno 1
npm ERR! cocreate-remote@0.0.1 test:unit: `jest`
npm ERR! Exit status 1
npm ERR!
npm ERR! Failed at the cocreate-remote@0.0.1 test:unit script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     /Users/karuppiahn/.npm/_logs/2020-10-22T03_58_55_902Z-debug.log
npm ERR! Test failed.  See above for more details.
```

Now it's only a matter of writing some tests. I was thinking if I should use the
Jest VS Code extension to just give it a try :P And write tests with it. Hmm.
Probably after I'm done with this.

Now, for a sample test, I wrote this test

```typescript
//  sample.test.ts
test("adds 1 + 2 to equal 3", () => {
  expect(1 + 2).toBe(3);
});
```

Of course it said some errors in my VS Code editor. It assumed `test` was from
`mocha`, and didn't understand `expect` and I guess `toBe` too consequently. But
didn't say anything about it

On running, I got this error from typescript

```bash
$ npm run compile && npm run test:unit

> cocreate-remote@0.0.1 compile /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> tsc -p ./

src/sample.test.ts:2:3 - error TS2304: Cannot find name 'expect'.

2   expect(1 + 2).toBe(3);
    ~~~~~~


Found 1 error.

npm ERR! code ELIFECYCLE
npm ERR! errno 2
npm ERR! cocreate-remote@0.0.1 compile: `tsc -p ./`
npm ERR! Exit status 2
npm ERR!
npm ERR! Failed at the cocreate-remote@0.0.1 compile script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     /Users/karuppiahn/.npm/_logs/2020-10-22T04_04_05_944Z-debug.log
```

I'm going to do a check on how people use Typescript along with Jest :P :)

https://duckduckgo.com/?t=ffab&q=typescript+and+jest&ia=web

https://dev.to/muhajirdev/unit-testing-with-typescript-and-jest-2gln

So, it talks about using `ts-jest`

https://kulshekhar.github.io/ts-jest

and some benefits of using it over babel
https://kulshekhar.github.io/ts-jest/user/babel7-or-ts

I was also wondering about the typescript runner, if that's needed or not. Hmm.

https://duckduckgo.com/?t=ffab&q=jest+runner+typescript&ia=software

https://jestjs.io/docs/en/configuration#runner-string

https://github.com/azz/jest-runner-tsc

I tried out the typescript runner first. And it gave this error

```bash
$ jest
 FAIL  out/sample.test.js
  ● Test suite failed to run

    TypeError: Cannot read property 'fileName' of undefined

      at node_modules/jest-runner-tsc/dist/runTsc.js:78:28
          at Array.filter (<anonymous>)
      at runTsc (node_modules/jest-runner-tsc/dist/runTsc.js:77:106)

 FAIL  src/sample.test.ts
Cannot find name 'expect'.
  1 | test("adds 1 + 2 to equal 3", () => {
> 2 |   expect(1 + 2).toBe(3);
    |   ^^^^^^
  3 | });
  4 |
Test Suites: 2 failed, 2 total
Tests:       1 failed, 1 total
Snapshots:   0 total
Time:        4.275 s
Ran all test suites.
```

And it matched both my js and ts files. Hmm.

I was checking the article

https://dev.to/muhajirdev/unit-testing-with-typescript-and-jest-2gln

and this config instructions

https://kulshekhar.github.io/ts-jest/user/config/

I'm also activating notifications on test results. But what happens in CI
then? It can't notify there right? Hmm. How will it know if it's a CI or not.
Through env variables? Some may not have them. Hmm.

```bash
$ npm i -D ts-jest @types/jest
```

I also added preset as `ts-jest` in jest config

It finally works if I type `jest`

```bash
$ jest
● Validation Warning:

  Unknown option "coverageProvider" with value "v8" was found.
  This is probably a typing mistake. Fixing it will remove this message.

  Configuration Documentation:
  https://jestjs.io/docs/configuration.html

 PASS  out/sample.test.js
ts-jest[versions] (WARN) Module jest is not installed. If you're experiencing issues, consider installing a supported version (>=26.0.0 <27.0.0-0).
 PASS  src/sample.test.ts

Test Suites: 2 passed, 2 total
Tests:       2 passed, 2 total
Snapshots:   0 total
Time:        2.491s
Ran all test suites.
```

Now I gotta check if the whole thing runs, including typescript compilation
which gave error before. I think it will still give errors.

But wait, it runs both my Js and typescript files. Right.

```bash
$ npm run test:unit

> cocreate-remote@0.0.1 test:unit /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> jest

● Validation Warning:

  Unknown option "coverageProvider" with value "v8" was found.
  This is probably a typing mistake. Fixing it will remove this message.

  Configuration Documentation:
  https://jestjs.io/docs/configuration.html

 PASS  out/sample.test.js
ts-jest[versions] (WARN) Module jest is not installed. If you're experiencing issues, consider installing a supported version (>=26.0.0 <27.0.0-0).
 PASS  src/sample.test.ts

Test Suites: 2 passed, 2 total
Tests:       2 passed, 2 total
Snapshots:   0 total
Time:        1.086s, estimated 3s
Ran all test suites.
```

The tests run twice. Damn. But it's fast, yeah :)

Let me see how to fix that.

So, I need to fix, duplicate test runs, and also typescript compilation and also
some weird warning about coverage provider "v8", hmm. That was in fact an
automated thing! Wow! I just said choose one and said okay when they asked what
to use.

```bash
$ npm run test

> cocreate-remote@0.0.1 pretest /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> npm run compile && npm run lint


> cocreate-remote@0.0.1 compile /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> tsc -p ./

node_modules/@types/jest/index.d.ts:34:13 - error TS2403: Subsequent variable declarations must have the same type.  Variable 'beforeEach' must be of type 'HookFunction', but here has type 'Lifecycle'.

34 declare var beforeEach: jest.Lifecycle;
               ~~~~~~~~~~

  node_modules/@types/mocha/index.d.ts:2514:13
    2514 declare var beforeEach: Mocha.HookFunction;
                     ~~~~~~~~~~
    'beforeEach' was also declared here.

node_modules/@types/jest/index.d.ts:36:13 - error TS2403: Subsequent variable declarations must have the same type.  Variable 'afterEach' must be of type 'HookFunction', but here has type 'Lifecycle'.

36 declare var afterEach: jest.Lifecycle;
               ~~~~~~~~~

  node_modules/@types/mocha/index.d.ts:2532:13
    2532 declare var afterEach: Mocha.HookFunction;
                     ~~~~~~~~~
    'afterEach' was also declared here.

node_modules/@types/jest/index.d.ts:37:13 - error TS2403: Subsequent variable declarations must have the same type.  Variable 'describe' must be of type 'SuiteFunction', but here has type 'Describe'.

37 declare var describe: jest.Describe;
               ~~~~~~~~

  node_modules/@types/mocha/index.d.ts:2548:13
    2548 declare var describe: Mocha.SuiteFunction;
                     ~~~~~~~~
    'describe' was also declared here.

node_modules/@types/jest/index.d.ts:39:13 - error TS2403: Subsequent variable declarations must have the same type.  Variable 'xdescribe' must be of type 'PendingSuiteFunction', but here has type 'Describe'.

39 declare var xdescribe: jest.Describe;
               ~~~~~~~~~


  node_modules/@types/mocha/index.d.ts:2569:13
    2569 declare var xdescribe: Mocha.PendingSuiteFunction;
                     ~~~~~~~~~
    'xdescribe' was also declared here.

node_modules/@types/jest/index.d.ts:40:13 - error TS2403: Subsequent variable declarations must have the same type.  Variable 'it' must be of type 'TestFunction', but here has type 'It'.

40 declare var it: jest.It;
               ~~

  node_modules/@types/mocha/index.d.ts:2583:13
    2583 declare var it: Mocha.TestFunction;
                     ~~
    'it' was also declared here.
node_modules/@types/jest/index.d.ts:42:13 - error TS2403: Subsequent variable declarations must have the same type.  Variable 'xit' must be of type 'PendingTestFunction', but here has type 'It'.

42 declare var xit: jest.It;
               ~~~

  node_modules/@types/mocha/index.d.ts:2604:13
    2604 declare var xit: Mocha.PendingTestFunction;
                     ~~~
    'xit' was also declared here.

node_modules/@types/jest/index.d.ts:43:13 - error TS2403: Subsequent variable declarations must have the same type.  Variable 'test' must be of type 'TestFunction', but here has type 'It'.

43 declare var test: jest.It;
               ~~~~


  node_modules/@types/mocha/index.d.ts:2597:13
    2597 declare var test: Mocha.TestFunction;
                     ~~~~
    'test' was also declared here.


Found 7 errors.

npm ERR! code ELIFECYCLE
npm ERR! errno 2
npm ERR! cocreate-remote@0.0.1 compile: `tsc -p ./`
npm ERR! Exit status 2
npm ERR!
npm ERR! Failed at the cocreate-remote@0.0.1 compile script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     /Users/karuppiahn/.npm/_logs/2020-10-22T04_41_45_793Z-debug.log
npm ERR! code ELIFECYCLE
npm ERR! errno 2
npm ERR! cocreate-remote@0.0.1 pretest: `npm run compile && npm run lint`
npm ERR! Exit status 2
npm ERR!
npm ERR! Failed at the cocreate-remote@0.0.1 pretest script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     /Users/karuppiahn/.npm/_logs/2020-10-22T04_41_45_827Z-debug.log
```

Damn. That was a long error. Mostly related to jest and mocha types conflicting.
Hmm.

https://duckduckgo.com/?q=mocha+and+jest+typescript+definition+clash&t=ffab&ia=web

https://github.com/cypress-io/cypress/issues/6690

https://stackoverflow.com/questions/55680391/typescript-error-ts2403-subsequent-variable-declarations-must-have-the-same-typ

I guess I'll have to change the code somehow to use only jest and get rid of
mocha completely. Or I could do some hacks or different workarounds - ignore the
integration test files and run compile only for others for unit test. But even
then, the package.json has the types for both jest and mocha. That's
conflicting. Hmm

So, currently, the integration tests run inside the VS Code instance, and they
use mocha in a programmatic way. I started checking how to do the same with
jest

https://duckduckgo.com/?t=ffab&q=running+jest+programmatically&ia=web

https://github.com/facebook/jest/issues/5048

Apparently there's a package for this

https://www.npmjs.com/package/@jest/core

A way to run the CLI thing

https://medium.com/web-developers-path/how-to-run-jest-programmatically-in-node-js-jest-javascript-api-492a8bc250de

---

I simply tried to remove `@types/mocha`

```bash
$ npm run test

> cocreate-remote@0.0.1 pretest /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> npm run compile && npm run lint


> cocreate-remote@0.0.1 compile /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> tsc -p ./

src/test/suite/index.ts:4:1 - error TS1109: Expression expected.

4 import * as glob from 'glob';
  ~~~~~~


Found 1 error.

npm ERR! code ELIFECYCLE
npm ERR! errno 2
npm ERR! cocreate-remote@0.0.1 compile: `tsc -p ./`
npm ERR! Exit status 2
npm ERR!
npm ERR! Failed at the cocreate-remote@0.0.1 compile script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     /Users/karuppiahn/.npm/_logs/2020-10-22T05_49_50_521Z-debug.log
npm ERR! code ELIFECYCLE
npm ERR! errno 2
npm ERR! cocreate-remote@0.0.1 pretest: `npm run compile && npm run lint`
npm ERR! Exit status 2
npm ERR!
npm ERR! Failed at the cocreate-remote@0.0.1 pretest script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     /Users/karuppiahn/.npm/_logs/2020-10-22T05_49_50_555Z-debug.log
```

That's weird. I didn't get the error message.

Oh. It occurs even if I put back `@types/mocha`. I'm checking what happened.

Oh. I made a mistake in one of the `ts` files where I was trying to import
`jest` but didn't do it.

Okay, so removing `@tyeps/mocha` from `package.json` and also running `npm i` to
make sure it's removed from `node_modules`, after this I ran the test

```bash
$ npm run test

> cocreate-remote@0.0.1 pretest /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> npm run compile && npm run lint


> cocreate-remote@0.0.1 compile /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> tsc -p ./

src/test/suite/extension.integration-test.ts:8:1 - error TS2582: Cannot find name 'suite'. Do you need to install type definitions for a test runner? Try `npm i @types/jest` or `npm i @types/mocha`.

8 suite('Extension Test Suite', () => {
  ~~~~~

src/test/suite/index.ts:2:24 - error TS7016: Could not find a declaration file for module 'mocha'. '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/node_modules/mocha/index.js' implicitly has an 'any' type.
  Try `npm install @types/mocha` if it exists or add a new declaration (.d.ts) file containing `declare module 'mocha';`

2 import * as Mocha from 'mocha';
                         ~~~~~~~

src/test/suite/index.ts:25:15 - error TS7006: Parameter 'failures' implicitly has an 'any' type.

25                      mocha.run(failures => {
                                  ~~~~~~~~


Found 3 errors.
npm ERR! code ELIFECYCLE
npm ERR! errno 2
npm ERR! cocreate-remote@0.0.1 compile: `tsc -p ./`
npm ERR! Exit status 2
npm ERR!
npm ERR! Failed at the cocreate-remote@0.0.1 compile script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     /Users/karuppiahn/.npm/_logs/2020-10-22T05_53_12_566Z-debug.log
npm ERR! code ELIFECYCLE
npm ERR! errno 2
npm ERR! cocreate-remote@0.0.1 pretest: `npm run compile && npm run lint`
npm ERR! Exit status 2
npm ERR!
npm ERR! Failed at the cocreate-remote@0.0.1 pretest script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     /Users/karuppiahn/.npm/_logs/2020-10-22T05_53_12_598Z-debug.log
```

Like I thought, now it has errors for the ts files which use mocha. Hmm. Of
course.

Wow. This is quite a lot huh. Just because I got some typescript and some global
types in the environment because of jest and mocha. Hmm. And I also got an extra
use case of running integration tests in a different manner in a different
environment - VS Code instance. Hmm

And I need to ensure that I don't run the tests twice - one for typescript and
one for javascript. And also ensure I have type check enabled for my tests too
using `ts-jest`. Hmm.

https://stackoverflow.com/questions/50827216/use-jest-run-or-jest-runcli-to-run-all-tests-or-ways-to-run-jest-programmati

---

https://basarat.gitbook.io/typescript/intro-1/jest

---

I can't believe that Jest does NOT have a programmatic way to run tests. What
were they thinking not having a great API for programmatic access even for
internal access and for other projects to use Jest. OMG. Hmm. mocha has it! I
think that's cool! :) Makes me think if I really need Jest as of now.

I chose a lot of things at once.

TypeScript
Jest

And couldn't help but I do need the integration tests. And mocha came by
default. Hmm.

---

I just realized the main problem in the whole thing. I mean, looking at the
problems I'm facing

- Typescript throws error when I use both jest and mocha types
- Integration tests with mocha need to be run separately and need to be compiled
  first? As they are in Typescript
- Unit tests can be run with just jest without even using `tsc` command.
- Multiple test runs - both ts and js files running

Thinking about this, I was like -

- Can I run Typescript just for source code and integration tests and not for
  my unit tests? I still gotta see if I'll get the errors about the conflicting
  types - I think I'll, given they are defined in package.json and are present
  in node_modules. Hmm. But I do have to ignore unit tests from typescript
  compilation. This will also stop creating js files for the unit tests which
  jest picks up and runs too!
- Can I just keep the integration test as is in mocha and compile the typescript
  code and just run it as it is ?
- Can I let the unit tests be run just by jest without calling `tsc`?
- Try to avoid multiple test runs from ts and js files.

I think the main uncertainity is around Typescript error for conflicting types.
Hmm. I gotta try and succeed!! :D I could even use a separate project for the
tests, within this project for it :P If it comes to that XP. I have seen people
do it, with lerna and what not. Let's see. That's a totally different
complexity. Phew. It's like bringing in another problem. Trading problems in
short.

---

I was checking about multiple packages in a repo simply -
https://github.com/forcedotcom/salesforcedx-vscode

They actually have too many packages hence this model I guess. Also, given a
package - an extension - they use a combination of mocha, chai etc

https://github.com/forcedotcom/salesforcedx-vscode/blob/develop/packages/salesforcedx-vscode-visualforce/package.json

---

https://medium.com/@admin_86118/testing-typescript-node-with-jest-6bf5db18119c

---

Okay. Now. Realization. The craziest problem is - global types!! :O I mean, I
was okay with this, but this creates biiiig problems. Hmm. Why can't they just
let people have easier lives. No idea. Anyways, since mocha and jest fight each
other for global types, I'm going to go with Ava or probably something simpler
which does NOT have globals and use it for integration test at least! :) I even
thought if I should use Jest - but I think it's okay. It's for the best for the
future to have a lot of power :P 😅

---

https://stackoverflow.com/questions/29050720/run-mocha-programmatically-and-pass-results-to-variable-or-function

https://medium.com/javascript-in-plain-english/introduction-to-programmatic-approach-to-mocha-testing-framework-e10d1947148e

---

https://raygun.com/blog/javascript-unit-testing-frameworks/

---

I realized that I could just use https://github.com/substack/tape

It can run with plain node. I just need to require the test file and that's it.
Nice!! :)

https://github.com/substack/tape#usage

Perfect use case! It has a binary too, very simple one, and I'm copying just one
line of the code, or maybe referring to it!! :)

https://github.com/substack/tape/blob/master/bin/tape#L54

OMG. Even this is not so easy as I thought. Somehow some weird things are happening. I'm not
sure if the tests even run and they don't run well on CLI and don't show errors when there
are errors. Same with running inside VS Code. I'm wondering if the problem is about not
waiting for the test to complete. Somehow it feels like that's the problem.

Actually, when running from command line, the test runs I think? But it doesn't show any
console log output and always passes and has exit code 0. Hmm.

---

I was trying to make tape work and I just noticed that I DO have a tsconfig.json already,
lolol. Hmm

```json
{
        "compilerOptions": {
                "module": "commonjs",
                "target": "es6",
                "outDir": "out",
                "lib": [
                        "es6"
                ],
                "sourceMap": true,
                "rootDir": "src",
                "strict": true   /* enable all strict type-checking options */
                /* Additional Checks */
                // "noImplicitReturns": true, /* Report error when not all code paths in function return a value. */
                // "noFallthroughCasesInSwitch": true, /* Report errors for fallthrough cases in switch statement. */
                // "noUnusedParameters": true,  /* Report errors on unused parameters. */
        },
        "exclude": [
                "node_modules",
                ".vscode-test"
        ]
}
```

---

Hey!!! :D I saw something about this conflict thingy 

https://stackoverflow.com/questions/56181799/how-to-use-mocha-and-jest-with-typescript-without-conflicts

https://github.com/facebook/jest/pull/8571

https://github.com/facebook/jest/issues/8570

Damn it. I guess I couldn't find a solution there too. Wondering if I should get rid of jest
and just use mocha and chai and sinon, which is apparently a famous combination. I read that
jest is easy with almost 0 config though. Hmm. Meh. Also that jest is good for big projects.
I don't know. Maybe this project may not be that big. Hmm.

---

Very old article though

https://andrew.codes/jest-vs-mocha-why-jest-wins

I had no idea about the automatic mocks until I saw some config named automock. Even now
idk what exactly it is

https://jestjs.io/docs/en/configuration.html#automock-boolean

I removed it from my config. I usually use things that I understand, as much as possible
at least :P Maybe I SHOULD use automock and if I face issues, then take it up from there.
Let's see. For now, not doing much. By default it's false. I guess I removed the commented
config :P

https://jestjs.io/docs/en/manual-mocks.html Some more stuff for mocks

Another older article

https://medium.com/@clint.brown/migrating-unit-tests-from-mocha-chai-sinon-karma-phantomjs-to-jest-c214df7f5262

https://blog.usejournal.com/jest-vs-mocha-whats-the-difference-235df75ffdf3

https://blog.logrocket.com/unit-testing-node-js-applications-using-mocha-chai-and-sinon/

https://npmcompare.com/compare/chai,jasmine,jest,karma,mocha

---

https://www.google.com/search?sxsrf=ALeKk00eYh0wVUNn0JCHXJVQixSU08fs0g%3A1603428250837&ei=ml-SX5fMMvPF4-EP74y9gAE&q=conflicting+global+types+packages+in+typescript&oq=conflicting+global+types+packages+in+typescript&gs_lcp=CgZwc3ktYWIQAzoECAAQR1D7_gVYkoYGYNOIBmgAcAJ4AIAB0AGIAd0JkgEFMC42LjGYAQCgAQGqAQdnd3Mtd2l6yAEIwAEB&sclient=psy-ab&ved=0ahUKEwiXgcHa88nsAhXz4jgGHW9GDxAQ4dUDCAw&uact=5

https://github.com/microsoft/TypeScript/issues/22331

---

An issue similar to mine

https://github.com/codeceptjs/CodeceptJS/issues/2055

One of solutions asks to use `skipLibCheck`

https://www.staging-typescript.org/docs/handbook/tsconfig-json.html
https://www.typescriptlang.org/tsconfig

https://www.typescriptlang.org/tsconfig#skipLibCheck

https://medium.com/@elenasufieva/handling-type-declarations-clash-in-typescript-b05b10723f47

https://github.com/Microsoft/TypeScript/issues/11437

That too didn't help. Hmm. For some reason, it still gave errors. Not the best way
though.

Okay, I made one mistake. I think I should reinstall the node_modules and also get rid
of `@jest/types`. And use `@types/jest`

https://www.npmjs.com/package/@types/jest

Oops. That was not the problem. Anyways, it was good to revert back to `@types/jest`.
I used the wrong name for overrides directory.

Okay, I tried both ways - creating override for jest - gave same 7 errors as before
I think. Then I tried override for mocha. Now I have different errors.

----

Finally I was able to make everything work!! :D :D

Typescript compiler only compiles source code and integration tests in typescript
files, and NOT unit test typescript files

Jest and ts-jest take care of compiling test typescript files and run them

Custom type definition for mocha and remove `@types/mocha`. Remove the conflicting
global type definitions from the custom definition file.

Custom definition file is a copy of the actual `@types/mocha` type definition with
small modifications - commented out definitions that is.

Even in the code, for mocha, it's better to use imported functions instead of
global functions - for better IDE support. If we use global functions without
importing, IDE will think of some global functions as Jest functions as we have
removed some of the global type definitions from Mocha as they conflict with
Jest global type definitions.

----

upgrade mocha to 8.2.0

remove ts-node

create docs about testing - unit and integration test code - m.suite
and suite, and test etc working but for better IDE support - m.suite
is key! To get type definitions etc

Mention about the custom type definition and manual maintenance and the
hope that it won't change much. But if we change mocha version, upgrade the
type definition with the commented global types for only the conflicting
global types. Damn thing

