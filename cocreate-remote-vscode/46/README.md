# Issue 46

https://gitlab.com/snapping-shrimp/cocreate-remote-vscode/-/issues/46

I think the next thing to build is the whole collaboration. But we could split
it in parts. But we need to analyze the idea first.

When I say collaboration, I mean - one user is able to send data to another
user. That's it. I don't want collaborative editing yet. It would be nice, but
it would be a bit complicated too. But I want to start with something simple.
I hope it's all a bit iterative and the code I write don't go to waste. Hmm.

Now, I have been looking at what Atom Teletype does. If I analyze a bit, we can
understand what we need.

For now, I think a good architecture diagram would help and the set of
components we need. At least to get started!

In Atom Teletype, I read the Teletype Server code and noticed it using Pusher
service for publish-subscribe feature for the clients to talk to each other.
https://pusher.com/channels . It was specifically channels feature.

I just noticed in Atom Teletype extension that they use the Teletype client
in their `package.json`. I think they get all the work done using the client,
in terms of networking. Hmm.

The Teletype client `package.json` has Teletype CRDT as a dependency. Looks like
it is using CRDT package for the storage part too, for collaborative editing.
Hmm. I can check it out but I may not need it for now if all I want is to just
show the whole file to the other user, but without letting them edit it. I think
the current feature I'm thinking is almost as good as sharing a single file from
one place to another and then viewing it in real time - where changes to the
file are also seen. But it all happens inside VS Code. That's it.

Talking about file sharing, I noticed that there's a tool for file sharing in
Golang - croc -
https://github.com/schollz/croc , https://schollz.com/blog/croc6/

It's peer to peer and secure too. Hmm. Maybe for the initial sharing of the file
or files, I could use this too! Hmm. It's written in Golang, let's see. For now
I'm only focusing on one file. When it becomes multiple files, big directories,
especially git repositories, and if I need to bring in `.git` directory too,
then I would need a good mechanism to share the repo peer to peer. Torrent,
or something like croc. Let's see. I want it to be fast and secure too, and
mostly peer to peer as much as possible so that there's no server to maintain
really. :)

After initial transfer of files, I gotta see if CRDT can help with changes
across many files, and with ease. Let's see. I should just start simple for now
with one file and maybe even use webrtc itself to share the file and changes
too.

Also, I could use pusher, but I'm not sure what that brings in. It's also a paid
service. But there's free tier.

Also, for webrtc, I gotta check some stuff. I think it's not easy to webrtc -
as in, we will still need some servers. I don't know much, but atom gets it's
ICE servers from twilio. There are other kinds of servers too. I need to
understand what we need and what can implement and stuff.

This is a nice place to read about STUN, TURN and ICE. https://www.twilio.com/docs/stun-turn/faq Hmm.

https://www.twilio.com/docs/stun-turn

Looks like the TURN is kind of like a relay server and since all the data is
encrypted (I need to ensure this), TURN cannot decrypt it it seems. I think it's
end to end encryption? Gotta check

https://www.twilio.com/video/pricing
https://www.twilio.com/docs/video/tutorials/understanding-video-rooms#video-webrtc-go-rooms

It says 25GB/month of TURN server usage is included. The above is specifically
for Video using WebRTC though. We just need to send other data, like text data,
through WebRTC.

And old answer -
https://stackoverflow.com/questions/41298153/how-to-get-tcp-connection-candidate-from-twilio-stun-turn-server

Also, I noticed that in a simple request to the twilio can get a list of ICE
servers. Gotta check. Hmm

---

I was checking more about teletype, teletype client and teletype crdt. Mostly
skimming through code.

I noticed that they use protocol buffers in crdt package. They have some sort of
Document, and serialization and deserialization concept etc. And I saw something
called operations and all. Hmm.

It felt a bit complicated. I think I need to skim through slowly and understand
everything. I was also checking if I can find existing crdt packages. But yeah,
it may not be specifically for text editors. Hmm. I don't know how much the crdt
package is tied the atom editor. Maybe it's not really tied to the editor. I
noticed mostly general stuff in the proto and all. Hmm

I also saw some repos while doing a search now ;)

https://github.com/search?q=conflict+replicated+data+type

https://github.com/orbitdb/crdts

https://github.com/widmogrod/js-crdt
https://github.com/widmogrod/notepad-app

https://notepad-app.herokuapp.com/

https://github.com/search?l=JavaScript&q=conflict+replicated+data+type&type=Repositories

Anyways, CRDT stuff is needed only when I start doing collaborative editing.
Initially, I just need to transfer files from one place to another! That's it!
Hmm.

I need to get back on track 🙈

---

File transfer mechanisms / methods

https://github.com/schollz/croc
https://schollz.com/blog/croc6/

https://github.com/search?utf8=%E2%9C%93&q=file%20transfer%20package

https://github.com/perguth/peertransfer

https://github.com/search?l=JavaScript&q=file+transfer+package&type=Repositories

https://www.npmjs.com/search?q=file%20transfer

https://www.npmjs.com/package/simple-peer-files

https://webdrop.space/#/

https://www.npmjs.com/search?q=keywords:webrtc

https://www.npmjs.com/search?q=keywords:p2p

https://www.npmjs.com/search?q=keywords:peer-to-peer

https://www.npmjs.com/search?q=keywords:data%20channel%20stream

https://www.npmjs.com/package/@hyperswarm/network

https://github.com/hyperswarm

https://github.com/hyperswarm/hyperswarm

https://github.com/hyperswarm/hyperswarm/network/dependents?package_id=UGFja2FnZS0xMzQwMzI4MA%3D%3D

https://github.com/DougAnderson444/peerpiper.io
https://peerpiperio.douganderson444.now.sh/

---

Module for

- Showing a read only document to a user. New window or same window? Try both!! :) Check if intellisense works in such a file. Check if you are able to update the document programmatically. Check if file is accessible in file system for modification through other editors. Maybe in-memory file or temp file whose location is not known? Hmm. Check if user can keep cursor in the document but not be able to edit it. Only navigate like peek, go to definition etc if it's a standalone file. Hmm
- Two node processes sharing a file through WebRTC using a NodeJs server. Check p2p chat code and check NodeJs WebRTC vs browser WebRTC. Atom client may also have it. Hmm
- Create UUID. Big? Medium? Small? Check. Use UUID as room code or portal code or ID to join room.
- First person in room is asked if they want to give permission to the other person
- Max only two people in a room
- use the file sharing module in the extension. But check how to show other user's details to ask if it has to be allowed or not

---

I think I need to create a lot of analysis card first to understand some of the
things! :)

I'm planning to start analysis with readonly documents - document in a tab. And
also check how to have in-memory files or store files in a temp file and modify
it programmatically.

Goals:

- How to create documents programmatically? file is present in-memory or /tmp
  file or something
- How to modify documents programmatically?
- How to not allow the user to modify the document by allow them to use the
  cursor in the document and interact or navigate?
- Create documents in existing window and in a new window in a directory

I have created an issue for this - https://gitlab.com/snapping-shrimp/cocreate-remote-vscode/-/issues/47

I think another analysis would be about connectivity among the users, using
WebRTC and some bootstrapping method with a backend service.

And there could be a separate networking mechanism for the first file transfer
alone.Or even for incremenetal file transfer, for even transferring changes in
files the same mechanism could be used. And WebRTC data channels could just be
used for document / file changes. But it's all related. Hmm. Gotta check what
Atom does with respect to CRDT and how they share files and how they share the
changes. Hmm.

I'll create cards for these too

Done.
https://gitlab.com/snapping-shrimp/cocreate-remote-vscode/-/issues/48
https://gitlab.com/snapping-shrimp/cocreate-remote-vscode/-/issues/49

---

Some networking related links -

https://duckduckgo.com/?t=ffab&q=node-webrtc&ia=web

https://github.com/node-webrtc/node-webrtc
https://github.com/node-webrtc/node-webrtc-examples

Atom Teletype Client uses this -
https://www.npmjs.com/package/webrtc-adapter
https://github.com/webrtchacks/adapter
