# Issue 8

https://gitlab.com/snapping-shrimp/cocreate-remote-vscode/-/issues/8

I want to do all the linting, formatting stuff over here so that there's no
inconsistency in code or arguments about how code should look! :)

https://duckduckgo.com/?t=ffab&q=linting+and+formatting+in+typescript&ia=web

I even want to make sure that even warnings are errors and are not tolerable,
or else there's just warnings and no consequences based on warnings.

https://medium.com/javascript-in-plain-english/my-best-vscode-linting-formatting-configuration-for-typescript-projects-ef400ed9b78f

The article recommends a tool on top of TSLint and Prettier. Hmm. Actually
ESLint is the way to go, clearly. As TSLint and ESLint and trying to merge
now

https://palantir.github.io/tslint/
https://medium.com/palantir/tslint-in-2019-1a144c2317a9
https://github.com/palantir/tslint/issues/4534

https://code.visualstudio.com/api/advanced-topics/tslint-eslint-migration

In my project, since it was generated recently, it already has ESLint

```json
{
  "devDependencies": {
    "@typescript-eslint/eslint-plugin": "^4.1.1",
    "@typescript-eslint/parser": "^4.1.1",
    "eslint": "^7.9.0"
  }
}
```

And I have the following config already for ESLint

```json
{
  "root": true,
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "module"
  },
  "plugins": ["@typescript-eslint"],
  "rules": {
    "@typescript-eslint/naming-convention": "warn",
    "@typescript-eslint/semi": "warn",
    "curly": "warn",
    "eqeqeq": "warn",
    "no-throw-literal": "warn",
    "semi": "off"
  }
}
```

And I have already put recommendation in the project for the VS Code ESLint
extension

https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint

---

https://prettier.io/

https://prettier.io/playground/

---

There's also this thing I have heard - called Standard JS

https://standardjs.com

Right, so, standard uses ESLint under the hood. Hmm. And there's some
outstanding issue too

https://standardjs.com/index.html#typescript

https://github.com/standard/standard/issues/1283

---

https://advancedweb.dev/javascript-code-linters

https://eslint.org/docs/user-guide/getting-started

https://eslint.org/docs/rules/

https://eslint.org/docs/rules/semi

https://eslint.org/docs/rules/no-extra-semi

https://duckduckgo.com/?t=ffab&q=typescript+eslint&ia=software

https://github.com/typescript-eslint/typescript-eslint
https://typescript-eslint.io/

---

Maybe semicolons are not needed. Meh

https://duckduckgo.com/?t=ffab&q=javascript+semicolon&ia=web

https://news.codecademy.com/your-guide-to-semicolons-in-javascript/

https://dev.to/adriennemiller/semicolons-in-javascript-to-use-or-not-to-use-2nli

If I see any issues caused by missing of semicolons, I can think about it.

Till then, I'm just going to let eslint warn me about missing semicolons :)
It's not so hard to fix them, so meh. I don't want eslint to error out because
of semicolon.

---

https://prettier.io/docs/en/install.html

https://prettier.io/docs/en/ignore.html

https://prettier.io/docs/en/integrating-with-linters.html

---

https://www.robertcooper.me/using-eslint-and-prettier-in-a-typescript-project

https://khalilstemmler.com/blogs/tooling/prettier/

---

Based on Prettier's recommendations, I have chosen to NOT use prettier through
ESLint and instead invoke prettier on it's own. This way it's faster. Even for
VS Code extension, use a separate extension for Prettier. That's the
recommendation.

https://prettier.io/docs/en/integrating-with-linters.html#notes

https://prettier.io/docs/en/editors.html#visual-studio-code

https://github.com/prettier/prettier-vscode

https://github.com/prettier/prettier-vscode#run-prettier-through-linters

---

https://github.com/okonet/lint-staged

---

I also want to check what these "recommended rules" are. Not sure how they help
and if they are really needed and are good. Hmm. And it's very magical and
confusing. Hmm

```json
[
  "eslint:recommended",
  "plugin:@typescript-eslint/eslint-recommended",
  "plugin:@typescript-eslint/recommended",
  "plugin:jest/recommended",
  "plugin:prettier/recommended"
]
```

---

https://github.com/prettier/eslint-config-prettier#cli-helper-tool

I found this thing

```bash
$ npx eslint --print-config src/extension.ts | npx eslint-config-prettier-check
```

and removed a conflicting rule

---

https://github.com/okonet/lint-staged

---

I need to add prettier command as part of test somehow, through lint command
maybe.

And to also fix stuff.

And also add a git pre commit hook to automatically fix issues before
committing

And add recommended eslint rules

---

Okay, now it's not so magical.

https://eslint.org/docs/user-guide/configuring#extending-configuration-files

And `prettier/@typescript-eslint` is about this file

https://github.com/prettier/eslint-config-prettier/blob/master/@typescript-eslint.js

And this is about plugins -
https://eslint.org/docs/user-guide/configuring#configuring-plugins

I need to dig in and understand better the relationships. But I can see some
code now -

https://github.com/jest-community/eslint-plugin-jest#usage
https://github.com/jest-community/eslint-plugin-jest
https://github.com/jest-community/eslint-plugin-jest#recommended

```bash
$ npm install eslint-plugin-jest@latest --save-dev
```

---

I added the eslint recommended rules

Damn it. I gotta do so much for some clean code huh. Hmm.

https://code.visualstudio.com/api/get-started/extension-anatomy

I didn't need an empty function for deactivate. Removed it. It has to be either
doing something or should not be present according to ESLint. Docs said it's
not needed if there's no cleanup required :)

---

Because I added recommended rules, some of them conflict with prettier.

```bash
$ npx eslint --print-config src/extension.ts | npx eslint-config-prettier-check
The following rules are unnecessary or might conflict with Prettier:

- @typescript-eslint/no-extra-semi
- no-mixed-spaces-and-tabs

The following rules are enabled but cannot be automatically checked. See:
https://github.com/prettier/eslint-config-prettier#special-rules

- no-unexpected-multiline
```

---

Finally added all the stuff that's needed and also fixed some of the stuff. Hmm.
Crazy to use linters and formatters huh. No wonder Standard and other systems are
probably too cool! :)

---

https://duckduckgo.com/?t=ffab&q=prettier+and+editotconfig&ia=web

https://blog.theodo.com/2019/08/why-you-should-use-eslint-prettier-and-editorconfig-together/

https://blog.theodo.com/2019/08/empower-your-dev-environment-with-eslint-prettier-and-editorconfig-with-no-conflicts/

---

https://duckduckgo.com/?t=ffab&q=husky+and+lint+staged&ia=web

https://codeburst.io/continuous-integration-lint-staged-husky-pre-commit-hook-test-setup-47f8172924fc

https://asciinema.org/a/199934

https://github.com/okonet/lint-staged#examples

I tried it out and I got errors from prettier that it could not understand some
of the files to format it. I started ignoring it. Later, instead I made
lint-staged only include some files to be given to eslint.

I also made the mistake of including many extensions when using eslint and
prettier to fix it - even though only prettier can understand many files,
eslint can't. eslint can only understand typescript and javascript. I fixed
that too then!

```bash
$ eslint vsc-extension-quickstart.md

/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/vsc-extension-quickstart.md
  1:1  error  Parsing error: Invalid character

✖ 1 problem (1 error, 0 warnings)
```

So yeah.

```bash
$ npx prettier --write .editorconfig
[error] No parser could be inferred for file: .editorconfig
```

---

https://editorconfig.org/

---

While I was checking about lint-staged, I noticed this

https://mrm.js.org/

```bash
$ npx mrm lint-staged
```

That was actually installing an older version of husky. Anyways, didn't need
that since I had already set things up :)

---

Husky is apparently not exactly free for anything other than open source
projects. They have some different license for their v5 version which I use.

https://github.com/typicode/husky/blob/next/LICENSE
https://github.com/typicode/husky/blob/next/LICENSE-PARITY
https://github.com/typicode/husky/blob/next/LICENSE-MIT
https://github.com/typicode/husky/blob/next/LICENSE-PATRON

---

Editor Config End of Line config, checking with prettier

https://prettier.io/docs/en/options.html#end-of-line

https://www.git-scm.com/docs/gitattributes

https://stackoverflow.com/questions/21472971/what-is-the-purpose-of-text-auto-in-gitattributes-file

https://mirrors.edge.kernel.org/pub/software/scm/git/docs/gitattributes.html

https://www.appveyor.com/docs/appveyor-yml/

https://git-scm.com/docs/git-config#Documentation/git-config.txt-coreautocrlf

https://docs.travis-ci.com/user/customizing-the-build#git-end-of-line-conversion-control

---

https://editorconfig-specification.readthedocs.io/en/latest
https://editorconfig-specification.readthedocs.io/en/latest/#glob-expressions

https://duckduckgo.com/?q=editorconfig+tutorial&t=ffab&ia=web

https://matthewsetter.com/consistent-editing-with-editorconfig/


