# Setup

- Create OAuth App in GitHub developer settings
- Opt-in for Device Auth beta feature
- Get the client ID of the OAuth App

# Step 1

```bash
curl -i -X POST \
-H 'Content-Type: application/json' \
-H 'Accept: application/json' \
-d '{
    "client_id": ""
}' \
https://github.com/login/device/code
```

# Step 2

Login with user code

# Step 3

```bash
curl -i -X POST \
-H 'Content-Type: application/json' \
-d '{
    "client_id": "",
    "device_code": "",
    "grant_type": "urn:ietf:params:oauth:grant-type:device_code"
}' \
https://github.com/login/oauth/access_token
```

---

OAuth App - web flow vs device flow

device flow:
client code, client ID. no backend. no client secret.

---

Personal Access Token - only user has access to revoke tokens any time

vs

Device Flow - you can revoke tokens, user can also revoke app access

---

```bash
curl -i -X POST \
-H 'Content-Type: application/json' \
-H 'Accept: application/json' \
-d '{
    "client_id": "62d0d5f37ed738425fb4"
}' \
https://github.com/login/device/code
```

```json
{
  "device_code": "773cff6ac8a1c92eae69d8f4a477d32222300926",
  "user_code": "28CC-B7E9",
  "verification_uri": "https://github.com/login/device",
  "expires_in": 899,
  "interval": 5
}
```

```bash
curl -i -X POST \
-H 'Content-Type: application/json' \
-H 'Accept: application/json' \
-d '{
    "client_id": "62d0d5f37ed738425fb4",
    "device_code": "773cff6ac8a1c92eae69d8f4a477d32222300926",
    "grant_type": "urn:ietf:params:oauth:grant-type:device_code"
}' \
https://github.com/login/oauth/access_token
```

```json
{
  "access_token": "3bb9212ffba87ad758e4981455165502db28560b",
  "token_type": "bearer",
  "scope": ""
}
```

```bash
curl -i \
-H "Authorization: token 3bb9212ffba87ad758e4981455165502db28560b" \
-H 'Content-Type: application/json' \
-H 'Accept: application/json' \
https://api.github.com/user
```
