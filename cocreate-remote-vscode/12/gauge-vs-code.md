# Gauge VS Code

I'm checking out the tests here. Especially the integration tests.

The tests start here `test/runTest.ts`

and then inside VS Code, it starts here `test/index.ts` and there's also some
test data in `test/testdata` which is the workspace test data. There's a
sample project called `test/testdata/sampleProject`. Hmm. I started checking
all the `.test.ts` suffix files as they get converted to `.test.js` files and
are run by `test/index.ts` inside VS Code.

Looking at `test/cli.test.ts`, I wonder it is part of an integration test and
why it has to run inside VS Code and why it can't run as a simple unit test in
a standalone mode. From what I see in the `CLI` code, there's no VS code
related interactions. I wonder why it has to be tested inside VS Code then.
Hmm. Weird.

There is one test to check the activation of the plugin though. Over here
`test/gauge.test.ts` which is kind of based on VS Code and it's processing. So
yeah, that kind of looks like an appropriate test. Hmm. In my case, I think the
plugin is going to be always active, no matter what 😅 Probably I can think
about that slowly - so that plugin activates only when necessary and not always
as plugin activation also causes VS Code startup to slow down as all the active
plugins just start initializing etc. Maybe the plugin I build can activate when
an option from the command palette is shown. Hmm. Not sure if the extension has
to be active to show it's command palette options. Hmm. Something to learn and
check. Hmm.

I see them using `ts-mockito` package for mocking for Typescript. It mocks
classes! I have usually seen mocking libraries mock interfaces. Hmm

https://www.npmjs.com/package/ts-mockito . It's inspired by Mockito it seems.
Hmm. I have used mockito long ago a bit. :) With Java

I noticed that they use lot of VS Code related interactions. For example

`commands.executeCommand()` where they execute a command. They store commands
as constants! Nice! And the same commands are also present in the
`package.json`. I guess that duplication can't be helped, not sure though. Hmm.
So, they have some duplicate string literals in `package.json`, but in code,
there are only one set of commands - they are present as constants. They also
define the VS code built in commands to execute them I guess. :)

I believe it's basically like choosing a command palette option, but through the
API. I thought people would have some sort of visual thing, like - open
command palette, search for something, choose something. But I guess this too
is good. I mean, most of those command palette stuff are VS Code's core features
and are going to work. If it doesn't that's a different problem. We just need to
check if our extension works assuming that VS Code core works in general. :)

I guess one just has to be careful about the command names though. In these
tests, they use the constants, which define a string literal, which, if right,
will invoke some function and do something. If all these tests pass, even then,
it's possible that in the package.json we have the wrong string literal and
hence the right function may not get invoked or no function may be get invoked.
Hmm.

I guess something to check the commands would help ;) Let's see.

Basically, check if all the commands are bound to some function, hopefully the
right function. But hope is not a strategy, so, gotta think and also see how
others check it out. As it's very easy to make mistakes when things are
decoupled. :)
