# Atom Teletype Server

https://github.com/atom/teletype-server

So, it's deployed in Heroku. There's also some docs on how to deploy, for
contributors, in the `docs` directory

You can see the `Procfile` which tells the starting point of the app

There's a migration run - for running postgres DB migrations

```
release: npm run migrate up
```

`node-pg-migrate` package is used for this

https://www.npmjs.com/package/node-pg-migrate

`pg-migrate` is the command line binary that is installed with the package.

I guess `pg-migrate up` is the command that runs for running migrations

Then there's the server running config in `Procfile`

```
web: node script/server
```

There is a small file under `script/server` which uses a package called `throng`

https://www.npmjs.com/package/throng

Looks like it's like a monitoring thing which monitors and runs the process
and also has support for multiple instances - many servers or workers

And can handle OS signals it seems

And then there's the `startServer` in `index.js`

Many packages are used here. `dotenv` for `.env` env file for config I guess,
and then `newrelic` for New Relic Monitoring, and then `bugsnag` again some
monitoring thing

https://newrelic.com/

https://www.bugsnag.com/

And then there's the `Server` at `lib/server.js`

It has a constructor with tons of options -

- database URL for connecting to DB
- pusher service related config - https://pusher.com/, https://pusher.com/channels . https://www.npmjs.com/package/pusher
- twilio service related config - https://www.twilio.com/. I wonder for what.
- github related config. API URL, Client ID, Client Secret is all normal. But,
  I wonder what the OAuth Token is for
- some boomtown secret. For testing I think. There's also some endpoint called
  `/boomtown` which looks for that secret, to go boom 💥
- hashSecret - I noticed it being used in the model layer, I wonder for what.
  For storing hashed data? Idk
- port - usual server port

And then the server is started with `start()`

---

`start` method inside `Server` -

There's the initialization of `ModelLayer`, `IdentityProvider`, `PubSubGateway`

And then there's Twilio ICE Server. Idk about ICE, TURN etc. I have only heard
the acronym long ago. Usually I have noticed it in WebRTC conversations. Hmm

Then there's a controller layer, that's built, along with the ICE servers part,
and then the controller listens at the port

---

Controller

The Controller starts off with creating an `express` app

It uses some middlewares - `cors` package for CORS - Cross Origin Resource
Sharing

https://www.npmjs.com/package/cors

Then there's the request body parser, for JSON request body / payload. And the
limit for the size of the body / JSON is 1MB

There are two custom middlewares - `enforceProtocol` for enforcing some header
to contain `https`, not sure how that works. And another is `authenticate` to
authenticate any incoming requests with a custom header called
`github-oauth-token`. Some of the requests are ignored from authentication
though

Then there are lot of initialization of request handlers for different kind of
requests

- protocol version - I wonder what this is about. Maybe they use some custom
  protocol? Or may be more like a API version, for payload contract etc

- ice servers - this one fetches ICE servers from Twilio and gives it to the
  clients. Hmm

- `/peers/:id/signals` - to send a signal to a peer with a particular ID. This
  signal - is a message, and it's being sent using the pusher service pub sub
  gateway

- `/portals` - to create a portal. I think this is more like a room like
  concept in chat world. People join a room, and then they all chat. Or join a
  group. Hmm. Here it's a portal. This request handler also creates an event -
  more like an audit log I guess, for create portal action

- `/portals/:id` - to get a portal. Actually to get a portal's host - the host
  is also a peer, so, host peer, and their ID specifically, as that's the
  identifying property

- `/identity` - to get the identity of the user. I don't get why it has to
  send the request to the server though. Hmm. I mean, the client itself can
  do that request 🤷 Hmm. The client has the oauth token, it can do the same
  github API `/user` request. This is just delegating to the server for a very
  small thing. Hmm

- `/boomtown` to crash the server with a particular secret key

- `/_ping` to ping the server. It checks if everything is operational. I was
  wondering what's with the identity provider checking if things are operational
  by using it's own oauth token that was passed initially in the constructor for
  the server. I think what they are checking is that - is the github API service
  up and running and is it able to identify users. Hmm. Right. In this case,
  the provider just does a simple api call, to the root I think, not to `/user`.
  There's also check for model layer - to check if db query works - basically -
  are we able to connect to the DB and also run a basic query, here it's a
  `select`. It also checks if the pusher service is up by using a non
  existing channel, which is for test purposes I think. And for ICE server
  provider, it does a call to Twilio and checks if it passes. Hmm.

  So, this is the kind of ping that people do to check if the service AND it's
  dependencies are all working to tell that the service is working. Hmm.

---
