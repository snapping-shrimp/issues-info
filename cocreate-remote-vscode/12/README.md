# Issue 12

https://gitlab.com/snapping-shrimp/cocreate-remote-vscode/-/issues/12

---

https://github.com/atom/node-keytar storing secrets in OSes

Have a wrapper around this. And think about how you want to store and retrieve secrets as an application. And then store it and retrieve it.

For example, key can be GitHub ID, or email ID. Or just say account1. The basic details of account1 can be kept somewhere else. Or it can kept in a secret place too.

account1.email-id
account1.name
account1.secrets.github.token
account1.secrets.google.token

Or just
account1.github.token
account1.gtihub.id
account1.github.refresh-token
account1.google.token

Etc

And then store the values as the values to the appropriate fields. It's easy to add more accounts. We can add more details to, in case it's needed. Or even less details. Gotta see how to do discovery with just this. Hmm.

Once the key name is used, the next time I release, if I have changed the code for it, dead!! I'll try to read from new place but value won't be there, maybe because value exists in old place or no place at all. The latter is first time install. Anyways, then you would ask user to login again, or you could handle some sort of data migration in case of such cases. Too much work though. Every time there is a breaking change, you need to put all those migrations. So that data is in the right place and others are all deleted.

I should try to use the same key name and appropriate value always, as much as possible. Or go ahead with cleanups!!! :)

And cleanups and migrations must be a step by step thing from one version to another during releaaes, especially when there are breaking changes. If it's step by step, if someone install v1 and then upgrades to v10, all migrations will be taken care of. But that will be slow and weird. V1 to V2 and then V2 to V3. Etc. Hmm. Gotta see if direct migration can easily be done or not. Hmm. Idk. Let's see.
Ideally, migration has to happen more on in-memory and then final results in the target destination with proper data working and then finally remove the past data, after asking user after sometime that if everything is workinh.

Some sort of transformation from v1 to v10, like functional programming and pure inputs and outputs. And then store output from in-memory to database, which in our case is some file or OS key store or key chain. I don't think gpg, encryption etc will exactly help as we still need to store some privaye keys somewhere.

If VS Code is running online, we can have a different application for storing stuff like secrets through similar or same interface but different concrete definition or implementation! :)

The keys can also be

account1.keycloak.token
account1.server.token

But the Keycloak key is very server software specific. Server is more generic. Crazy part is, is this a generic server or auth server? Later we might have a separate server too. Hmm. One endpoint is easier than multiple endpoints. Hmm.

So

abc.com

With auth server at abc.com/auth ?

Or auth.abc.com ? Which is preferred? I wonder. Gotta check. Anyways. If it's token, I guess we can put it under server. Usually we use only one token. Given Keycloak and similar services, one token to do everything is the idea usually. Usually these are jwt tokens

---

Sign in with GitHub. I need some server for sure, right? What will it do?

And I need OAuth right? Or can people use personal access token and still prove their identity to the servers?

Different options
Custom built server. With Golang. With low level GitHub libraries or existing OAuth libraries like passportjs but in Golang. Or use NodeJs. Golang benefits - low memory foot print in general and less CPU needs too. Especially if I code well, a lot better. Hmm.

Use libraries like casbin

Use server software like Keycloak, ORY

Use third party services like firebase. But they are closed source and have free limits. Hmm.

What other services and software are there for GitHub login and other login?

Teletype server - GitHub login. Check what it does. Try to not reinvent the wheel.

Deployment of server? It's easier and cheaper to deploy simple software. Keycloak ain't simple.

Database? Keycloak has support for many. What do I choose? If any other software, what to choose? Postgres? It doesn't seem to be light weight to me. Sqlite is single instance DB. Redis? If all I need is just basic data about sessions, nothing too complex. Redis as DB and not cache. Store data with TTL maybe, for session timeout :p let's see

What kind of server software? Long running or serverless? ;) Given there will be very less users, serverless seems to be a nice thing! :)

Deployment platforms. Free - Heroku, Now, Glitch, Netlify, etc
Paid - digital ocean.

---

Product perspective. To start with, we can support just GitHub.

Later, if we want other signin, we need to see what to do.

For example, if our implementation is based on the tool we choose, for example Keycloak, then we are stuck with it. Later, if we change it, our client might have to login again, in case the auth data isn't migrated in the backend and even for that migration, the auth data has to be compatible.

So the question arises - how do we store this auth data in the front end? Hmm? Ideally it shouldn't change even if our implementation changes behind the scenes. But it's not that easy.

One approach is, we can allow the user to choose a different backend in case they want to login with a different account like Google etc. This will be the future strategy where we bring in some all in one backend especially for enterprises, like Keycloak.

For public users, if we just allow GitHub now, for some it might be hard I guess? Some may not want to do OAuth, even though we put email ID only as scope. People don't like giving away email ID. Not sure if we can try to get nothing. Hmm. But email ID could be useful. I mean, it's also a good identity apart from GitHub ID.

---

Security. Of course we need to use HTTPS.

Try to not reinvent the wheel. Learn how Atom teletype server does it.

Use libraries like passportjs, casbin and similar. Or use servers like ORY, Keycloak. Try to write as little code as possible.

---

https://github.com/ory/oathkeeper
https://www.ory.sh/oathkeeper

https://github.com/ory/keto
https://www.ory.sh/keto

https://www.ory.sh/kratos
https://www.ory.sh/kratos/docs/guides/sign-in-with-github-google-facebook-linkedin

https://github.com/ory/hydra
https://www.ory.sh/hydra

https://www.ory.sh/docs/index

https://www.beyondcorp.com/

Gluu. Open Source thing.
https://www.gluu.org/
https://www.gluu.org/pricing/
https://github.com/GluuFederation

---

Key Store stuff -

Mock out the key store stuff for unit tests. Any integration test needed? For Linux maybe we can do. Or even all OS. But this integration test will not be with vs code running. Different one.

Ensure you don't test the library itself. Only your code. Even with your code, if it's too simple, maybe leave it. Or. Write test. Meh ;)

---

IF Keycloak is chosen -

Keycloak VS Code Extension adapter. Providing modules for storing keys in OS keystore ring, refreshing keys.

What about the part where it has to redirect to a particular link? With a particular protocol, in VS Code Live Share it was vsls (VS Live Share? Idk). Gotta check if that is possible. Maybe I might have to use some front end? Maybe.

Maybe I don't need a backend at all and can use Keycloak? Maybe. This is for better security! :)

Check how Keycloak is for login with GitHub. Hmm. If I build stuff, it's just reinventing the wheel and not just that, I'm not some pro security guy! So yeah. I can't take care of everything. Unlike Keycloak. Or I can try other IAM Open Source servers too like the one from ORY

Maybe later, I can have some sort of adapter and I can let the user choose which they want to use. Hmm. Why would they want that. To connect to different backends maybe. Hmm. Or some backend should be configurable to connect to other different backends. Hmm. Gotta think about where to keep the complexity.

Keycloak and GitHub and NodeJs client. Existing Keycloak Js client has some
browser redirect and stuff I think. Not sure though. Hmm

---

Tasks at a high level

[ ] Research the different tools that I have in mind. ORY, Keycloak, Gluu,
Atom teletype server to name a few.

[ ] Research storing of secrets in Operating Systems

[ ] Research removal of secrets in Operating Systems when extension is
uninstalled from the system. Check if this is possible.

[ ] Research how browser can communicate with extension through a protocol,
similar to `vsls` (VS Code Live Share). Can we do `vscocreateremote` or
`vscocreate`? How secure is it? Can it go wrong? What can go wrong?

[ ] Research how extension can open a link in the browser

[ ] Research how to show indefinite progress bar

[ ] Research how to show data in the status bar

[ ] Research how to show data in the explorer area in a separate section

[ ] Think about where to show the data about the signed in user, for example
show name, or GitHub ID or email of the signed in user to show that they are
signed in

---

Researching ORY Kratos to start with.

https://www.ory.sh/

https://www.ory.sh/kratos/

https://www.ory.sh/kratos/docs/

https://www.ory.sh/kratos/docs/concepts/index/

---

Something to note for the alpha/beta version that we release for testing is -
we must not fill in the server URL in the extension. Let's keep it closed.
Maybe we could do one round of testing, security check with someone and think
about possible issues and then release the server to the public for basic
public use.

The public server is more like a demo - since we cannot handle too many people.
Gotta see if we really want a public instance for long term and to handle too
many people. We will need funds for that. Till then, it will be a basic demo,
for 10 or 15 minutes maybe? Let's start with that

---

There should be a setting where the user can mention the server URL. If it's
not mentioned, the extension should ask the user to give it as input or to put
it in the settings.

Handle errors and show it to the user in case the extension is not able to
access the server due to some issue.

---

We DONT want to store email IDs really - I mean, what do we do with it? Yes it
is a good identity, but what else is it good for in our case? We don't plan to
send emails or anything. We also don't want to be in the business or managing
user profiles, password recovery etc.

We just want to ensure that the user has an identity and that we can trust it
and the other users can trust it when they see the identity and when the
extension says "hey, this person wants to access your IDE. allow? or deny?".
If all we want is an identity that we can trust, we just need to ensure that
our authentication mechanism is good and secure and we can use services like
GitHub, GitLab, Twitter and others, where there's a concept of username or
similar, apart from email ID, which identifies users uniquely and is a good
identity, especially a good public identity, since people usually don't like
sharing email IDs, so that's more of a private identity.

For this issue, we will start with using GitHub as the identity. Make sure that
it's clear that it's only for GitHub in the implementation, so that, in the
future, we can have more services too if we want.

It could be a burden for the user in case they don't want to link the GitHub or
any other service and want a new account with it's own credentials. We can't
manage that as a public service, so we will not implement that initially. But
for private services, we can implement it and provide it, where people can host
it privately and use it.

---

Possible Security issues?

If someone has hacked into the server or has found the link to a session to join a session somehow, then, if they try to access some user's pairing session, they can access only if the user allows access. Now, the hacker can create a duplicate extension with same code but without the allow access feature. If the victim uses this along with the public server that we host, then the hacker is at an advantage.

Our extension should have some sort of identity. Only our published extension should be able to communicate with the server. No other extension should be able to access it.

Also, do mention in readme that people should check the server they connect to in their settings and the extension they use. Ideally they should not trust anything developed by anyone.

How will they trust the extension I build? Hmm. Can they build and use my extension? Sure, but what about communication with server? It needs that secret
identity. Hmm

Also, if this is the case - where we use a shared secret identity between the
published extension and the server, how can people use the published extension
with a private server? We can't share the identity with the private servers

Also, this is just a single shared secret identity for all installed extensions
and is also present in the server for checking. That's what I was thinking
initially. Also that it will of course not be part of the codebase and that it
will be injected during build time using a secret environment variable in the
build settings. But this is NOT viable for private servers that people might
wanna host.

Research on this. Check the possibility of such attacks. Check how people solve
the problem of fake clients - in our case, fake extensions. I have seen this
happen in mobile apps, where people create mod apps - not sure what's the
benefit - I think they game the system (backend service) somehow? Not sure

---

GitHub Sign In

We can't afford emails for public service. So we'll rely on third party services for login.

We'll just believe that your email ID has been verified by those platforms. Also, you and your friends know your friends ID in other platforms. Like GitHub ID or Google email ID for sign in with Google. Hmm.

Gotta check if ory kratos supports disabling it's email module and only support some things like GitHub login without basic stuff like registration and login with email or username or even advanced things like passwordless. Hmm. As I can't host email service or use external ones. Hmm

I can add 2FA though. That's just storing secret key in DB and asking user to
set it up. But for recovery, we can only provide offline backup codes. Can they
regenerate it? Hmm. Gotta check. But no SMS service to help with losing access
to 2FA device or secret.

Besides, do I want users to be on the system for so long? As I can't support
many users because I won't have much space for storing data. Hmm.

Is there any concept of guest in ory kratos? No sign-in might be nice, but it's
not exactly secure. Gotta check if guests can use the same IDs etc. And it may
not exactly be user friendly - it's easier for my friend to know I'm
karuppiah7890 on GitHub than to say over chat that my guest username is
karuppiah1234567 because others got taken :P Also we don't want any explosion
of guest users, hmm. That could be a hack too - to be able to create tons of
guest users just like that. Then we will be out of space - the only little space
we have in our DB. Hmm. I think we should just stick to using third party
services for the public service. Starting with GitHub.

---

Any other attacks possible on the server? DDoS? Gotta check. Gotta do threat
modelling exercise before and after implementation :)

---

I'm trying out ORY Kratos now. Installing it

https://www.ory.sh/kratos/docs/install#macos

```bash
$ brew tap ory/kratos
$ brew install ory/kratos/kratos
```

But it failed. I created an issue for it.

https://github.com/ory/homebrew-kratos/issues/1

https://www.ory.sh/kratos/docs/install#docker

https://github.com/ory/kratos/releases

---

To read:
https://wiki.owasp.org/index.php/Testing_for_User_Enumeration_and_Guessable_User_Account_(OWASP-AT-002)

https://www.ory.sh/docs/ecosystem/software-architecture-philosophy/

---

User experience should be like -

Go to page where login with GitHub is present as a button. This can be based on
redirect or not be based on redirect. As some authentication and authorization
systems like to put a unique ID in the URL for every flow

After logging in with GitHub, I want to see a jwt or similar token on the
screen, which I can use to access the server

Think about what you will use it for. How will you use the token? The token is
mainly for the server to be able to identify the user. Is that possible with
what you are doing? Can it show "hi karuppiah!" ? With tokens and not cookies
or sessions

---

Things to do now for trying out ORY with GitHub

Create GitHub app in GitHub. A dummy app for local.

Create config file to run kratos. Use sqlite or Postgresql.

Check signin with GitHub docs. They have "provider" for that. But they have
some default scope for that. If that's a lot of scope, then we can actually try
to use lesser scope.

What if the most basic scope has ability to read email ID? How does this whole
thing work?

I read that they use the GitHub user API to get details, as they are not some
Open ID Connect thingy. OAuth I guess? Idk if they are related. Hmm

There are also generic things that we can use for using GitHub provider. Just
in case if it's needed. As I don't want to store any email ID and also not be
able to have credentials and access to read user's email ID. Hmm

---

https://github.com/settings/developers

https://docs.github.com/en/free-pro-team@latest/developers/apps/building-oauth-apps

https://docs.github.com/en/free-pro-team@latest/developers/apps/managing-oauth-apps

https://docs.github.com/en/free-pro-team@latest/developers/apps/creating-an-oauth-app

I need to understand what's the callback URL, hmm. Gotta check the scopes and
access and run a kratos server first

https://docs.github.com/en/free-pro-team@latest/developers/apps/authorizing-oauth-apps

For testing, there's this it seems -
https://docs.github.com/en/free-pro-team@latest/developers/apps/authorizing-oauth-apps#non-web-application-flow

https://docs.github.com/en/free-pro-team@latest/developers/apps/authorizing-oauth-apps#web-application-flow

Scopes
https://docs.github.com/en/free-pro-team@latest/developers/apps/scopes-for-oauth-apps

Wow, there IS a no-scope :D :D :D YASSSS!

So yeah, it's possible for users to revoke the app's access from their settings,
or even provide lesser scopes during the auth flow. In our case, since there's
no scope in the first place, I guess the only thing left out is - revoke access.
In which case, we will have to immediately do some action. If the user is
already pairing and has no need to verify their identity or talk to the backend
then no problem - but this is something to think about. When and how much do
we check the identity of the users and is it really needed and helpful? I think
during the start of a pairing session, we need to tell the collaborating users
about their identities to other users to ensure that they can trust the other
users. For now, I guess we can check identity only during the start of the
pairing session

https://docs.github.com/en/free-pro-team@latest/developers/apps/scopes-for-oauth-apps#requested-scopes-and-granted-scopes

If for some reason we are not able to find the identity of the user, we will
ask them to signin with GitHub. 🤷

---

https://www.ory.sh/kratos/docs/concepts/index

https://www.ory.sh/kratos/docs/concepts/terminology

https://www.ory.sh/kratos/docs/concepts/ui-user-interface

https://www.ory.sh/kratos/docs/concepts/identity-user-model

---

GitHub OAuth has two kinds of flow for usual authentication, there's the usual
browser one, and then a device one

https://docs.github.com/en/free-pro-team@latest/developers/apps/authorizing-oauth-apps#device-flow

Wondering what are the pros and cons.

Also, I need to define what kind of need the app has. This also brings in some
context and requirements from future stories which aren't implemented or even
designed or thought about. Hmm.

The plan is to check what Atom does.

I intend to have a basic design for now after checking what Atom Teletype does.

The idea is - the backend service will be the middle system between the users,
which will bootstrap the connectivity among them and then the communication is
all peer to peer with no server in between. This is what I'm preferring, but not
sure about the pros and cons of the design and the technology to be used for the
networking (communication), for example WebRTC, Dat project etc.

The idea is to also keep most of the data, like, almost all, in the client side,
as much as possible, to avoid server having any data - hence making it an
important thing with lots of data to be protected.

For the server to be able to bootstrap the initial communication and introduce
the users to each other, it has to be able to identify the users and trust the
identity of the users, so that it can introduce them to the other users using
that identity. And we can also ask the other user "Hey, karuppiah7890 (github)
wants to access your VS Code. Allow? Or Deny?" and if we have read and write
access in the future, also ask what kind of access the user wants to provide,
if they are allowing.

So yeah, these are some things to note.

---

Can we use a client only authentication mechanism ? Where client gets an access token and can access APIs with it. This token will be obtained through device flow of ouath. Cons is that user has to type some device code etc. Pro is, we might not have a very complex mechanism for authentication. No problems of copying to clipboard or even communicating from browser to vs code.

But. How do we use the access token later? It's a token with no scope. So it only has any public info that the user has provided and is public for all. We want to use only GitHub ID and hence want that only from API and we can access the API as the user using that token. Is it okay to send the access token to the server during bootstrap to prove the user's identity? Any threats possible? All communication is encrypted with HTTPS, and we will ensure that HTTPS is forced. No HTTP. So, what else can be an issue? Also, we can get rid of the token immediately once the GitHub ID is known. What say?

The API to be called to know identity should be something like "who am I?" And find the GitHub ID then. No email, nothing. Can someone spoof this token? Hmm. I don't think so.

What's the validity of this access token? Does it have to be refreshed? Hmm. How to invalidate it? Anyway possible? What happens if it's not refreshed? Login again? Hmm

Some of these questions has to be asked even if ORY is being used. There might be a problem with ORY - where it stores the GitHub access token and gives the user it's own session cookie or token. And we have to see the validity of that token.

One benefit to using ORY is, enterprise users can benefit from it. Hmm.

If you think about it, do I really need to keep track of the users in the system? Like, in the current case. A lot of my thoughts are around how this can be more and more peer to peer.

For enterprises, maybe we can think later. What say?

Also, how does the communication even happen? For the bootstrapping of the peer to peer communication. Are we sure about anything on this?

Also, can we try both device flow and also web flow with ORY? Just to understand what each beholds. And go with device flow for now. Ensure it's all very loosely coupled, so that it can be changed easily later.

In terms of user experience, can we ensure that the notification shown to user to login can actually stay for a long time? Maybe it can be a combination of progress bar with info? Hmm. Should we automatically open browser for login with code? Should we copy the code to clipboard? Maybe not. It's a security issue too, in case clipboard managers are there, they can read the code too. Meh. Leave it. Let it be. No data should be unnecessarily leaked. Let the user type the code or it's up to them to copy it. It's their wish. Hmm.

Also, for doing device flow, we will need GitHub Typescript or JavaScript client libraries for the GitHub API. I'm sure there must be one. Gotta check it out. Will it use simple REST API or GraphQL API? Idk. Gotta check what's possible. Maybe both are possible. Idk what I want to choose. REST API is easy and straight forward as my use case is too simple.

Check if people send third party access tokens in requests to servers. Hmm. The only problem is, once hacked, we can't do much. But the token doesn't have much secret to it. I believe it's a read only access to the public info. Verify this. So, not so bad maybe? Still, can be confirmed only after verification.

The backend service can find the user info on the fly for bootstrapping communication. But, how will it store stuff? In-memory? Hmm. Maybe store the GitHub ID alone. Check what p2p video chat service does. Don't store access tokens, it's not needed really, it's unnecessary - extra data and it's still a secret. Hmm.

---

Secret storage

Storing secrets (access tokens etc) securely in the user system. How? Hmm. If it's in the key store, other applications accessing keystore can ask user for permission and then get access. Hmm. Does VS Code have an API to store secrets secretly? And only be accessible by the extension?

Also, with key store, one con is, the user experience. I mean, the user is asked for system password multiple times. I noticed this is VS Code Live Share. I think they use key store to store the token or secret. Hmm. I can rever engineer ;) hmm

Also, do I always dynamically get GitHub ID using the token to show the user's identity in the extension? Or cache it and only recheck when doing pairing etc? Hmm. Gotta think

https://stackoverflow.com/q/43526491/4772008

https://security.stackexchange.com/q/218958

https://stackoverflow.com/q/45293157/4772008

https://github.com/microsoft/vscode/issues/31131

https://security.stackexchange.com/q/44668

https://github.community/t/api-for-determining-personal-access-token-scopes/14053/2

---

Can anyone hack and ask for extra scopes with my client ID? Hmm. Should I protect it maybe? By only inserting it during packaging and tests maybe. Tests that need the ID. Probably tests can use a dummy app ID. Hmm.

---

Authentication mechanism should ensure that the right people are identified, and the malicious people cannot spoof identities. How easy or hard is it for hackers to do identity spoofing? And also get access to the pairing link? Or maybe guess the pairing link

---

Secret storage

For token storage, apart from storing in key store or key ring, should we encrypt it before storing? Encrypt it with server owned public key, and let's ensure this is separate from the SSL public key as we have no idea when that changes. Hmm

Why encrypt? Because I'm not sure how key store and key ring access is like. Can any application access any password? Or can it access only the passwords it stored? How does keystore know that?

Also, VS Code is an application, extension is kind of like a sub application. So, can all extensions read all the passwords stored by other extensions? Hmm

---

I'm trying out the GitHub device flow currently.

https://docs.github.com/en/free-pro-team@latest/developers/apps/authorizing-oauth-apps#device-flow

```bash
$ curl -i -X POST \
> -d '{
>     "client_id": "6be7639de10814d1d865",
>     "scope": ""
> }' \
> https://github.com/login/device/code
HTTP/1.1 404 Not Found
Date: Fri, 30 Oct 2020 04:21:32 GMT
Content-Type: text/plain; charset=utf-8
Transfer-Encoding: chunked
Server: GitHub.com
Status: 404 Not Found
Vary: X-PJAX
Cache-Control: no-cache
Strict-Transport-Security: max-age=31536000; includeSubdomains; preload
X-Frame-Options: deny
X-Content-Type-Options: nosniff
X-XSS-Protection: 1; mode=block
Referrer-Policy: origin-when-cross-origin, strict-origin-when-cross-origin
Expect-CT: max-age=2592000, report-uri="https://api.github.com/_private/browser/errors"
Content-Security-Policy: default-src 'none'; base-uri 'self'; connect-src 'self'; form-action 'self'; img-src 'self' data:; script-src 'self'; style-src 'unsafe-inline'
Vary: Accept-Encoding, Accept, X-Requested-With
X-GitHub-Request-Id: C20B:08B8:1839BB:1E24EF:5F9B94CB

Not Found
```

okay, I need to opt-in for this feature to be available. Hmm

https://docs.github.com/en/free-pro-team@latest/developers/apps/activating-beta-features-for-apps

Still doesn't work after opting in. Hmm.

Okay, I had to put content type as `application/json` in the request.

```bash
$ curl -i -X POST \
> -H 'Content-Type: application/json' \
> -d '{
>     "client_id": "6be7639de10814d1d865",
>     "scope": ""
> }' \
> https://github.com/login/device/code
HTTP/1.1 200 OK
Date: Fri, 30 Oct 2020 04:25:09 GMT
Content-Type: application/x-www-form-urlencoded; charset=utf-8
Transfer-Encoding: chunked
Server: GitHub.com
Status: 200 OK
Vary: X-PJAX
ETag: W/"2fefc659ad7faee99f465d0c37ad6fd8"
Cache-Control: max-age=0, private, must-revalidate
Strict-Transport-Security: max-age=31536000; includeSubdomains; preload
X-Frame-Options: deny
X-Content-Type-Options: nosniff
X-XSS-Protection: 1; mode=block
Referrer-Policy: origin-when-cross-origin, strict-origin-when-cross-origin
Expect-CT: max-age=2592000, report-uri="https://api.github.com/_private/browser/errors"
Content-Security-Policy: default-src 'none'; base-uri 'self'; block-all-mixed-content; connect-src 'self' uploads.github.com www.githubstatus.com collector.githubapp.com api.github.com www.google-analytics.com github-cloud.s3.amazonaws.com github-production-repository-file-5c1aeb.s3.amazonaws.com github-production-upload-manifest-file-7fdce7.s3.amazonaws.com github-production-user-asset-6210df.s3.amazonaws.com cdn.optimizely.com logx.optimizely.com/v1/events wss://alive.github.com; font-src github.githubassets.com; form-action 'self' github.com gist.github.com; frame-ancestors 'none'; frame-src render.githubusercontent.com; img-src 'self' data: github.githubassets.com identicons.github.com collector.githubapp.com github-cloud.s3.amazonaws.com *.githubusercontent.com; manifest-src 'self'; media-src 'none'; script-src github.githubassets.com; style-src 'unsafe-inline' github.githubassets.com; worker-src github.com/socket-worker.js gist.github.com/socket-worker.js
Vary: Accept-Encoding, Accept, X-Requested-With
Vary: Accept-Encoding
X-GitHub-Request-Id: C22C:7B72:88E32:AB0FA:5F9B95A5

device_code=0330f2c20a927090941956bc55016a25e2bedfff&expires_in=899&interval=5&user_code=8AE1-A7A8&verification_uri=https%3A%2F%2Fgithub.com%2Flogin%2Fdevice
```

I tried the login with this and during authorization, it said

`Make sure you trust this device as it will get access to your account.`

Also, I think anyone with the client ID can ask for any number of scopes. The
client ID has to be a secret, of course!! :) And not just the client secret.
Keep all of it safe and secure.

```bash
$ curl -i -X POST \
> -H 'Content-Type: application/json' \
> -d '{
>     "client_id": "6be7639de10814d1d865",
>     "device_code": "0330f2c20a927090941956bc55016a25e2bedfff",
>     "grant_type": "urn:ietf:params:oauth:grant-type:device_code"
> }' \
> https://github.com/login/oauth/access_token
HTTP/1.1 200 OK
Date: Fri, 30 Oct 2020 04:30:15 GMT
Content-Type: application/x-www-form-urlencoded; charset=utf-8
Transfer-Encoding: chunked
Server: GitHub.com
Status: 200 OK
Vary: X-PJAX
ETag: W/"263a15211b2a1bb1fb82b887b896c4c5"
Cache-Control: max-age=0, private, must-revalidate
Strict-Transport-Security: max-age=31536000; includeSubdomains; preload
X-Frame-Options: deny
X-Content-Type-Options: nosniff
X-XSS-Protection: 1; mode=block
Referrer-Policy: origin-when-cross-origin, strict-origin-when-cross-origin
Expect-CT: max-age=2592000, report-uri="https://api.github.com/_private/browser/errors"
Content-Security-Policy: default-src 'none'; base-uri 'self'; block-all-mixed-content; connect-src 'self' uploads.github.com www.githubstatus.com collector.githubapp.com api.github.com www.google-analytics.com github-cloud.s3.amazonaws.com github-production-repository-file-5c1aeb.s3.amazonaws.com github-production-upload-manifest-file-7fdce7.s3.amazonaws.com github-production-user-asset-6210df.s3.amazonaws.com cdn.optimizely.com logx.optimizely.com/v1/events wss://alive.github.com; font-src github.githubassets.com; form-action 'self' github.com gist.github.com; frame-ancestors 'none'; frame-src render.githubusercontent.com; img-src 'self' data: github.githubassets.com identicons.github.com collector.githubapp.com github-cloud.s3.amazonaws.com *.githubusercontent.com; manifest-src 'self'; media-src 'none'; script-src github.githubassets.com; style-src 'unsafe-inline' github.githubassets.com; worker-src github.com/socket-worker.js gist.github.com/socket-worker.js
Vary: Accept-Encoding, Accept, X-Requested-With
Vary: Accept-Encoding
X-GitHub-Request-Id: C252:55ED:191056:1F18C1:5F9B96D6

access_token=03944c415d9e087b84c844ae5e50da988df55003&scope=&token_type=bearer
```

```bash
$ curl \
>   -H "Authorization: token 03944c415d9e087b84c844ae5e50da988df55003" \
>   -H "Accept: application/vnd.github.v3+json" \
>   https://api.github.com/user
{
  "login": "karuppiah7890",
  "id": 12808424,
  "node_id": "MDQ6VXNlcjEyODA4NDI0",
  "avatar_url": "https://avatars3.githubusercontent.com/u/12808424?v=4",
  "gravatar_id": "",
  "url": "https://api.github.com/users/karuppiah7890",
  "html_url": "https://github.com/karuppiah7890",
  "followers_url": "https://api.github.com/users/karuppiah7890/followers",
  "following_url": "https://api.github.com/users/karuppiah7890/following{/other_user}",
  "gists_url": "https://api.github.com/users/karuppiah7890/gists{/gist_id}",
  "starred_url": "https://api.github.com/users/karuppiah7890/starred{/owner}{/repo}",
  "subscriptions_url": "https://api.github.com/users/karuppiah7890/subscriptions",
  "organizations_url": "https://api.github.com/users/karuppiah7890/orgs",
  "repos_url": "https://api.github.com/users/karuppiah7890/repos",
  "events_url": "https://api.github.com/users/karuppiah7890/events{/privacy}",
  "received_events_url": "https://api.github.com/users/karuppiah7890/received_events",
  "type": "User",
  "site_admin": false,
  "name": "Karuppiah Natarajan",
  "company": "ThoughtWorks",
  "blog": "https://karuppiah7890.github.io/blog/",
  "location": "Chennai, India",
  "email": "karuppiah7890@gmail.com",
  "hireable": true,
  "bio": "Ooohhh.....I am Karups!!!",
  "twitter_username": "karuppiah7890",
  "public_repos": 216,
  "public_gists": 19,
  "followers": 119,
  "following": 444,
  "created_at": "2015-06-09T05:27:40Z",
  "updated_at": "2020-10-30T04:22:23Z"
}
```

Cool! I guess I got a gist of how that will look like!! :) Hmm.

Now the other flows are yet to be researched and tried out.

---

One thought in my mind is - now, we are identifying users through third party
services, for example GitHub, and it's all from the client side - with no server
code from my end. I just have a client app in my local and then GitHub server,
no app of mine in a server. Hmm.

I wonder how people have client only apps and have peer to peer communication
and have security in place. Basically - security in peer to peer apps and
peer to peer communication. Hmm. Something to think about.

Currently I was thinking about using the bootstrapping server to be the service
which will do bootstrapping and will also ensure the security between the two
users where the two users know each other's identity and can also trust each
other. Actually, till now, I was always thinking about the local user trusting
the incoming remote user who is going to have access to the local user's code.
I'm not sure if the other way round is a threat - where the remote user is
concerned about the local user - maybe the local user can still hack the remote
user even though it's the remote user accessing local user's code, but still,
there's data flowing among them. So, anyone can hack the other one if one of
them is a hacker and is spoofing a real person, or is legit and still hacking
the other person who is a victim and innocent. Hmm.

---

The REST API I used

https://docs.github.com/en/free-pro-team@latest/rest
https://docs.github.com/en/free-pro-team@latest/rest/overview/resources-in-the-rest-api
https://docs.github.com/en/free-pro-team@latest/rest/overview/resources-in-the-rest-api#authentication
https://docs.github.com/en/free-pro-team@latest/rest/reference
https://docs.github.com/en/free-pro-team@latest/rest/reference/users
https://docs.github.com/en/free-pro-team@latest/rest/reference/users#get-the-authenticated-user

I think GraphQL API can also be used! :D :D ;)

---

Some things to take care of in the device flow -

https://docs.github.com/en/free-pro-team@latest/developers/apps/authorizing-oauth-apps#rate-limits-for-the-device-flow - rate limits

Error codes -
https://docs.github.com/en/free-pro-team@latest/developers/apps/authorizing-oauth-apps#error-codes-for-the-device-flow

One good thing is, client_secret is NOT needed for the device flow. It's also
explicitly mentioned in the docs

---

I was wondering what's the expiry of the access token I have.

https://docs.github.com/en/free-pro-team@latest/developers/apps/refreshing-user-to-server-access-tokens

So, there's talk around github apps only. It's different from OAuth apps. Hmm.

https://docs.github.com/en/free-pro-team@latest/developers/apps/identifying-and-authorizing-users-for-github-apps

Couldn't find much info on expiration for device flow access tokens. Hmm. There
are many for GitHub Apps. Hmm.

Checking more.

---

On the side, I was simply reading about GitHub Apps and OAuth Apps. I guess it
was the right decision to choose OAuth Apps for my use case where I just wanted
to verify identity of the user. It's still only half way done. The extension in
the local is able to tell the identity of the user, but how do I make the other
remote extension to trust I'm the user - through server or directly in a peer
to peer fashion. Hmm. Or maybe I don't even need GitHub and I can think of a
different identity. Let's see. This is all assuming that all the communication
or at least most will be peer to peer. I really gotta check possibility of
webrtc in electron. Hmm.

https://docs.github.com/en/free-pro-team@latest/developers/apps/differences-between-github-apps-and-oauth-apps

---

If you notice this

https://docs.github.com/en/free-pro-team@latest/developers/apps/refreshing-user-to-server-access-tokens#renewing-a-user-token-with-a-refresh-token

The response has information of when the access token will expire and when the
refresh token will expire. Hmm.

Looks like, currently, there are two ways to make the token go invalid - one is
using the OAuth app - to which admin, that's me, has access. I can revoke ALL
tokens. I can also see how many people are using the app after signing in. Hmm.

Another way is - user goes to their GitHub settings, and checks the applications
section and goes to Authorized OAuth Apps and revokes the App's access

```bash
curl -i \
-H "Authorization: token 03944c415d9e087b84c844ae5e50da988df55003" \
-H "Accept: application/vnd.github.v3+json" \
https://api.github.com/user

HTTP/1.1 401 Unauthorized
Date: Fri, 30 Oct 2020 05:15:23 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 90
Server: GitHub.com
Status: 401 Unauthorized
X-GitHub-Media-Type: github.v3; format=json
X-RateLimit-Limit: 60
X-RateLimit-Remaining: 57
X-RateLimit-Reset: 1604038408
X-RateLimit-Used: 3
Access-Control-Expose-Headers: ETag, Link, Location, Retry-After, X-GitHub-OTP, X-RateLimit-Limit, X-RateLimit-Remaining, X-RateLimit-Used, X-RateLimit-Reset, X-OAuth-Scopes, X-Accepted-OAuth-Scopes, X-Poll-Interval, X-GitHub-Media-Type, Deprecation, Sunset
Access-Control-Allow-Origin: *
Strict-Transport-Security: max-age=31536000; includeSubdomains; preload
X-Frame-Options: deny
X-Content-Type-Options: nosniff
X-XSS-Protection: 1; mode=block
Referrer-Policy: origin-when-cross-origin, strict-origin-when-cross-origin
Content-Security-Policy: default-src 'none'
Vary: Accept-Encoding, Accept, X-Requested-With
X-GitHub-Request-Id: C373:43B2:834D3:A58AD:5F9BA16A

{
  "message": "Bad credentials",
  "documentation_url": "https://docs.github.com/rest"
}
```

---

So, as far as I can see, the token never expires for now. Hmm.

I'm wondering the possibilities of this being a problem. If the token never
expires, if it's leaked, it has to be revoked by the user. As me, the admin,
cannot revoke exactly one token. I can only revoke all tokesn at once. Hmm.

In such cases an identity management system helps, like ORY, but then again,
it's a complex thing and for my use case - just to trust and identify a user,
I don't think I need it. GitHub ID is just an identity for my use case. For
having trust on users connecting to each other later. Gotta think more on it
maybe.

Now, how will one even know that the token leaked? No idea. If they do find it
being leaked, they can revoke it.

But, how can it leak? It can leak only if the token has been stored very
insecurely. So, if I store the token securely, then all is good. I do have to
see how I use the token - it has to be processed securely. If I encrypt, I can't
process it, or use it at all, unless I have a way to decrypt it. And storing
encryption keys securely is a another problem. It's like it goes on and on and
on. So, it ideally comes down to

- Security at rest - when it's in storage
- Security in transit - when it's in transit
- Security when it's being processed

Also, if it DOES leak, what's the penalty? What's the consequence? So, the
hacker can actually use the token to just get public information of the user as
the extension will not use any scope while getting the token - that is
`(no-scope)`.

The other thing to note is, if the token DOES leak, can the hacker spoof as the
other user? I guess so. Since there's nothing else present as secret for
identity. Hmm. I guess it comes down to - keep the token secure. We surely
DONT want identity spoofing attacks.

---

Apparently I can give a link to users if they want to revoke access or check
access information

https://docs.github.com/en/free-pro-team@latest/developers/apps/authorizing-oauth-apps#directing-users-to-review-their-access

https://github.com/settings/connections/applications/:client_id

---

I'll need to add the logo or badge to the OAuth App if I use it

https://docs.github.com/en/free-pro-team@latest/developers/apps/creating-a-custom-badge-for-your-oauth-app

---

Since device flow requires only client ID, also the fact that the user can
easily find the Client ID from the setting, by going to

https://github.com/settings/connections/applications/

And clicking on the particular application and finding the particular app's
client ID by checking it's URL.

This is according to this

https://docs.github.com/en/free-pro-team@latest/developers/apps/authorizing-oauth-apps#directing-users-to-review-their-access

Then, anyone can get my app's client ID and impersonate my app - they can
create their own CLI, VS Code extension, anything and impersonate my app and
my app's name and people will see the name and trust it - let's say it's on the
VS Code Marketplace, then they can actually impersonate the app, and also
impersonate the GitHub OAuth App device flow which requires only client ID
which they can easily get by being a legit user and using the real app and then
using the above steps.

Then people will be fooled by the fake app or impersonating app, and also they
will be thinking that they are giving access to me and my app, but really they
are giving access to another app though the GitHub OAuth App is the same and is
being shared. The hacker can put any code in their VS Code extension or
whatever app and also ask for any scope from the user. So, now the user must
understand that we DO NOT ask for extra permissions. But the hacker will ask,
and if the user is innocent, and doesn't know much about security, they will
give access and hacker will be able to use people's trust on me and my app to
be able to get access to some user's information, maybe many users! Hmm.
Access to private repos, code, secrets and what not. Hmm.

I think this is something to inquire with GitHub. Hmm. But they can't also ask
developers to use client secret in a client only app because sometimes, the
app can be opened up - it's source code checked etc, and hence find out the
client secret. So, it's very dangerous. Hmm.

I think in the GitHub OAuth App description, we can add information that we
ONLY ask for public info and NO OTHER information. The hacker CANNOT change the
description as I have access to that and my account is secure. The hacker can
only put extra scope and ask for extra scope. But description will clearly say
what the app is made for - for example, the VS Code extension link and ask the
user to ensure that they are using exactly that app, and also mention that you
ask only for public info and NOTHING else.

But this is too much information for a user - legit user using legit app and
also legit user using duplicate app. Their trust on the system will go low -
if I say "ensure you don't see or provide any extra access as we DO NOT require
extra access. this is just to ensure no other app is hacking you" etc.

I'll ask GitHub about this. Hmm.

---

Helped with this a bit

https://github.community/t/device-flow-authentication/139181

https://gist.github.com/kcranston/6edfebf177f82af6c6f6e1db92e9c722

---

https://bounty.github.com/#rules

---

I was checking VS Code Live Share key stored in my machine Keychain Access

I was able to search some vs code live share related tokens with the search
term "live"

Name: `com.microsoft.vs.liveshare: UserInfo`
Kind: `application password`
Account: `github|some-email-id`
Where: `com.microsoft.vs.liveshare: UserInfo`

```json
{
  "Expires": "2020-05-28T11:35:15.951117Z",
  "UserInfo": {
    "Id": "99999999-2a1a-45d2-87cb-020e5606308d",
    "DisplayName": "Karuppiah Natarajan",
    "EmailAddress": "some-email-id",
    "UserName": "some-username",
    "AccountStatus": 3,
    "AvatarUri": "https://avatars3.githubusercontent.com/u/12808424?v=4",
    "RecentCollaborators": [
      {
        "Id": "99999999-2a1a-45d2-87cb-020e5606308d",
        "Email": "some-email-id",
        "Name": "Karuppiah Natarajan",
        "AvatarUri": "https://avatars3.githubusercontent.com/u/12808424?v=4"
      }
    ],
    "ProviderName": "github",
    "AccountId": "some-email-id"
  }
}
```

Under access control, it has options.

`Allow all applications to access this item` - Not chosen

`Confirm below allowing access` - Chosen

- `Ask for Keychain password` - Sub option. Not chosen

`Always allow access by these applications`:

`vsls-agent`

That was all. There were more entries for VS Code Live Share, where Account
field alone was different. Looks like they have some expiry dates for their
tokens. Some of them said "anonymous". Hmm.

I also found one of them had a JWT token. Hmm.

Also, while trying to access the password stored in the Keychain, Keychain asks
for password - twice.

Once it said ' Keychain Access wants to use your confidential information stored
in "com.microsoft.vs.liveshare: UserInfo" '

And asked for password and the options below were `Always Allow`, `Deny`,
`Allow`

The second time it asked for password, it said

' Keychain Access wants to access key "com.microsoft.vs.liveshare: UserInfo"
in your keychain '

So, I think, the first time, it is about "Name" and the second time, it is about
"Where". Hmm.

Wierdly, there are many with same name, but yeah, different account. Hmm.
Gotta learn more about Keychain Access and how applications and users access it.
Hmm.

I have noticed before that atom teletype also stores it in Keychain Access.
But there's a difference. The application which has access to it is - Atom
Helper. I think that's at the editor level - more like, something that can be
shared by multiple extensions. Other extensions could try to access it and maybe
able to access it. Hmm. Crazy huh.

I noticed VS code related some secrets are also completely accessible by VS
Code Helper. But there's also the vsls-agent in some of them - I think that the
vsls-agent is a very specific process / application specific to the extension
which is only present in the extension and hence it's more specific and hence
other extensions can't try to access it - not sure if it's really name specific
or maybe has some more info inside it. I hope it does have some more info.
If it's just a name, then anyone can create a process somehow, with the name
vsls-agent I think. Also, I was simply checking if I'm able to invoke or run
any command in my local, to see if there's anything called `vsls-agent`,
nothing in my PATH environment variable. Hmm

---

Okay, so I checked what kind of token atom teletype uses. And it's the same!! :)
It's a simple github access token. I noticed this token inside the keychain
access app.

```bash
$ curl -i \
-H "Authorization: token f48fc1e38e76f64530cfdb34c26eee2bf9aede2f" \
-H "Accept: application/vnd.github.v3+json" \
https://api.github.com/user

HTTP/1.1 200 OK
Date: Fri, 30 Oct 2020 17:02:18 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 1487
Server: GitHub.com
Status: 200 OK
Cache-Control: private, max-age=60, s-maxage=60
Vary: Accept, Authorization, Cookie, X-GitHub-OTP
ETag: "ce8176749a323d747d115de4a8f9f37a4f2e0dd13c657f0c3cedbe40d07f3dde"
Last-Modified: Fri, 30 Oct 2020 04:22:23 GMT
X-OAuth-Scopes:
X-Accepted-OAuth-Scopes:
X-OAuth-Client-Id: 99c5a54e39feda31f25b
X-GitHub-Media-Type: github.v3; format=json
X-RateLimit-Limit: 5000
X-RateLimit-Remaining: 4995
X-RateLimit-Reset: 1604080810
X-RateLimit-Used: 5
Access-Control-Expose-Headers: ETag, Link, Location, Retry-After, X-GitHub-OTP, X-RateLimit-Limit, X-RateLimit-Remaining, X-RateLimit-Used, X-RateLimit-Reset, X-OAuth-Scopes, X-Accepted-OAuth-Scopes, X-Poll-Interval, X-GitHub-Media-Type, Deprecation, Sunset
Access-Control-Allow-Origin: *
Strict-Transport-Security: max-age=31536000; includeSubdomains; preload
X-Frame-Options: deny
X-Content-Type-Options: nosniff
X-XSS-Protection: 1; mode=block
Referrer-Policy: origin-when-cross-origin, strict-origin-when-cross-origin
Content-Security-Policy: default-src 'none'
Vary: Accept-Encoding, Accept, X-Requested-With
Vary: Accept-Encoding
X-GitHub-Request-Id: E3FE:5506:59A6A:7A0E4:5F9C471A

{
  "login": "karuppiah7890",
  "id": 12808424,
  "node_id": "MDQ6VXNlcjEyODA4NDI0",
  "avatar_url": "https://avatars3.githubusercontent.com/u/12808424?v=4",
  "gravatar_id": "",
  "url": "https://api.github.com/users/karuppiah7890",
  "html_url": "https://github.com/karuppiah7890",
  "followers_url": "https://api.github.com/users/karuppiah7890/followers",
  "following_url": "https://api.github.com/users/karuppiah7890/following{/other_user}",
  "gists_url": "https://api.github.com/users/karuppiah7890/gists{/gist_id}",
  "starred_url": "https://api.github.com/users/karuppiah7890/starred{/owner}{/repo}",
  "subscriptions_url": "https://api.github.com/users/karuppiah7890/subscriptions",
  "organizations_url": "https://api.github.com/users/karuppiah7890/orgs",
  "repos_url": "https://api.github.com/users/karuppiah7890/repos",
  "events_url": "https://api.github.com/users/karuppiah7890/events{/privacy}",
  "received_events_url": "https://api.github.com/users/karuppiah7890/received_events",
  "type": "User",
  "site_admin": false,
  "name": "Karuppiah Natarajan",
  "company": "ThoughtWorks",
  "blog": "https://karuppiah7890.github.io/blog/",
  "location": "Chennai, India",
  "email": "karuppiah7890@gmail.com",
  "hireable": true,
  "bio": "Ooohhh.....I am Karups!!!",
  "twitter_username": "karuppiah7890",
  "public_repos": 216,
  "public_gists": 19,
  "followers": 119,
  "following": 444,
  "created_at": "2015-06-09T05:27:40Z",
  "updated_at": "2020-10-30T04:22:23Z"
}
```

I think this is great! At least I know one more thing about atom teletype. But
atom teletype uses the usual web authentication flow. Hmm.

The only thing about device flow that worries me is, anyone can just use the
client ID and imitate me. Hmm. I have emailed GitHub about it. Let's see.

I guess I need to see how Atom teletype server and client do the communication
and how they work collaboratively. Hmm :)

---

Okay, reading this
https://github.com/github/roadmap/issues/130

The issue mentions how the support is mainly for use cases where there is no
support for browser. Hmm. I mean, even inside the extension, there's no direct
access - I have to make a call from the browser to the extension. Hmm. My
extension does have a user interface, just not inside the browser. Hmm

---

Interesting code related to device flow. A demo. But it has some extra stuff -
some heroku app etc

https://gist.github.com/panva/652c61e7d847e0ed99926c324fa91b36

Maybe it's not exactly a GitHub thing, but just related. I guess Device Flow is
a general mechanism. Hmm.

---

https://duckduckgo.com/?t=ffab&q=github+device+flow+security&ia=web

https://www.identityserver.com/articles/an-introduction-to-the-oauth-device-flow

---

GitHub Signin

Read atom teletype server and client code to see how it communicates and authenticates the clients. How does it show the gravatar of the remote user to the local user?

Any possible security issues? Hmm

Finally, decide on one way to authenticate. I think device flow is okay for now. And we can start thinking about testing this feature. Handling errors etc. :)

Check GitHub Typescript client library for request or create a small simple one as we have only a request or two. But we have to define error codes or enums for error responses. Hmm.

For device flow, check if the user really has to type the user code or can the sign in link contain query param to auto fill the user code and the user just has to click sign in?

Hmm. Or maybe ORY is better? This web auth way can be done later. And we will use URI handler to handle the access token. This is believing the fact that no other extension can register the same handler and hack and get the access token. Even if they do, it won't have any private or extra scope (access). But I think that another extension can do it. Can register with and URI handler and intervene or cause confusion. All sorts of hacking. Idk. Have to verify. Hmm. Other than that, we'll most probably store the token in local.

We do have to think about trust among users and authentication and trust and verification of Identities. Hmm. Especially for a possible peer to peer connection. Usually in a centralised system with server, users send info to server, server authenticates and authorises user and then stores the info. And then when the other user is available online, the server sends off this info to that user. Some server systems get rid of the info once it has been delivered. It's upto to the user to keep it safe, back it up in the cloud in a separate service etc. Usually the client apps help with integrating with cloud services for backups. Such systems are good systems. Not storing user info for long and only till it's required. In our case, it's assumed that both users are online at the same time collaborating. But I still have to understand how in a peer to peer manner and also handle unavailability at times. I remember thinking about this. About data loss etc. Gotta pull that out and think on that. Hmm

---

Protocol Or URI handler for VS Code Extensions

https://github.com/microsoft/vscode/issues/48511

https://github.com/microsoft/vscode/issues/48427

https://code.visualstudio.com/api/references/vscode-api

registerUriHandler

---

Now I'm checking the Atom Teletype Server Code. I'm planning to write down
the observations in a separate file -
[Atom Teletype Server](./atom-teletype-server.md)

---

Backend Service

Looking at teletype code, I was wondering if I should reuse or atleast be inspired by it and not reinvent the wheel much.

But to us the code as is, I need to use services like bugsnag, pusher etc. I have no idea about them. And also use Postgres. I think Postgres is good. I was just wondering can I use something too simple. As I want to keep very less data and simple data in the server. More like, just use it temporarily for the bootstrapping. But I guess bootstrapping can happen anytime. Hmm. Like, once two users join, third user may join them very very later. Hmm. I think I'll have to think about that use case later. I just want to support two people for now and with many features. Then move on to multiple. And also see what users got to say.

I was thinking can I use redis. Or even just simple in-memory database. But if server crashes, it's a problem. Hmm. I could use in-memory database with file storage backup frequently and ensure it's also a distributed one too so that I can run multiple instances of my server too or my users could do it. Hmm. Or I could go with etcd, redis but use them as a store. Some people use redis as a cache and don't worry too much if the data is lost. But yeah, if redis does go down, it will make things slow since it was the cache to keep things faster. Hmm.

---

GitHub Signin

Maybe I'm thinking too much. For now, I'll go ahead with the GitHub signin feature and finish it off to come back and check how to do collaboration and keeping things secure later!! :) I'm going to use device flow only though! The thing about people copying my client ID, I have sent GitHub an email about it. Let's see what they got to say about it. Hmm. And I think having GitHub access token in the local is fine. It will be a life long thing, but it won't harm people as it's a no scope token so not so important. But I do have to ensure that I can still show to the other user the real identity of the remote user when doing collaboration. Gotta think how to do it. Hmm. Like, ensure there's no spoofing possible, apart from hacking the access token of the remote user. Which is like the only thing that identifies the user. 🤷‍♂️😅😂 If they get hold of the token, then it's gone for sure. For that, we will ensure safe storage of token, safe transit of token and also safe processing of token. But no other way of spoofing or weak points for spoofing should be possible!! :) Zero trust system as much as possible!!

---

Now the next thing to do is, define a user journey and go ahead with it.
I'm gonna have to cut off some things from the research list. It can be done
later I guess :) when I need those features.

I also have to think about the testing strategy. Hmm.

---

User Journey

User opens their VS Code. They open command palette and choose
`Cocreate Remote: Sign in with GitHub`. A notification asks to sign in. If we
can open a URL, that's cool too. And if the URL can contain query param that
would be good, to auto fill the user code. But, no, in github, it doesn't seem
to be possible. So, we ask the user to copy paste or type the code themselves.
Hmm. A bit much, hmm. Anyways. It's not that bad a UX I think. It will do for
now :)

Once logged in, the user can see their GitHub Name and ID in their status bar
and also on the left explorer in a separate section, which shows the list of
participants, with the user being one of them. In the list, the user's GitHub
Name and ID are shown like this

Karuppiah Natarajan (karuppiah7890)

This changes the user journey a bit from what was envisioned initially. I think
it's still good. Not so bad. And secure too. :) As long as people use the real
app and not any fake apps which could misuse the client ID and get lots of
access from users and hack them. Users have to be wary of the fact that they
are providing access to something and also check what access they provide. Hmm.

The next time the user tries to sign in, there should be a notification saying
they are already signed in.

In the future, this will be part of the sharing session initiation, where it
will checked if the user is signed in. The user can also sign in using the
separate option in the command palette for signin.

---

I'm wondering how to test this user journey. There will be lot of unit tests.
What about integration tests? Hmm.

I read this non-web app flow

https://docs.github.com/en/free-pro-team@latest/developers/apps/authorizing-oauth-apps#non-web-application-flow

It's for testing it seems, but there's no clue on how to do it. Hmm

I was checking about this but found other stuff

https://duckduckgo.com/?t=ffab&q=testing+device+flow+oauth&ia=web

https://auth0.com/docs/flows/device-authorization-flow

https://oauth.net/2/device-flow/

Hmm. Come to think of it, I think I don't need to create any test account or
do actual GitHub authentication. I can have an end to end test but with GitHub
API calls mocked. It is only going to be for the happy path, so all the mocks
would exhibit happy path.

I'll do other kinds of testing like unit and integration testing for more lower
level testing with different happy and error paths.

I guess what I could do for integration testing is, ensure that the client
library used for github api is mocked, or use a mocke server and point the
client library to that. Gotta see which one of those. Hmm.

I gotta see how to write integration tests first. Gonna check other repos on
how they do it! :)

---

```bash
$ git clone git@github.com:getgauge/gauge-vscode
```

I'm reading this code. I'm going to write what I understand over here -
[Gauge VS Code](./gauge-vs-code.md)

---

Typing an extra space before or after the user code is a problem?

---

Activating extension when a command palette option is chosen. Possible? Benefits? Faster start-up time without loading our extension unless necessary

---

Creating user code for GitHub sign in...

Sign in to GitHub using this link

Type this user code to sign in to GitHub

Any options to open browser URL? But user might miss to see user code and instructions. Hmm

---

I think I need to start coding something now. Also, I got quite some
experiments to do! :)

Let me start with the first small thing - open command palette and choose
sign in with GitHub and it should show a notification of sorts and also
wait for me to sign in, by showing some sort of progress bar. I should not be
able to close it. Hmm

I also need to test this, hmm.

---

I have started adding a command to the `package.json`. I was also checking what
Gauge does. I noticed a `category` field

https://code.visualstudio.com/api/extension-guides/command

https://code.visualstudio.com/api/references/contribution-points#contributes.commands

There's also something called menus

https://code.visualstudio.com/api/references/contribution-points#contributes.menus

Okay, so there are different kind of menus! Hmm.

There are so many things in the menu. One of them says "comment thread" and
other "comment" stuff. Not sure what it means. Is it like the VS Code Live Share
comments feature? Or code comments? Hmm

Ah, so one of these menus is the one on the top right, when you have added
icons

And then if you right click, that's also a menu! :) There's some sorting and
grouping

https://code.visualstudio.com/api/references/contribution-points#Sorting-of-groups

Anyways, I'll get back to adding sign in with github! :)

I tried to run the extension and run the command from command palette

```
Command 'Cocreate Remote: Sign in with GitHub' resulted in an error (command 'cocreate-remote.signin.github' not found)
```

I didn't write code for it 😅

So, when I run the extension with F5, I see that nothing is printed in the
console log in the `Output` section. I can see there are different stuff like
`Tasks`, `Git` and other extension names, but Cocreate Remote is missing from
the list. Hmm. So, it's not activated yet, as activation puts a log in the
console. Hmm

In `package.json`, we have the code `activationEvents` which says activation
happens only when sign in command is used! Nice :)

Okay, the log shows up in my editor where I'm coding the extension, not the
other editor where the extension is active. Hmm. I gotta check again when it
got activated. For now, I did get the message

`Hello World from Cocreate Remote!` with `Source: Cocreate Remote (Extension)`
to let the user know who gave the notification or message. Actually, it's just
a message, not a notification. An information message.

Okay, so it gets activated only when the command is run! :)

I was able to render an information message and also realized that I need some
sort of progress to show till the user signs in. Hmm

https://code.visualstudio.com/api/references/vscode-api#window

```javascript
withProgress<R>(options: ProgressOptions, task: (progress: Progress<{increment: number, message: string}>, token: CancellationToken) => Thenable<R>): Thenable<R>
```

There are some progress options.

Also, something to note is, with information message, we can also add some
actions to the message for the user to click on.

Currently the command says "Sign in" so I think it should just start showing
progress bar or something and give instructions to the user on how to sign in.
Since "Sign in" itself is an action command, we don't need more action buttons
etc

https://code.visualstudio.com/api/references/vscode-api#env

```javascript
openExternal(target: Uri): Thenable<boolean>
```

https://code.visualstudio.com/api/references/vscode-api#Uri

```javascript
parse(value: string, strict?: boolean): Uri
```

`openExternal` works ! :)

- We need to create user code after hitting the GitHub API
- Show the user code to the user. Ask the user to copy it. Show an action "I
  have noted it down". Or, better, use the progress bar message to show the user
  code and after that, let them open the link using the action button.

---

```javascript
const githubSignInLink = vscode.Uri.parse("https://github.com/login/device");
vscode.env.openExternal(githubSignInLink).then((openSuccessful) => {
  if (!openSuccessful) {
    // Display a message box to the user
    vscode.window.showInformationMessage(
      "Due to some issue, we were not able to open the link https://github.com/login/device"
    );
  }
});
```

---

```javascript
const progressOpts: vscode.ProgressOptions = {
  location: vscode.ProgressLocation.Notification,
  title: "Copy this below code",
  cancellable: true,
};
vscode.window.withProgress(progressOpts, (progress, token) => {
  if (!token) {
    console.log("things got cancelled in the start itself");
    return Promise.resolve();
  }

  const stopListeningForCancellation = token.onCancellationRequested(() => {
    console.log("things got cancelled as an event");
  });
  stopListeningForCancellation.dispose();
  return new Promise(() => {
    setTimeout(() => {
      console.log("simply");
    }, 10000);
  });
});
```

I was able to understand how to show a progress bar. And also about cancelling
stuff

---

Now I'm able to understand how to show progress bar as a notification and also have a cancel button in case the user wants to cancel.

I'm wondering how the flow would look like. This is also very specific to device flow. Hmm.

I need to have a separate module for device flow.

The UX, will be like -

• Show progress when initiating GitHub device flow login. Just say "Initiating GitHub login" to the user. This is to get the user code, device code from GitHub API. Once this is obtained, stop the progress, resolve it.
• Show an information message to the user to copy the user code and then click on "Open Sign In Link". Also have cancel button to cancel the sign-in. They can also close the message which also means that they cancelled the signin.
• Once they click on the link, we will know they have clicked it. Open the external link now in a programmatic way. If opening is not possible, show user an error or warning or info message to open the link themselves.
• After opening the link (successful) or showing message to open link manually (on failure to open automatically), now, show progress to show that you are waiting for the user to sign in - "Waiting for you to sign in and give access to Cocreate Remote". Show cancel button here too. They can cancel the sign in here too.
• Once they have signed in, finish progress and show information that they are signed in as their username
• In between, we actually need to store the access token in OS key ring. We could show that they have signed in and that we are storing the access token. This might be too much info. But it will also be explicit to the user who might be waiting thinking "I finished sign in, why is this thing still showing as waiting". So, we can show "securely storing your access token" after showing they have signed in successfully as their username. I don't want to show cancellation button for the storage of token. Hmm. I don't want them to stop the signin process there, but it's possible when there is no intermediate progress to show token storage. Hmm. But, if for some reason, the token storage is having issues and is taking time...if it's taking time, let them cancel it, instead of getting annoyed with it. If it's giving error, we can show the error to the user :)

Check how disposable works. Check how to test it and it's usefulness.

Check how to send HTTP requests with ability to cancel them in between. Check how to do processing with cancellation in between.

Thinking about this whole process, come to think of it, the device flow seems complex. I wonder if the normal flow is easier. Actually, that's crazier, there's one issue of communicating from browser to VS Code, and if that fails, we have to ask user to copy a code and read from the clipboard. Instead, in our case, there's complication initially, during the user code and stuff. I think those requests will happen too soon, unless there's some network issue, for example, slow network, or issues because of VPN - like VPN blocking request etc. So, it's good to let the user know what's going on and also let them cancel anytime. I would love cancel buttons instead of looking at a stuck thing which I can't close!!

Always try to show cancel button and handle cancel and have a graceful cancel whenever possible! :) I think if the user cancels and we are almost done, then that's crazy, hmm. Maybe, we can dispose listening to cancel request once we are done with our work. Hmm. But user can still cancel and UI would go away for the progress, but we might be doing our work behind the scenes I guess? Hmm. Allowing cancellation is crazy huh 😅😂

---

I need to interact with the github API. Hmm. Checking for libraries for this.
Or it's just too simple code, just that, I'll have to handle the error codes
too. I'm just looking for an existing client that has handled all the errors
too :) Especially with typescript! I need some constants for those errors maybe

Searching for a client library now

https://github.com/search?utf8=%E2%9C%93&q=github%20api

Unofficial one -
https://github.com/github-tools/github
https://www.npmjs.com/package/github-api
http://github-tools.github.io/github/ - I see the latest version is v3 hmm.
Also, last commit is about a year ago and it's looking for maintainers. Hmm.

GitHub API has v4 I think
REST API is v3 - https://docs.github.com/en/free-pro-team@latest/rest/overview/resources-in-the-rest-api#current-version
GraphQL API is v4 - https://docs.github.com/en/free-pro-team@latest/graphql/overview/about-the-graphql-api#overview

v3 - https://docs.github.com/en/free-pro-team@latest/v3 - REST
v4 - https://docs.github.com/en/free-pro-team@latest/v4 - GraphQL

https://github.com/search?l=JavaScript&q=github+api&type=Repositories

https://github.com/octokit/rest.js
https://octokit.github.io/rest.js/v18

https://github.com/search?q=github+api+library&type=Repositories

Google has created a Golang one! https://github.com/google/go-github

I want JavaScript or TypeScript... :/

https://github.com/search?l=TypeScript&q=github+api+library&type=Repositories

Official clients!!
https://github.com/octokit
https://developer.github.com/v3/libraries/

So what I saw previously was an official client! Hmm :)

But it didn't have authentication stuff, this one seems like for
authentication -

https://github.com/octokit/auth.js

https://github.com/octokit/auth-oauth-app.js#readme

https://github.com/octokit/core.js

https://github.com/octokit/oauth-authorization-url.js

https://github.com/octokit/request.js

https://github.com/octokit/octokit.js - future

https://github.com/octokit/graphql.js

---

I think we will need

https://github.com/octokit/auth-token.js

https://github.com/octokit/auth-token.js#find-more-information

https://github.com/octokit/auth-token.js#find-out-what-scopes-are-enabled-for-oauth-tokens

And we will need `request`

https://github.com/octokit/request.js#readme

It has some good features

https://github.com/octokit/request.js#features

And it supports both node and browser. I believe the environment we are in is
NodeJs.

And it supports testing too! :)

It internally uses https://github.com/octokit/endpoint.js which in turn uses
`fetch` API for browser and `node-fetch` for NodeJs

https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API

https://github.com/node-fetch/node-fetch

To mock things, we need to pass a custom fetch method it seems! :)

---

So, I'm starting with lower most piece of the code - sending requests to GitHub.
I also need to ensure that there's a way to cancel the request / the promise
in between. Hmm.

I also need to write tests.

So, the faetures I need are -

- Send request to GitHub API to get user code and device code
- Send request to GitHub API to check if the user has signed in or not, using
  the device code
- Send request to GitHub API to get details about the user

  - GitHub ID / GitHub handle / GitHub username
  - GitHub Name
  - GitHub Gravatar

  I think that's enough for now ;) :P :) :D

Okay, so, let's start writing code for these! And let's see what we need to
test! :)

I need to rename the `test` folder to `integration-test` folder. Hmm.

So, I can use the `options` to do stuff

https://github.com/octokit/request.js#request

For example, `options.request.fetch` can be used to mock the fetch, hmm. And
there's a way to cancel requests too! :)

`options.request.signal`

I'm wondering what I want to test here. Hmm. I have to see what kind of API I
want to provide and then see. I think this is going to be too simple. Wondering
if I should test this :P :P

So, I tried it out. I also wrote a very simple test. Not TDD really. Slowly
converted it into a good test though

https://jestjs.io/docs/en/expect.html#expectobjectcontainingobject

I also realized that the call was failing - two things I missed out - one is, I
didn't opt-in for the device flow beta feature, another is - the `Accept`
header was set to the default which the API didn't accept. The default was
`application/vnd.github.v3+json` but that's only for `api.github.com`, but we
were hitting the `https://github.com/login/device/code` endpoint. So yeah.

Previously, I didn't set the `Accept` and I got form encoded response, when
using `curl`. Now, I have used `application/json` and everything works well.

The thing is, now, I use the actual API. I need to mock it out! :) This will
also help me handle some possible errors. I don't think the official libraries
have considered the device flow. So, I gotta do everything I guess. Not sure
though. Gotta check. Maybe the error codes are already there in other APIs and
the same are being used in the device flow too. Who knows 🤷

I got a lot of 406 while trying things out, when I made the mistakes

https://duckduckgo.com/?t=ffab&q=error+code+406&ia=web

Mostly it was the `Accept` header.

I was checking about how to mock `fetch`, specifically `node-fetch`. I was
wondering if I really need to pass a mock through the options, then for the
real code, either I don't pass anything, or create a fetch thing and pass it.
It seemed a bit much just to test stuff.

I started checking a bit. Jest is fancy I guess? ;)

https://duckduckgo.com/?t=ffab&q=mock+fetch+node&ia=web

https://stackoverflow.com/questions/53484219/how-can-i-mock-fetch-function-in-node-js-by-jest
https://www.npmjs.com/package/jest-fetch-mock

https://www.npmjs.com/package/fetch-mock
http://www.wheresrhys.co.uk/fetch-mock/
https://www.npmjs.com/package/fetch-mock-jest
https://github.com/wheresrhys/fetch-mock/blob/master/docs/cheatsheet.md

---

Had to get rid of one of the naming issues as GitHub uses snake case in their
pay load

https://github.com/typescript-eslint/typescript-eslint/blob/v4.4.1/packages/eslint-plugin/docs/rules/naming-convention.md
https://duckduckgo.com/?t=ffab&q=typescript+eslint+naming+convention&ia=web

I had to use the dirty

```typescript
// eslint-disable-next-line @typescript-eslint/naming-convention
```

---

For mocking fetch, in my company project, we use

https://mswjs.io/ , but that's in the front end. Hmm

I did think about using or running an actual mock server, a HTTP server. But
that would be too much. Too slow. Hmm. I want things to be faster! So, no, no
real HTTP servers. :)

Looking at https://mswjs.io/docs/comparison and https://mswjs.io/docs/ , I
think msw is only for front end :)

https://duckduckgo.com/?q=mock+fetch+node+jest&t=ffab&ia=web

There are some people talking about this library too, which was in one of the
stack overflow questions

https://www.npmjs.com/package/jest-fetch-mock
https://www.leighhalliday.com/mock-fetch-jest

https://swizec.com/blog/mocking-and-testing-fetch-requests-with-jest
https://github.com/wheresrhys/fetch-mock

I'm gonna go ahead and choose https://github.com/wheresrhys/fetch-mock

---

```bash
$ npm i -D fetch-mock-jest
npm WARN deprecated core-js@2.6.11: core-js@<3 is no longer maintained and not recommended for usage due to the number of issues. Please, upgrade your dependencies to the actual version of core-js@3.

> core-js@2.6.11 postinstall /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/node_modules/babel-runtime/node_modules/core-js
> node -e "try{require('./postinstall')}catch(e){}"

Thank you for using core-js ( https://github.com/zloirock/core-js ) for polyfilling JavaScript standard library!

The project needs your help! Please consider supporting of core-js on Open Collective or Patreon:
> https://opencollective.com/core-js
> https://www.patreon.com/zloirock

Also, the author of core-js ( https://github.com/zloirock ) is looking for a good job -)


> core-js@3.6.5 postinstall /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/node_modules/core-js
> node -e "try{require('./postinstall')}catch(e){}"

+ fetch-mock-jest@1.3.0
added 14 packages from 11 contributors and audited 765 packages in 9.95s

53 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```

Weird. They need to upgrade their core-js package, or their dependencies need
to. Hmm.

Types are missing, hmm

```bash
$ npm install @types/fetch-mock-jest
npm ERR! code E404
npm ERR! 404 Not Found - GET https://registry.npmjs.org/@types%2ffetch-mock-jest - Not found
npm ERR! 404
npm ERR! 404  '@types/fetch-mock-jest@latest' is not in the npm registry.
npm ERR! 404 You should bug the author to publish it (or use the name yourself!)
npm ERR! 404
npm ERR! 404 Note that you can also install from a
npm ERR! 404 tarball, folder, http url, or git url.

npm ERR! A complete log of this run can be found in:
npm ERR!     /Users/karuppiahn/.npm/_logs/2020-11-01T12_51_01_155Z-debug.log
```

Right, the tests don't even work

```bash

    src/github.test.ts:3:32 - error TS7016: Could not find a declaration file for module 'fetch-mock-jest'. '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/node_modules/fetch-mock-jest/server.js' implicitly has an 'any' type.
      Try `npm install @types/fetch-mock-jest` if it exists or add a new declaration (.d.ts) file containing `declare module 'fetch-mock-jest';`

    3 import * as fetchMockJest from "fetch-mock-jest";
                                     ~~~~~~~~~~~~~~~~~
```

Damn it typescript stuff. Hmm. I think it expects all the modules to be having
types.

Oh. There are types here

https://github.com/wheresrhys/fetch-mock/blob/c553baf8295d2a57ad05b4d7524b866a42a0a9e6/types/index.d.ts

Hmm.

https://github.com/wheresrhys/fetch-mock/issues?q=is%3Aissue+typescript+is%3Aclosed
https://github.com/wheresrhys/fetch-mock/issues/501

Oh, the jest wrapper does NOT have a typescript definition

https://github.com/wheresrhys/fetch-mock-jest

Hmm.

https://github.com/wheresrhys/fetch-mock-jest/issues/7

Hmm. Okay. Moving on to another thing just to try it out.

https://github.com/jefflau/jest-fetch-mock
https://www.npmjs.com/package/jest-fetch-mock

```bash
$ npm install --save-dev jest-fetch-mock
```

I tried this. It worked well. It did mock the response. But weirdly, my
response came out as string in the request result and not as an object. Not
sure how it wasn't parsed.

I noticed that the octokit request uses the other fetch-mock libray

https://github.com/octokit/request.js/blob/master/test/request.test.ts

That too with typescript and with ts-jest and all. With jest! :)

Hmm.

```typescript
import fetchMock, { enableFetchMocks } from "jest-fetch-mock";
enableFetchMocks();

import { initiateSignIn } from "./github";

describe("GitHub API", () => {
  it("should initiate sign", async () => {
    fetchMock.mockResponseOnce(() =>
      Promise.resolve({
        body: JSON.stringify({
          // eslint-disable-next-line @typescript-eslint/naming-convention
          device_code: "this-is-a-very-big-device-code",
          user_code: "small-user-code",
          verification_uri: "https://github.com/some-url",
          expires_in: 1000,
          interval: 10,
        }),
      })
    );
    const codeResponse = await initiateSignIn();
    expect(codeResponse).toStrictEqual(
      expect.objectContaining({
        deviceCode: expect.any(String),
        userCode: expect.any(String),
        verificationUri: expect.any(String),
        expiresIn: expect.any(Number),
        interval: expect.any(Number),
      })
    );
  });
});
```

It didn't workout well. So, moving on. :P I mean, it was always string data,
and not parsed string, which will give a Js object. Oh. I realized the issue.
Hmm. It was because I didn't put the `Content-Type` header. Of course! I mean,
the client library will parse stuff only if it knows the data type of the data
right. Hmm. I was able to make it work. Hmm.

request library uses the fetch-mock library in a different way - they actually
pass in the fetch option. Hmm.

So, I'm NOT moving to back to another library. This one works. Let's see how
it goes. :)

https://www.npmjs.com/package/jest-fetch-mock does NOT have a way to match body
though. Hmm. I mean, I could add a function which checks the request. Hmm.
Damn. I need to use the other library huh. Phew.

```typescript
/* eslint-disable @typescript-eslint/naming-convention */

import fetchMock, { enableFetchMocks } from "jest-fetch-mock";
enableFetchMocks();

import { initiateSignIn } from "./github";

describe("GitHub API", () => {
  it("should initiate sign", async () => {
    fetchMock.mockResponseOnce(
      JSON.stringify({
        device_code: "this-is-a-very-big-device-code",
        user_code: "small-user-code",
        verification_uri: "https://github.com/some-url",
        expires_in: 1000,
        interval: 10,
      }),
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    const codeResponse = await initiateSignIn();
    expect(codeResponse).toStrictEqual({
      deviceCode: "this-is-a-very-big-device-code",
      userCode: "small-user-code",
      verificationUri: "https://github.com/some-url",
      expiresIn: 1000,
      interval: 10,
    });
  });
});
```

---

Right. I went back to fetch-mock for ease of use and realized it just sends
actual requests in this case

```typescript
/* eslint-disable @typescript-eslint/naming-convention */

import fetchMock from "fetch-mock";

import { initiateSignIn } from "./github";

describe("GitHub API", () => {
  it("should initiate sign", async () => {
    fetchMock.mock(
      {
        url: "https://github.com/login/device/code",
        // headers: {
        //   accept: "application/json",
        // },
        body: {
          client_id: "dummy-client-id",
        },
      },
      {
        device_code: "this-is-a-very-big-device-code",
        user_code: "small-user-code",
        verification_uri: "https://github.com/some-url",
        expires_in: 1000,
        interval: 10,
      }
    );

    const codeResponse = await initiateSignIn("dummy-client-id");
    expect(codeResponse).toStrictEqual({
      deviceCode: "this-is-a-very-big-device-code",
      userCode: "small-user-code",
      verificationUri: "https://github.com/some-url",
      expiresIn: 1000,
      interval: 10,
    });
  });
});
```

It says 404 Not found as the client ID is wrong. It's a dummy one. Also,
switching off the Internet fails the request too. Hmm. It's a real request.
Hmm.

```bash
$ jest
 FAIL  src/github.test.ts
  ● Console

    console.log
      TypeError: fetch is not a function
          at fetchWrapper (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/node_modules/@octokit/request/dist-src/fetch-wrapper.js:14:12)
          at Object.newApi [as request] (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/node_modules/@octokit/request/dist-src/with-defaults.js:7:20)
          at Object.<anonymous> (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/src/github.ts:17:26)
          at Generator.next (<anonymous>)
          at /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/src/github.ts:8:71
          at new Promise (<anonymous>)
          at Object.<anonymous>.__awaiter (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/src/github.ts:4:12)
          at Object.initiateSignIn (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/src/github.ts:15:12)
          at /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode/src/github.test.ts:28:32
          at Generator.next (<anonymous>)

      at Object.<anonymous> (src/github.ts:37:13)

  ● GitHub API › should initiate sign

    TypeError: fetch is not a function

      15 | ): Promise<VerificationCodeResponse> {
      16 |   try {
    > 17 |     const result = await request("POST https://github.com/login/device/code", {
         |                          ^
      18 |       headers: {
      19 |         accept: "application/json",
      20 |       },

      at fetchWrapper (node_modules/@octokit/request/dist-src/fetch-wrapper.js:14:12)
      at Object.newApi [as request] (node_modules/@octokit/request/dist-src/with-defaults.js:7:20)
      at Object.<anonymous> (src/github.ts:17:26)
      at src/github.ts:8:71
      at Object.<anonymous>.__awaiter (src/github.ts:4:12)
      at Object.initiateSignIn (src/github.ts:15:12)
      at src/github.test.ts:28:32

 PASS  src/sample.test.ts

Test Suites: 1 failed, 1 passed, 2 total
Tests:       1 failed, 1 passed, 2 total
Snapshots:   0 total
Time:        4.273 s
Ran all test suites.
```

It finally passes now, after using `sandbox()`. It's weird that I have to use
`fetch` option for it to work. It's an optional thing and only for the test 😅

But in reality, this makes things a lot easier for testing. fetch-mock makes it
easy to test things!

Wow, okay, so everything works now. I have been trying to write a test for the
error scenarios. For verification code API, there won't be any errors usually.
I was simply trying it out with the https://github.com/login/oauth/access_token
API and I noticed that if I don't obey the interval time, it just increases the
interval exponentially!! :O So, I gotta be really careful! Also, I should have
some max amount of retries I think. And if I get `slow_down` error, then I
should first slow down. Ideally, it shouldn't have been so fast at all!! Hmm.
I'm wondering how to test for it. Hmm. This is only for later though

---

https://tools.ietf.org/html/rfc8628#section-3.5
https://tools.ietf.org/html/rfc8628

---

https://kentcdodds.com/blog/stop-mocking-fetch

“How to mock a Fetch API request with Jest and TypeScript” by Cesar William Alvarenga https://link.medium.com/pCF91wE33ab

---

Do I really have to use request library? Hmm. I wonder what kind of capabilities it has. But it does have some cancellation feature etc. Hmm.

I still don't like the fact that I have to pass in fetch as an argument. Hmm. I guess it's mostly because I have to find a different way to mock the whole thing when running integration tests. One way for jest, another way for mocha. Hmm. I guess it's easier if an actual server is running huh. Hmm. Gotta check Kent C Dodds blog post about not mocking fetch. Hmm. But running a mock server takes time. Hmm. At least that's my assumption.

I'm also wondering the benefit of this current test. If I refactor, I don't have to manually run the extension to check if it still works. Hmm. The test will do automated testing.

I also have to create an interface and this will be an implementation of it. Later, I could mock the interface and do dependency injection to avoid calls to GitHub or even the fetch mocks. At least for unit tests.

For integration tests, I can try to mock the GitHub part somehow and just have a happy path alone.

I will handle errors at the lower level at unit test level :)

For initiate sign in, that means - taking care of client and server errors. For server errors like 5xx tell that the GitHub server has some issue. For 4xx, tell that there was an issue in the extension. Report the issue to Sentry or something. See how to report issues with consent. Or else it might be a big problem, since it's confidential data. Maybe the error reporting can contain basic data, like what kind of error happened, in which stage, and what's the error message etc. But no user data, or any credentials or codes (user code, device code).

---

Consider using a simple HTTP client? Which can create a simple request like curl? Hmm. And can also be mocked? Hmm. As we don't have any complicated use cases. Also, the library is not helping with error codes. So yeah. I could just use a simple http client like axios or something. Gotta check how to mock though.

And also check about how to get user details using GitHub REST API.

---

The activate function, where sign in command is registered, we'll call a method of a class or a function. And that's it. We'll use real stuff over here.

While integration testing, we could fake it all. Like, use fake server or something. Hmm.

And for unit testing, we'll pass in a mock implementation. Use an interface called GitHubDeviceFlowOAuthAPI and real implementation would be GitHubDeviceFlowOAuth or maybe smaller? Hmm.

The interface methods could be, initiateLogin or initLogin or initSignIn . And then one isUserSignedIn or getAccessToken or similar. This method would have multiple calls. Right. Till expiry and then that's it :)

Check how to do polling with an interval. Check how to handle rate limit errors. Should we ensure it never happens or just handle it just in case. And if it happens too many times continuously, chuck it. Report it. Hmm? Hmm

We need to support cancellation of request too!! :)

---

Separate file with module to get user information with /user endpoint. Again, interface. This time GitHub API interface

In mock, only respond to requests with Accept and Content Type as application/json !! :) Always respond with JSON. If no mock defined by test matches the API call made by code, then mock should return 500 server error as response. Most tests would fail then.

---

[ ] How easy is it to change HTTP Client libraries?

[ ] Will the tests change when I change implementation?

[ ] Is it easy to read the code? Or complicated?

[ ] Do we have a good and fast mocking strategy? We need it for both unit and
integration tests too in the future

[ ] Are we defining what we are testing? Are we writing too many tests? Are we
writing tests for very simple code?

---

https://duckduckgo.com/?t=ffab&q=writing+tests+when+interacting+with+external+API&ia=web&iax=qa
http://stackoverflow.com/questions/7564038/ddg#7564101

---

https://github.com/kentcdodds/bookshelf

https://miragejs.com/
https://miragejs.com/docs/comparison-with-other-tools/#msw

https://github.com/mswjs/msw

---

For the benefit of keeping my mock code for the happy path in one place, I'm
going to be using `msw` https://github.com/mswjs/msw

Also, this helps me avoid the mock `fetch` to be passed to the function. Also,
this can be reused easily for the mocha integration tests I write :)

And I guess this makes the GitHub client test more of an integration test.

Current version

```typescript
/* eslint-disable @typescript-eslint/naming-convention */

import fetchMock from "fetch-mock";

import { initiateSignIn } from "./github";

describe("GitHub API", () => {
  it("should initiate sign in", async () => {
    const fetch = fetchMock.sandbox().mock(
      {
        url: "https://github.com/login/device/code",
        method: "POST",
        headers: {
          accept: "application/json",
        },
        body: {
          client_id: "dummy-client-id",
        },
      },
      {
        device_code: "this-is-a-very-big-device-code",
        user_code: "small-user-code",
        verification_uri: "https://github.com/some-url",
        expires_in: 1000,
        interval: 10,
      }
    );

    const codeResponse = await initiateSignIn("dummy-client-id", fetch);
    expect(codeResponse).toStrictEqual({
      deviceCode: "this-is-a-very-big-device-code",
      userCode: "small-user-code",
      verificationUri: "https://github.com/some-url",
      expiresIn: 1000,
      interval: 10,
    });
  });

  it("should give appropriate error on initiate sign in", async () => {
    // change code to have bad response
    const fetch = fetchMock.sandbox().mock(
      {
        url: "https://github.com/login/device/code",
        method: "POST",
        headers: {
          accept: "application/json",
        },
        body: {
          client_id: "dummy-client-id",
        },
      },
      {
        device_code: "this-is-a-very-big-device-code",
        user_code: "small-user-code",
        verification_uri: "https://github.com/some-url",
        expires_in: 1000,
        interval: 10,
      }
    );

    const codeResponse = await initiateSignIn("dummy-client-id", fetch);
    expect(codeResponse).toStrictEqual({
      deviceCode: "this-is-a-very-big-device-code",
      userCode: "small-user-code",
      verificationUri: "https://github.com/some-url",
      expiresIn: 1000,
      interval: 10,
    });
  });
});
```

---

https://medium.com/dev-genius/unit-tests-c70618dc1e25

---

So, yeah, msw does support NodeJs. I didn't notice this before. Actually, even
in my company project, we use msw with node, in the jsdom environment with jest.

https://mswjs.io/docs/faq

https://mswjs.io/docs/faq#can-i-use-it-with-node

https://mswjs.io/docs/getting-started/install

It uses this for Node
https://github.com/mswjs/node-request-interceptor

```bash
$ npm install msw --save-dev
```

https://mswjs.io/docs/getting-started/mocks
https://mswjs.io/docs/getting-started/mocks/rest-api
https://mswjs.io/docs/getting-started/integrate
https://mswjs.io/docs/getting-started/integrate/node

---

https://duckduckgo.com/?t=ffab&q=making+api+calls+in+javascript&ia=web

---

I started to try things out and I saw these BIIG errors with typescript telling
me that it's not able to understand some stuff. Apparently it's all browser
stuff

```bash
$ npm run compile

> cocreate-remote@0.0.1 compile /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/cocreate-remote-vscode
> tsc -p ./

node_modules/headers-utils/lib/headersToList.d.ts(2,48): error TS2304: Cannot find name 'Headers'.
node_modules/headers-utils/lib/headersToObject.d.ts(6,50): error TS2304: Cannot find name 'Headers'.
node_modules/headers-utils/lib/listToHeaders.d.ts(2,59): error TS2304: Cannot find name 'Headers'.
node_modules/headers-utils/lib/objectToHeaders.d.ts(5,62): error TS2304: Cannot find name 'Headers'.
node_modules/headers-utils/lib/stringToHeaders.d.ts(5,55): error TS2304: Cannot find name 'Headers'.
node_modules/msw/lib/types/context/body.d.ts(7,55): error TS2304: Cannot find name 'Blob'.
node_modules/msw/lib/types/context/body.d.ts(7,94): error TS2304: Cannot find name 'ReadableStream'.
node_modules/msw/lib/types/context/body.d.ts(7,116): error TS2304: Cannot find name 'FormData'.
node_modules/msw/lib/types/context/fetch.d.ts(2,56): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/context/fetch.d.ts(2,72): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/context/fetch.d.ts(8,75): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/rest.d.ts(18,38): error TS2304: Cannot find name 'Blob'.
node_modules/msw/lib/types/rest.d.ts(18,77): error TS2304: Cannot find name 'ReadableStream'.
node_modules/msw/lib/types/rest.d.ts(18,99): error TS2304: Cannot find name 'FormData'.
node_modules/msw/lib/types/rest.d.ts(23,97): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/rest.d.ts(33,42): error TS2304: Cannot find name 'Blob'.
node_modules/msw/lib/types/rest.d.ts(33,81): error TS2304: Cannot find name 'ReadableStream'.
node_modules/msw/lib/types/rest.d.ts(33,103): error TS2304: Cannot find name 'FormData'.
node_modules/msw/lib/types/rest.d.ts(38,101): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/rest.d.ts(43,42): error TS2304: Cannot find name 'Blob'.
node_modules/msw/lib/types/rest.d.ts(43,81): error TS2304: Cannot find name 'ReadableStream'.
node_modules/msw/lib/types/rest.d.ts(43,103): error TS2304: Cannot find name 'FormData'.
node_modules/msw/lib/types/rest.d.ts(48,101): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/rest.d.ts(54,42): error TS2304: Cannot find name 'Blob'.
node_modules/msw/lib/types/rest.d.ts(54,81): error TS2304: Cannot find name 'ReadableStream'.
node_modules/msw/lib/types/rest.d.ts(54,103): error TS2304: Cannot find name 'FormData'.
node_modules/msw/lib/types/rest.d.ts(59,101): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/rest.d.ts(64,42): error TS2304: Cannot find name 'Blob'.
node_modules/msw/lib/types/rest.d.ts(64,81): error TS2304: Cannot find name 'ReadableStream'.
node_modules/msw/lib/types/rest.d.ts(64,103): error TS2304: Cannot find name 'FormData'.
node_modules/msw/lib/types/rest.d.ts(69,101): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/rest.d.ts(75,42): error TS2304: Cannot find name 'Blob'.
node_modules/msw/lib/types/rest.d.ts(75,81): error TS2304: Cannot find name 'ReadableStream'.
node_modules/msw/lib/types/rest.d.ts(75,103): error TS2304: Cannot find name 'FormData'.
node_modules/msw/lib/types/rest.d.ts(80,101): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/rest.d.ts(85,42): error TS2304: Cannot find name 'Blob'.
node_modules/msw/lib/types/rest.d.ts(85,81): error TS2304: Cannot find name 'ReadableStream'.
node_modules/msw/lib/types/rest.d.ts(85,103): error TS2304: Cannot find name 'FormData'.
node_modules/msw/lib/types/rest.d.ts(90,101): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/rest.d.ts(96,42): error TS2304: Cannot find name 'Blob'.
node_modules/msw/lib/types/rest.d.ts(96,81): error TS2304: Cannot find name 'ReadableStream'.
node_modules/msw/lib/types/rest.d.ts(96,103): error TS2304: Cannot find name 'FormData'.
node_modules/msw/lib/types/rest.d.ts(101,101): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/rest.d.ts(106,42): error TS2304: Cannot find name 'Blob'.
node_modules/msw/lib/types/rest.d.ts(106,81): error TS2304: Cannot find name 'ReadableStream'.
node_modules/msw/lib/types/rest.d.ts(106,103): error TS2304: Cannot find name 'FormData'.
node_modules/msw/lib/types/rest.d.ts(111,101): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/rest.d.ts(117,42): error TS2304: Cannot find name 'Blob'.
node_modules/msw/lib/types/rest.d.ts(117,81): error TS2304: Cannot find name 'ReadableStream'.
node_modules/msw/lib/types/rest.d.ts(117,103): error TS2304: Cannot find name 'FormData'.
node_modules/msw/lib/types/rest.d.ts(122,101): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/rest.d.ts(127,42): error TS2304: Cannot find name 'Blob'.
node_modules/msw/lib/types/rest.d.ts(127,81): error TS2304: Cannot find name 'ReadableStream'.
node_modules/msw/lib/types/rest.d.ts(127,103): error TS2304: Cannot find name 'FormData'.
node_modules/msw/lib/types/rest.d.ts(132,101): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/rest.d.ts(138,42): error TS2304: Cannot find name 'Blob'.
node_modules/msw/lib/types/rest.d.ts(138,81): error TS2304: Cannot find name 'ReadableStream'.
node_modules/msw/lib/types/rest.d.ts(138,103): error TS2304: Cannot find name 'FormData'.
node_modules/msw/lib/types/rest.d.ts(143,101): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/rest.d.ts(148,42): error TS2304: Cannot find name 'Blob'.
node_modules/msw/lib/types/rest.d.ts(148,81): error TS2304: Cannot find name 'ReadableStream'.
node_modules/msw/lib/types/rest.d.ts(148,103): error TS2304: Cannot find name 'FormData'.
node_modules/msw/lib/types/rest.d.ts(153,101): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/rest.d.ts(159,42): error TS2304: Cannot find name 'Blob'.
node_modules/msw/lib/types/rest.d.ts(159,81): error TS2304: Cannot find name 'ReadableStream'.
node_modules/msw/lib/types/rest.d.ts(159,103): error TS2304: Cannot find name 'FormData'.
node_modules/msw/lib/types/rest.d.ts(164,101): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/rest.d.ts(169,42): error TS2304: Cannot find name 'Blob'.
node_modules/msw/lib/types/rest.d.ts(169,81): error TS2304: Cannot find name 'ReadableStream'.
node_modules/msw/lib/types/rest.d.ts(169,103): error TS2304: Cannot find name 'FormData'.
node_modules/msw/lib/types/rest.d.ts(174,101): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/setupWorker/glossary.d.ts(7,43): error TS2304: Cannot find name 'URL'.
node_modules/msw/lib/types/setupWorker/glossary.d.ts(9,13): error TS2304: Cannot find name 'ServiceWorker'.
node_modules/msw/lib/types/setupWorker/glossary.d.ts(10,19): error TS2304: Cannot find name 'ServiceWorkerRegistration'.
node_modules/msw/lib/types/setupWorker/glossary.d.ts(18,31): error TS2304: Cannot find name 'Event'.
node_modules/msw/lib/types/setupWorker/glossary.d.ts(18,46): error TS2304: Cannot find name 'EventTarget'.
node_modules/msw/lib/types/setupWorker/glossary.d.ts(30,5): error TS2304: Cannot find name 'ServiceWorker'.
node_modules/msw/lib/types/setupWorker/glossary.d.ts(31,5): error TS2304: Cannot find name 'ServiceWorkerRegistration'.
node_modules/msw/lib/types/setupWorker/glossary.d.ts(37,19): error TS2304: Cannot find name 'RegistrationOptions'.
node_modules/msw/lib/types/setupWorker/start/createStart.d.ts(2,124): error TS2304: Cannot find name 'ServiceWorkerRegistration'.
node_modules/msw/lib/types/utils/createBroadcastChannel.d.ts(10,54): error TS2304: Cannot find name 'MessageEvent'.
node_modules/msw/lib/types/utils/handlers/requestHandler.d.ts(9,97): error TS2304: Cannot find name 'RequestInit'.
node_modules/msw/lib/types/utils/handlers/requestHandler.d.ts(13,10): error TS2304: Cannot find name 'URL'.
node_modules/msw/lib/types/utils/handlers/requestHandler.d.ts(14,13): error TS2304: Cannot find name 'Request'.
node_modules/msw/lib/types/utils/handlers/requestHandler.d.ts(17,11): error TS2304: Cannot find name 'Request'.
node_modules/msw/lib/types/utils/handlers/requestHandler.d.ts(18,16): error TS2304: Cannot find name 'Request'.
node_modules/msw/lib/types/utils/handlers/requestHandler.d.ts(19,12): error TS2304: Cannot find name 'Request'.
node_modules/msw/lib/types/utils/handlers/requestHandler.d.ts(20,18): error TS2304: Cannot find name 'Request'.
node_modules/msw/lib/types/utils/handlers/requestHandler.d.ts(21,16): error TS2304: Cannot find name 'Request'.
node_modules/msw/lib/types/utils/handlers/requestHandler.d.ts(22,18): error TS2304: Cannot find name 'Request'.
node_modules/msw/lib/types/utils/handlers/requestHandler.d.ts(23,15): error TS2304: Cannot find name 'Request'.
node_modules/msw/lib/types/utils/handlers/requestHandler.d.ts(24,15): error TS2304: Cannot find name 'Request'.
node_modules/msw/lib/types/utils/handlers/requestHandler.d.ts(25,21): error TS2304: Cannot find name 'Request'.
node_modules/msw/lib/types/utils/handlers/requestHandler.d.ts(27,15): error TS2304: Cannot find name 'Request'.
node_modules/msw/lib/types/utils/matching/matchRequestUrl.d.ts(7,46): error TS2304: Cannot find name 'URL'.
npm ERR! code ELIFECYCLE
npm ERR! errno 2
npm ERR! cocreate-remote@0.0.1 compile: `tsc -p ./`
npm ERR! Exit status 2
npm ERR!
npm ERR! Failed at the cocreate-remote@0.0.1 compile script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     /Users/karuppiahn/.npm/_logs/2020-11-02T04_12_36_972Z-debug.log
```

https://github.com/mswjs/msw/issues/408

The main problem is that, I won't know if I use any browser stuff by mistake.
Hmm.

---

Create an issue to have separate Typescript config for creating production extension. Current Typescript config is mostly working out for test - but it has stuff like "dom" which is needed for MSW module (it's dependencies), but dom is NOT at all needed for actual production code which will run only in node environment. So, we gotta create a separate Typescript config with no dom and other good stuff to ensure we have a nice packaged output! Just for publishing! :)

More like a simple config - where we don't have to mention so many exclude etc. Instead, we just need to mention what to compile (extension ts file) and that's it. And NO dom. :) And good configs for bundling, minification etc

---

I have been trying to use msw and damn, it also sounds a bit complicated. I have
been trying to make the request handler work - by doing some request body
matching. But no, I'm not able to do it. Weird errors or issues.

I noticed that the request body is already parsed, so I don't need to parse it
with `JSON.parse`, so that's good. I found this by running debug.

But now, I'm not even sure how to read the request body. Hmm.

I get some weird error. My code -

```typescript
/* eslint-disable @typescript-eslint/naming-convention */
// src/mocks/handlers.js
import { rest } from "msw";

export const githubApiHandlers = [
  rest.post("https://github.com/login/device/code", (req, res, ctx) => {
    const accept = req.headers.get("Accept");
    // Allow only clients that accept JSON
    if (accept !== "application/json") {
      return res(ctx.status(400));
    }

    // const reqBody = req.body?.toString();
    // if (!reqBody) {
    //   return res(ctx.status(400));
    // }
    // const parsedReqBody = JSON.parse(reqBody);
    // Client ID must be present and allow only one client ID
    if (req.body?["client_id"][0] !== "dummy-client-id") {
      return res(ctx.status(400));
    }

    return res(
      ctx.status(200),
      ctx.json({
        device_code: "this-is-a-very-big-device-code",
        user_code: "small-user-code",
        verification_uri: "https://github.com/some-url",
        expires_in: 1000,
        interval: 10,
      })
    );
  }),
];
```

The line

```typescript
if (req.body?["client_id"][0] !== "dummy-client-id") {
```

I'm asked to put some expression at the end. It's weird. I'm still trying to
understand what's going on. ESLint is silent when I do this

```typescript
if (req.body?["client_id"][0] !== "dummy-client-id":true) {
```

I have to check if there is any typescript error.

https://mswjs.io/docs/api/request
https://mswjs.io/docs/basics/request-handler

Damn it. I misunderstood the `?` operator. It's the conditional operator just
like JavaScript. Hence the error about `:` in both ESLint and TypeScript. Hmm.

```typescript
if (req.body && req.body["client_id"] !== "dummy-client-id") {
```

It just says that `client_id` cannot be used.

```
Element implicitly has an 'any' type because expression of type '"client_id"' can't be used to index type 'string | Record<string, any>'.
  Property 'client_id' does not exist on type 'string | Record<string, any>'.ts(7053)
```

https://duckduckgo.com/?t=ffab&q=typescript+Record+type&ia=web&iax=qa

https://fnune.com/typescript/2019/01/30/typescript-series-1-record-is-usually-not-the-best-choice/

https://blog.ricardofilipe.com/understanding-typescript-records/

https://duckduckgo.com/?t=ffab&q=Record%3Cstring%2C+any%3E&ia=web&iax=qa

https://stackoverflow.com/questions/51936369/what-is-the-record-type-in-typescript#51937036

https://duckduckgo.com/?t=ffab&q=typescript+read+from+record&ia=web

http://www.rickcarlino.com/2017/02/27/real-world-use-case-for-typescript-record-types.html

---

Finally had to change the type using `as`. Hmm. Looks like that's the usual
thing to do. I just noticed msw code do something like this

```typescript
ctx.json(req.body as Record<string, any>),
```

Even though I thought that just `ctx.json(req.body)` was enough. Hmm

My mock server code looks ugly and too much work and code. Hmm.

I just realized I also made a mistake in my mock server code. Hmm. I missed to
check if the body is present or not. I let the request have a good response
even when there was no body present. Hmm. My tests will need tests I guess, lol.

I just need to make sure that the two entites are equal.

I'm just wishing that mocking was easier. Hmm. Gonna check fetch-mock again,
which felt simpler in terms of matching. Some of the goals are

- Be able to define all the happy path mocks in one place
- Both jest and mocha should be able to use it with ease
- Do not change existing source code to mock things
- Be able to define edge cases in some tests and also remove them too once the
  test is done
- Be able to match request headers with ease without code and just declaratively
- Be able to match request body with ease without code and just declaratively
- Be able to send response body and status with ease, without code and just
  declaratively

I was checking out Nock. I read in msw site that it needs adapters, but only in
the case of Axios, hmm

https://github.com/nock/nock#axios

I do need to change my http library I think. octokit `request` may not be needed
for my current use case. It's not really helping that much. 😅 At least, that's
my belief

Even nock seems complicated 😕 I mean, even Jest is complicated I think. I'm
still trying to understand them. Hmm. I might have to define the kind of
behavior I want -

- What kind of mocking happens? Mocking for every request or just one request?
  - That is, What happens to the mock after the mock has been used once for a request?
- Does the mock server throw errors by default when there are no mocks to be
  matched?
- Is there any option to let the request pass through to the real network? Is
  it enabled by default? Do I want that? Maybe not!

fetch-mock seems to be able to mock only fetch like stuff. Hmm. Also, I was
wondering - how frequently would I really be changing the library for the http
request? But if I try to mock the http library using Jest, I would need to do
the same for mocha.

---

https://duckduckgo.com/?q=intercept+and+mock+http+request+in+nodejs&t=ffab&ia=web

https://codeburst.io/testing-mocking-http-requests-with-nock-480e3f164851

---

Come to think of it, I think it's okay to have msw - it's a general library and
not specific to fetch. It does check the type of the request body. That's okay.
I was also thinking if I should define the types of the request body in the
code, but then, if I do that and use it in code and reuse it in the test, then
the test would always work, even if the request body schema and strucuture is
actually wrong and does not match what github expects.

So, I guess I'll just define the request body in the test. I think I could
extract the part about checking if it's a JSON based request and response, using
the headers. So that I can use it later with ease as I want it for all requests.
Or, I could do that later too, hmm. Anyways, I think this is not so bad. Hmm.
Instead of spending too much, I think I'll just go ahead and choose msw itself
and check more about it.

I think the possible pitfalls are - a mistake in the server mock, that is the
mock request handler. I need to be really careful about changing existing mocks,
for example refactoring etc. Ideally, there shouldn't be any change required as
long as the GitHub API doesn't change and I don't think it will as it looks like
that's the standard for any OAuth Device Authentication Flow.

I think I should try as much as possible to have less logic in the mock code, to
keep it simple. Do I avoid duplication or not? I could duplicate and keep it
simple, it's just too verbose. Other than that, I don't think it will need any
changes really, so duplication is also fine, for example, checking the content
type and accept headers. Hmm.

It looks a bit complex, but not very complex. I can check if the tests fail if
I change the code etc.

I guess I just gotta be careful when touching the request handler of github. I
was wondering if I should have any snapshot testing thing :P Lol. Like, change
it only intentionally. I guess it's something to take care of. If someone or me
tries to change it, we need to check what change it is - is it an addition of a
new mock API or a change in existing API - in any case, check the changes, to be
careful! :)

---

I did a youtube video on the GitHub Device Flow. The content is here -
[YouTube Video 1](./youtube-video-1.md)

---

Find a library that has request cancellation and also polling kind of feature.
If polling is not possible by any library itself, chuck it for sometime. Finish
the first step of getting user and device code with UI. Polling comes in only
during getting Access Token

---

I'm just checking how to do record / object matching or equality. This is for
easy checking of the request body

https://duckduckgo.com/?t=ffab&q=compare+two+typescript+records&ia=web

https://howtodoinjava.com/typescript/comparison-operators/

https://duckduckgo.com/?q=typescript+object+equality&t=ffab&ia=web

https://www.npmjs.com/search?q=typescript%20matcher

https://www.npmjs.com/package/minta - no..

https://www.npmjs.com/package/matzen - no ..

https://www.npmjs.com/package/mismatched

https://www.npmjs.com/search?q=typescript%20assertion

https://www.npmjs.com/search?q=typescript%20object%20equal

https://www.npmjs.com/package/shallow-equal-object

https://www.npmjs.com/search?q=typescript%20equal

https://www.npmjs.com/search?q=deep%20equal

https://www.npmjs.com/package/deep-equal

https://www.npmjs.com/package/fast-deep-equal

https://gist.github.com/substack/2790507#gistcomment-3099862
https://gist.github.com/substack/2790507

Hmm

I finally used the nodejs `util` module and used `isDeepStrictEqual` and also
put some basic error json for the user to understand what went wrong. Didn't
add any diff on what was expected and what was the actual. I'll probably add
that later :P 😅 I don't think there will be much changes in the github module
to get so many errors. We'll see :)

---

Now, I need a good and simple client library with cancellation feature and also
a feature to do polling if possible.

https://www.npmjs.com/search?q=http%20client

https://github.com/axios/axios
https://github.com/axios/axios#cancellation

https://www.npmjs.com/search?q=request

https://www.npmjs.com/package/got
https://www.npmjs.com/package/got#aborting-the-request

https://github.com/sindresorhus/gh-got
https://github.com/alexghr/got-fetch

I think I can use `got` directly. Hmm. Also, I gotta check the features it has.

I also gotta check about polling, hmm

---

I'm going to commit whatever I have done till now. Phew. Too much perfectionism
now. 🙈

Some eslint error while committing

https://duckduckgo.com/?t=ffab&q=eslint+error++%27module%27+is+not+defined++no-undef&ia=web

https://stackoverflow.com/questions/49789177/module-is-not-defined-and-process-is-not-defined-in-eslint-in-visual-studio-code#52094784

https://eslint.org/docs/user-guide/configuring#specifying-environments

Created two issues regarding this.

https://gitlab.com/snapping-shrimp/cocreate-remote-vscode/-/issues/44
https://gitlab.com/snapping-shrimp/cocreate-remote-vscode/-/issues/45

---

I'm going to start with this part now -

Show progress when initiating GitHub device flow login. Just say "Initiating
GitHub login" to the user. This is to get the user code, device code from
GitHub API. Once this is obtained, stop the progress, resolve it.

I'm going to defer the cancel feature for now.

While doing this, since I'm initiating sign in, I need to ensure that I don't
hard code the client ID in the code. I want to pass it as build time argument or
as an environment variable or something similar. I think build time argument is
the best. Once the code is compiled, it should have the right value in the final
code. But in source code, it's just going to be a placeholder / a variable :)

I also need to check how my promise can have a rejected thing too. For now,
`withProgress` only seems to have a `then`. Oh. There's no catch, but the `then`
has both fulfilled and rejected. Right.

So, yay!! I was able to make it work!! :D :D

I got the result of the verification code in an information message

```
6320-0A09 e8f65f66714a46a24aa1153be9605e85bd3ed37a https://github.com/login/device 899 5
```

I guess I need to show this to the user and ask them to login. I need to show
only user code though.

Damn. While doing this whole thing, I just noticed that there's something called
`vscode.authentication`. I can see some GitHub and Microsoft Authentication.
Right.

https://duckduckgo.com/?t=ffab&q=vscode.authentication&ia=web

https://github.com/microsoft/vscode/issues/88309

```typescript
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";
import { initiateSignIn } from "./github";

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext): void {
  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated
  console.log("cocreate-remote extension is now active");

  // The command has been defined in the package.json file
  // Now provide the implementation of the command with registerCommand
  // The commandId parameter must match the command field in package.json
  const disposable = vscode.commands.registerCommand(
    "cocreate-remote.signin.github",
    () => {
      // The code you place here will be executed every time your command is executed
      const progressOpts: vscode.ProgressOptions = {
        location: vscode.ProgressLocation.Notification,
        title: "Initiating GitHub Sign In",
      };
      vscode.window
        .withProgress(progressOpts, () => {
          return initiateSignIn("a097fbd94a0a6e2f29b8");
        })
        .then(
          (verifcationCodeResponse) => {
            vscode.window.showInformationMessage(
              `${verifcationCodeResponse.userCode}
          ${verifcationCodeResponse.deviceCode}
          ${verifcationCodeResponse.verificationUri}
          ${verifcationCodeResponse.expiresIn}
          ${verifcationCodeResponse.interval}
          `
            );
          },
          (error) => {
            vscode.window.showErrorMessage(`Some error: ${error}`);
          }
        );
    }
  );

  context.subscriptions.push(disposable);
}
```

---

The first thing in VS Code API page is about authentication 🤦

https://code.visualstudio.com/api/references/vscode-api

I found out about it when I was trying to find if there's any way to store
things through VS Code API, store the device code I got. I think I would have
had it in-memory, or used OS Key ring I think. Hmm. Right.

Anyways. It's good that I noticed this. Since I need only GitHub Authentication
for now, I can just go ahead and use this. If I need a different way to
authenticate, for example Google etc, I can come back and check how to do it.
Hmm. Also, this would work well with VS Code as this is the usual way people are
using VS Code I think, with GitHub or Microsoft account I think.

Also, there's a concept of modal in VS Code. The modal is not the one we see
on the bottom right for notifications and information and error messages. Modal
is what comes on the top - it pops down from the top, with UI related to the OS.
Like, in Mac OS, I had `ok` button and `cancel` button with Mac OS usual UI.
Hmm

And for the flow, there are some errors that can occur. For example, user
stopping the flow in between - like, by clicking cancel when asked to login.
By closing the auth tab without signing in to GitHub or without giving access to
the app. We can show an error to the user or show an information to the user in
case we feel that the user might have intentionally done it. Hmm

For example this was one of the errors.

`Error occured: Error: User did not consent to login.`

Some weird things happened when I tried it with Firefox. Hmm. Sad. :/

But it worked with Chrome though! :)

I was able to get the login details. I wished it worked with Firefox too. Hmm.
Makes me think why it didn't work with Firefox. Hmm. It happened even with
someone else I was pairing with the other day. They were using Firefox Nightly
though. Hmm. Maybe basic Firefox will work? I have noticed that my
ex-colleague's simple Firefox used to work, but not my Developer Edition. Hmm.
I don't want to create something that works in one browser but doesn't work in
another. I wanted to use this authentication mechanism. Hmm. Ideally this
should be the way to go. They also provide users a way to manage what
extensions can access their account and if the extensions have used the account.
Hmm.

```bash
$ curl -i \
-H 'Authorization: token 074e247f4a5f0e976e894223d0b3c0c26ca8803b' \
-H 'Accept: application/json' \
https://api.github.com/user
```

And the access token from the auth is perfect. Hmm.

And somehow, VS Code is also able tell when the extension last used the account.
Hmm.

Hmm. Now. I could go ahead with the usage of existing implementation and not
worry about security of the flow and everything and also have a good user
experience. The only crazy thing being - people should be able to sign in with
it. Hmm. Come to think of it, I think people just have to deal with this issue
and be able to use it - because, if this sign in is an issue for them, then
probably a lot of sign-ins are an issue for them, at least some sign-ins, as
many sites use cookies and sessions. And in my case, the Firefox developer
edition browser disallows third party cookies - I have to read about this, and
it's pretty strict. Due to this, I have had issues with using Microsoft Teams
too. Due to this, I had to allow third party cookies in my browser. I think I
need to check into that later.

Hmm. I think this is okay for now. Given the main feature for the user is to be
able to collaborate and not have the extension's fancy extra feature be in
between.

To implement device flow, I gotta do quite some stuff - actually, I could also
leverage the existing authentication mechanism and create a authentication
provider. Hmm. But I think that's unnecessary for now. Given I know that many
user Chrome, which is pretty lineant, I'll just go ahead with the existing
feature and not spend time on building an alternate feature. I'll try to see
how to show that the user is logged in - I mean, VS Code already has the account
or profile feature - where they show the account being logged in. But someone
can hide it too. Hmm.

Anyways, later I need to show all participants - like, to start with, two
participants. So, I could use that!! :)

Also, I think I should do a tutorial on how people can use this built in
authentication feature in VS Code to have login feature with GitHub. And maybe
separately on Microsoft, or they could explore it on their own 🤷 :)

Given most of the feature is done by VS Code, I need to see what I need to test.
I think there will be very less stuff for me to test. Hmm. Let's see. Later,
when I need to, or, if I need to have feature for login using any kind of site,
among many, then I need to see how to use it when I start sharing. I mean, when
starting sharing, I can only do `getSession` with a particular provider, like,
`github`. Or, I need have my own method to actually do account management. Hmm.
We will get to it when we need it ;) I'll not worry about it now. Hmm. :D :)

So, I guess I'll remove the whole `github.ts` and move on ;) Damn. This was
easy. If only I had done some research. Anyways, it was fun learning some of
the stuff ;) :D :D

Btw, even after logout, the access token the extension had access to, it's still
working. I think it was a one time oauth token and will work forever. Hmm.
Not sure why won't revoke it on signout. Or maybe they can't? Hmm

Something to checkout. But I can't control it if all account management is done
by VS Code. Hmm.

---

It's good that I don't have to implement device flow because there will be no steps of device flow, the initiation, polling to get access token, and there won't be storage of token etc.

For security reasons, I think I can always ask VS Code for token whenever I need it and use it. And everytime, I will pass in the createIfNone as true so that user is shown a modal. Maybe I can wrap this in a module and use it. I don't need to test the module I think. It's a one line logic and depends on VS Code logic and I can trust that. The value I pass it, I can't be testing that - that will be too much and it's probably hard and too much, to test it. So, yeah, I'll leave it. And anywhere I do test, I'll try to mock off that module so that I can return a mock auth session. I think I can put an interface of sorts and do it. Or a class and mock the class. One of those. Interface way looks nice. Let's see. :)

And I can remove MSW, GitHub test file and also GitHub module for now. I can have an auth module with one function for GitHub auth and have interface

I can bring in msw later for mocking the backend I will build later. I can remove all the changes I made in that initiate sign in commit I think.

---

I think, something to learn from working on this issue is that, researching is
key and having knowledge about the different features of what we have in our
hand is key. The platform we use, for example here it's VS Code, the language
we use, the libraries we use. We need to what all they can provide, and be able
to get more from outside when needed, for example ORY, Keycloak, Gluu etc when
we may need an enterprisey thing or all in one auth strategy.

Next time I start working on a feature, especially inside VS Code, which is,
most of the time, I'm going to read through this web page everytime, to ensure
I'm not reinventing the wheel! :)

https://code.visualstudio.com/api/references/vscode-api

---

So, I guess the github sign in all works now. I can remove the existing github
code I wrote.

I was wondering if I need to get the user's name, gravatar. But I think I can
do that later. For now, this is good. Also, maybe the command sign in with
github is not needed. Later I can convert this to `Share` and have sign in as
part of it.

As part of this, I'll just remove other unnecessary code. I think I'll let msw
stay - in case later I want to use github API , or my own backend API. But I'll
remove all handlers for now and make it as a general server. Or. Should I bring
it in later. Hmm. I can bring it in later too 🤷 But, that would be quite some
work, or I would do revert or something. Hmm. But yeah, better to bring it in
later, using revert or something. I don't know what issues it will cause in
between, so yeah.

---

There's also an event that fires during change of authentication sessions

```typescript
/**
 * An [event](#Event) which fires when the authentication sessions of an authentication provider have
 * been added, removed, or changed.
 */
export const onDidChangeSessions: Event<AuthenticationSessionsChangeEvent>;
```

I could use this later if needed. I don't see a point for now. So, meh.

---

I was wondering if I need to show "Sign in" command if user is already signed
in, but meh, I think that's okay. Also, since I'm planning to remove it later,
I should just not worry too much about that. Things just work now :) :D

And the code is too simple. For now, I'm not going to test it I think. Hmm.
Yeah. I'm going to leave it for now and come back later if things change! :)
